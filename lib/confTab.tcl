#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/confTab.tcl,v $
# $Date: 2000/07/17 12:37:16 $
# $Revision: 1.18.1.25 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
#
package provide zircon 1.18
#
proc configWin {net} {
    global lastConf

    if {[winfo exists [set w .@conf$net]]} {
	popup $w
	return
    }
    set lastConf($net) Nicknames
    makeToplevel $w "Configure Netspace [$net name]" {} {}
    frame $w.btn
    foreach x {revert apply save dismiss} {
	button $w.btn.$x -text [trans $x] -command "conf$x $net $w.f.chld"
    }
    evenGrid $w.btn column 0 3
    grid $w.btn.revert $w.btn.apply $w.btn.save $w.btn.dismiss -sticky ew
    canvas $w.page -highlightthickness 0 -width 350
    after idle "drawTabs $net"
    frame $w.f -relief sunken -bd 1
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 1 -weight 1 -minsize 200
    grid $w.page -sticky ew
    grid $w.f -sticky nsew
    grid $w.btn -sticky ew
    tabShow $net $lastConf($net)
}
#
proc drawTabs {net} {
    global lastConf Tabs
    set w .@conf$net.page
    set normalColor [$w cget -selectbackground]
    set activeColor [$w cget -background]
    set margin 6
    set x 2
    set maxh 0
    foreach name $Tabs {
	set id [$w create text [expr {$x+$margin+2}] [expr {-0.5*$margin}]\
	  -anchor sw -text [trans $name] -tags $name]
	set bbox [$w bbox $id]
	set wd [expr {[lindex $bbox 2]-[lindex $bbox 0]}]
	set ht [expr {[lindex $bbox 3]-[lindex $bbox 1]}]
	if {$ht > $maxh} {
	    set maxh $ht
	}
	$w create polygon 0 0 $x 0 [expr {$x+$margin}] [expr {-$ht-$margin}]\
	  [expr {$x+$margin+$wd}] [expr {-$ht-$margin}] [expr {$x+$wd+2*$margin}] 0\
	  2000 0 2000 10 0 10 -outline black -fill $normalColor -tags\
	  [list $name tab tab-$name]
	$w raise $id
	$w bind $name <ButtonPress-1> "tabShow $net $name"
	incr x [expr {$wd+2*$margin}]
    }
    set height [expr {$maxh+2*$margin}]
    $w move all 0 $height
    $w configure -width $x -height [expr {$height+4}]
    $w itemconfigure tab-$lastConf($net) -fill $activeColor
    $w raise $lastConf($net)
}
#
proc tabShow {net pane} {
    global t option pref lastConf tablock
    if {[info exists tablock($net)]} return
    set w .@conf$net
    $w.page itemconfigure tab -fill [$w.page cget -selectbackground]
    $w.page itemconfigure tab-$pane -fill [$w.page cget -background]
    $w.page raise $pane
    set f $w.f
    catch {destroy $f.chld}
    frame $f.chld
    conf$pane $net $f.chld
    grid rowconfigure $f 0 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f.chld -sticky nsew
    set lastConf($net) $pane
    switch {} [$net confChange] { confClean $net }
    update idletasks
}
#
proc confUnchange {net tab} {
   set cc [$net confChange]
   listkill cc $tab
   $net configure -confChange $cc
}
#
proc confrevclean {net which} {
    catch {uplevel #0 unset new$net}
    global Tabs
    switch $which {
    Servers {
	    global newSv delSv
	    foreach v $newSv($net) { $v delete }
	    unset newSv($net) delSv($net)
	    Server :: cleanup $net new
        }

    Channels {
	    global newChn delChn
	    foreach v $newChn($net) {
	        if {![$v sys]} { $v delete }
	    }
	    Channel :: cleanup $net new
	    unset newChn($net) delChn($net)
        }
    Messages {
	    global newMsg delMsg
	    foreach v $newMsg($net) {
	        if {![$v sys]} { $v delete }
	    }
	    Message :: cleanup $net new
	    unset newMsg($net) delMsg($net)
        }

    Friends {
	    global newFrd delFrd
	    foreach v $newFrd($net) { $v delete }
	    unset newFrd($net) delFrd($net)
	    Friends :: cleanup $net new
        }
    }
    if {[listmember $Tabs $which]} { confUnchange $net $which }
}
#
proc confdismiss {net win} {
    switch {} [$net confChange] {} default {
	if {![askUser SAVECONF {Save Changes} "You have unsaved changes. Really	dismiss?"]} return
    }
    catch {uplevel #0 unset new$net}
    killWindow .@conf$net
    $net cleanup new
    safeUnset confI$net tablock($net)
}
#
proc confrevert {net win} {
    global lastConf
    confrevclean $net $lastConf($net)
    confClean $net
    tabShow $net $lastConf($net)
}
#
proc confsave {net win} {
    global lastConf
    confCopyBack $net $lastConf($net)
    saverc $net
    confInit $net $lastConf($net)
    confClean $net
}
#
proc confapply {net win} {
    global lastConf
    confCopyBack $net $lastConf($net)
    confInit $net $lastConf($net)
    confCleaner $net
}
#
proc confDirty {net} {
    global lastConf tablock
    $net configure +confChange $lastConf($net)
    set win .@conf$net
    foreach bt {revert apply save} { $win.btn.$bt configure -state normal }
    catch {unset tablock($net)}
}
#
proc confCleaner {net} {
    global lastConf tablock
    $net configure -confChange [listremove [$net confChange] $lastConf($net)]
    set win .@conf$net
    foreach bt {apply} { $win.btn.$bt configure -state disabled }
    $win.btn.dismiss configure -state normal
    catch {unset tablock($net)}
}
#
proc confClean {net} {
    global lastConf tablock
    $net configure -confChange [listremove [$net confChange] $lastConf($net)]
    set win .@conf$net
    foreach bt {revert apply save} { $win.btn.$bt configure -state disabled }
    $win.btn.dismiss configure -state normal
    catch {unset tablock($net)}
}
#
proc confDABtns {net args} {
    global cbwstate tablock
    set win .@conf$net
    foreach bt {revert apply save dismiss} {
	set cbwstate($net,$bt) [$win.btn.$bt cget -state]
	$win.btn.$bt configure -state disabled
    }
    set tablock($net) 1
}
#
proc confRABtns {net args} {
    global cbwstate tablock
    set win .@conf$net
    foreach bt {revert apply save dismiss} {
	switch {} [$net confChange] {set st $cbwstate($net,$bt) } default {
	  set st normal
	}
	$win.btn.$bt configure -state $st
	catch {unset cbwstate($net,$bt)}
    }
    catch {unset tablock($net)}
}
