#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Consts.tcl,v $
# $Date: 2000/02/02 10:09:10 $
# $Revision: 1.18.1.8 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide zircon 1.18
#
proc InitConsts {} {
    global cfgv zFileTypes mIRCCol vt100Colour inexp1 inexp2 cVars confData\
      znetList RCFV zircsrv Tabs ctcpCmds specialCmds ch29 zMorse underflags\
      TypeCode tls
    #
    array set tls {
	coff	{Chat Offers}
	achat	{Active Chats}
	fOffer	{File Offers}
	fSend	{Active File Sends}
	fGet	{Active File Gets}
    }
    #
    array set TypeCode {
	Channel	text
	Message	msg
	Notice	note
	Chat	chat
	Info	info
    }
    #
    array set underflags {
	OLDSNO      0x1
	SERVKILL    0x2
	OPERKILL    0x4
	HACK2       0x8
	HACK3       0x1
	UNAUTH      0x20
	TCPCOMMON   0x40
	TOOMANY     0x80
	HACK4       0x100
	GLINE       0x200
	NETWORK     0x400
	IPMISMATCH  0x800
	OLDREALOP   0x1000
	CONNEXIT    0x2000
    }
    #
    array set zMorse {
	A	.-
	B	-...
	C	-.-.
	D	-..
	E	.
	F	..-.
	G --.
	H ....
	I ..
	J .---
	K -.-
	L .-..
	M --
	N -.
	O ---
	P .--.
    Q --.-
    R .-.
    S ...
    T -
    U ..-
    V ...-
    W .--
    X -..- 
    Y -.-- 
    Z --..
    a .-
    b -...
    c -.-.
    d -..
    e .
    f ..-.
    g --.
    h ....
    i ..
    j .---
    k -.-
    l .-..
    m --
    n -.
    o ---
    p .--.
    q --.-
    r .-.
    s ...
    t -
    u ..-
    v ...-
    w .--
    x -..-
    y -.--
    z --..
    0 -----
    1 .----
    2 ..---
    3 ...--
    4 ....-
    5 .....
    6 -....
    7 --...
    8 ---..
    9 ----.
    ? ..--..
    . .-.-.-
    , --..--
    }
    #
    set ch29 {&LOCAL &KILLS &NOTICES &ERRORS &CHANNEL &HASH &NUMERICS
      &SERVERS &SERVICES}
    #
    set ctcpCmds {Clientinfo Finger Ping Time Version Userinfo Other}
    set specialCmds {Zircon}
    #
    set Tabs {Nicknames IRCNames Servers Channels Friends Ignores Info}
    #
    set zircsrv {}
    #
    set RCFV 1
    #
    set znetList {}
    #
    set inexp1 {^([^ ]*) ([^ ]*)(( ([^:][^ ]*))*)( :(.*))?$}
    set inexp2 {^([^ ]*) ([^ ]*)(.*)?$}
    #
    set cfgv {showLocal showPublic showPrivate topicOnly
	minMembers friendsStyle helpService friendsOn monitorTime
	verboseCTCP noRefresh killPath showFriends noConfirm toInfo
	noPopup popInfo topicPattern listPattern wallops srvmsg 
	invisible notifyInterval testTime autoget autochat
    }
    #
    set zFileTypes {
	{{All Files}		{*}				}
	{{Gif Images}		{.gif .GIF}			}
	{{Gif Images}		{}				GIFF}
	{{JPEG Images}	{.jpg .JPG .jpeg .JPEG}		}
	{{JPEG Images}	{}				JFIF}
	{{Wav Sounds}		{.wav .WAV}			}
    }
    #
    #	0 white       8 yellow
    #	1 black       9 lightgreen
    #	2 blue       10 cyan
    #	3 green      11 lightcyan
    #	4 red        12 lightblue
    #	5 brown      13 pink
    #	6 purple     14 grey
    #	7 orange     15 lightgrey
    #
    array set mIRCCol {
	0 #ffffff	 8 #ffff00
	1 #000000	 9 #80ff80
	2 #0000ff	10 #00ffff
	3 #00ff00	11 #00ffff
	4 #ff0000	12 #000080
	5 #993333	13 #ff00ff
	6 #9933ff	14 #808080
	7 #ff9900	15 #d3d3d3
    }
    #  ANSI COLOURS
    #
    #    0 black
    #    1 red
    #    2 green
    #    3 brown
    #    4 blue
    #    5 magenta
    #    6 cyan
    #    7 white
    #
    array set vt100Colour {
	0 #ffffff
	1 #ff0000
	2 #00ff00
	3 #993333
	4 #0000ff
	5 #ff00ff
	6 #00ffff
	7 #000000
    }
    #
    # Configuration panel stuff
    #
    array set cVars [list \
    Nicknames	[list nicks] \
    IRCNames	[list ircnames] \
    Servers	{} \
    DCC		{} \
    Messages	{} \
    Notices	{} \
    Chat	{} \
    Channels	{} \
    Friends	[list showFriends friendsOn] \
    Ignores	[list ignores] \
    Info	[list showLocal showPublic showPrivate topicOnly minMembers \
    	noConfirm toInfo helpService listPattern topicPattern \
	notifyInterval sorted nocase] \
    ]
    global Configure.Net
    foreach {nm vl} [array get Configure.Net] {
        switch bool [lindex $vl 0] {
	    lappend cVars(Info) $nm
	}
    }
    #
    array set confData {
	channel	{{{Auto Join} join} {{Pop Up} open}  {{Pop Down} close} {{On Menu}\
      menu} {Draw draw}  {Jump jump} {Quiet quiet} {{Ignore Case} nocase}}
    }
}
