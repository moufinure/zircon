#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/confChan.tcl,v $
# $Date: 2001/07/12 15:35:50 $
# $Revision: 1.18.1.38 $
#
package provide zircon 1.18
#
proc editChan {net pos win chan} {
    global selID
    upvar #0 new$selID($net) news
    switch {} $chan return
    switch -- [set lchan [string tolower $chan]] [$selID($net) lname] return
    $win delete $pos
    confInsSel $win $pos [list $chan]
    set news(name) $chan
    set news(lname) $lchan
}
#
proc selectDefault {net var} {
    global selID
    upvar #0 $var deflt
    set selID($net) $deflt($net)
    setCCB $net $deflt($net)
}
#
proc selChan {net win y} {
    global selID lbdata
    set v [lindex $lbdata($win) [set p [$win nearest $y]]]
    switch {} [set chn [lindex $v 1]] {
        global defChan
	incr p -1
	set chn $defChan($net)
    }
    switch $chn $selID($net) return
    saveChan $net
    set selID($net) $chn
    setCCB $net $chn
    confSelClear $win $p
}

proc changeChan {net win y} {
    global selID defChan lbdata
    set v [lindex $lbdata($win) [set p [$win nearest $y]]]
    switch {} [set chn [lindex $v 1]] {
	set selID($net) nil
	setCCB $net {}
	confAddChan $net $win $p
    } default {
	set selID($net) $chn
	set cnm [lindex $v 0]
	setCCB $net $chn
	confSelClear $win $p
	switch $selID($net) $defChan($net) {} default {
	    confDABtns $net
	    tkwait window [mkEntryBox {} "[$net name] - Edit Channel" \
	      {Edit the channel name:} [list [list channel $cnm]] \
	      [list ok "editChan $net $p $win"] \
	      [list delete "confDelChan $net $win $y"] \
	      [list cancel {}]]
	    confRABtns $net
	}
    }
}
#
proc setCCB {net chan args} {
    upvar #0 Configure.Channel cInfo confB$net confB
    set w .@conf$net.f.chld
    switch {} $chan {
        foreach i [winfo children $w.values] { $i.entry conf -state disabled }
        foreach i [winfo children $w.options] {
	    catch {$i conf -state disabled}
        }
	set state disabled
    } default {
	upvar #0 new$chan new $chan cdata
	set state normal
	foreach b [lindex $cInfo(msg) 1] {
	    $w.options.msg$b conf -state normal
	    set confB($b) [expr {[lsearch $new(msg) $b] < 0}]
	}
	foreach {x y} [array get cInfo] {
	    switch -glob -- $y bool* {
	        $w.options.$x configure -variable new${chan}($x) -state normal
	    } int* - file* - key* {
	        $w.values.$x.entry conf -textvariable new${chan}($x) -state normal
	    }
        }
	set v $new(icon)
        entrySet $w.values.icon1.entry [lindex $v 0]
        entrySet $w.values.icon2.entry [lindex $v 1]
	set state normal
    }
    foreach i [winfo children $w.look] {
	$i conf -state $state
    }
}
#
proc CancelCAC {net win posn args} {
    selectDefault $net defChan
    confSelClear $win [expr {[$win size] - 2}]
}
#
proc doCAC {net win posn chan} {
    global lbdata
    switch -glob -- $chan {} return {[&#+]*} {} default {set chan #$chan}
    set chid [Channel :: make $net $chan]
    upvar #0 new$chid newc
    set x $posn
    if {![$chid keep]} {
	set x [expr {[llength $lbdata($win)] - 2}]
	set lbdata($win) [linsert $lbdata($win) $x [list $chan $chid]]
	$chid configure -keep 1 -sys 0
    }
    if {![array exists newc]} { $chid pack new }
    uplevel #0 "set selID($net) $chid ; lappend newChn($net) $chid"
    confSelClear $win $x
    setCCB $net $chid
    confDirty $net
}
#
proc confAddChan {net win posn} {
    confDABtns $net
    tkwait window [mkEntryBox {} "[$net name] - New Channel" {Enter the channel name:} \
      [list [list channel {}]] [list ok "doCAC $net $win $posn"] \
      [list cancel "CancelCAC $net $win $posn"]]
    confRABtns $net
}
#
proc confDelChan {net win y args} {
    switch {} [set dx [$win curselection]] {set dx [$win nearest y]}
    if {$dx < [expr {[$win size] - 2}]} {
	$win delete $dx
	global selID newChn delChn
	uplevel #0 unset new$selID($net)
	$selID($net) configure -keep 0
	if {[set x [lsearch $newChn($net) $selID($net)]] >= 0} {
	    listdel newChn($net) $x
	    if {![$selID($net) active]} {
		$selID($net) configure -keep 0
		$selID($net) delete
	    }
	} {
	    lappend delChn($net) $selID($net)
	}
	set cnm [string tolower [$win get $dx]]
	confSelClear $win $dx
	setCCB $net [set selID($net) [Channel :: find $cnm $net]]
    }
    confDirty $net
}
#
proc confChannels {net win} {
    global defChan selID Configure.Channel lbdata
    confInit $net Channels
    set wincn [makeLB $win.nels]
    set lbdata($wincn.l) {}
    trace variable lbdata($wincn.l) w lbUpdate
    set ld {}
    foreach c [$net channels] {
	if {[string compare $c $defChan($net)] && ![$c sys] && [$c keep]} {
	    lappend ld [list [$c name] $c]
	}
    }
    set selID($net) $defChan($net)
    lappend ld [list *DEFAULT* $defChan($net)]
    lappend ld  [list *NEW* {}]
    set lbdata($wincn.l) $ld
    confSelClear $wincn.l [expr {[$wincn.l size] - 2}]
    bind $wincn.l <Delete> "confDelChan $net %W %y"
    bind $wincn.l <BackSpace> "confDelChan $net %W %y"
    bind $wincn.l <Control-h> "confDelChan $net %W %y"
    bind $wincn.l <Button-1> "selChan $net %W %y ; break"
    bind $wincn.l <Double-Button-1> "changeChan $net %W %y ; break"
    bind $wincn.l <Shift-Button-1> "changeChan $net %W %y"
    frame $win.options
    set i 0
    set j 0
    foreach opt [array names Configure.Channel] {
	switch [lindex [set ov [set Configure.Channel($opt)]] 0] bool {
	    switch {} [set tn [lindex $ov 1]] { set tn $opt }
	    checkbutton $win.options.$opt -text [trans $tn] \
	      -variable new$defChan($net)($opt) \
	      -command "confDirty $net"
	    grid $win.options.$opt -row $i -column $j -sticky w
	    if {[incr j] > 2} { set j 0 ; incr i }
	}
    }
    label $win.options.msg -text [trans {show messages}]
    grid $win.options.msg - -
    if {$j<= 2} { incr i 2 } { incr i}
    set j 0
    foreach opt [lindex [set Configure.Channel(msg)] 1] {
	checkbutton $win.options.msg$opt -text [trans $opt] \
	  -variable confB${net}($opt) -command "doConfButton $net $opt"
	grid $win.options.msg$opt -row $i -column $j -sticky w
	if {[incr j] > 2} { set j 0 ; incr i }
    }
    frame $win.values
    frame $win.look
    set cd "confDirty $net"
    foreach x [array names Configure.Channel] {
	switch [lindex [set Configure.Channel($x)] 0] int {
	    labelNumber 0 $win.values.$x "-text [list [trans $x]] -width 12" {} $cd
	    grid $win.values.$x -sticky ew
	}
    }
    labelEntry 0 $win.values.icon1 "-text [list [trans icon]] -width 12" {} $cd
    labelEntry 0 $win.values.icon2 "-text [list [trans {active icon}]] -width 12" {} $cd
    labelEntry 0 $win.values.logfile "-text [list [trans {log file}]] -width 12" {} $cd
    labelEntry 0 $win.values.key "-text [list [trans key]] -width 12" {} $cd
    foreach wx "$win.values.icon1 $win.values.icon2 $win.values.logfile $win.values.key" {
	grid $wx -sticky ew
    }
    grid [button $win.look.fg -text [trans {foreground colour}] \
      -command "confChanFG $net $win"] -sticky ew
    grid [button $win.look.bg -text [trans {background colour}] \
      -command "confChanBG $net $win"] -sticky ew
    grid [button $win.look.ft -text [trans font] \
      -command "confChanFt $net $win"] -sticky ew
    grid $win.nels $win.options -rowspan 2 -sticky nsew
    grid $win.values -row 0 -column 2 -sticky nsew
    grid $win.look -row 1 -column 2 -sticky nsew
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure $win 0 -weight 1
    setCCB $net $defChan($net)
    bind $wincn <Enter> "focus $wincn.l"
}
switch -glob [info tclversion] {
8* {
#
proc confChanFG {net win} {
    global selID
    upvar #0 new$selID($net) news
    switch {} [set g [tk_chooseColor -parent $win]] {} default {
	set news(foreground) $g
        confDirty $net
    }
}
#
proc confChanBG {net win} {
    global selID
    upvar #0 new$selID($net) news
    switch {} [set g [tk_chooseColor -parent $win]] {} default {
	set news(background) $g
        confDirty $net
    }
}
#
proc confChanFt {net win} {
    tk_dialog .@ge Unimplemented "Sorry, not implemented yet" {} 0 [trans ok]
}
}
{7.[67]} {
#
proc confChanFG {net win} {
    global selID
    upvar #0 new$selID($net) news
    switch {} [set g [tk_chooseColor -parent $win]] {} default {
	set news(foreground) $g
        confDirty $net
    }
}
#
proc confChanBG {net win} {
    global selID
    upvar #0 new$selID($net) news
    switch {} [set g [tk_chooseColor -parent $win]] {} default {
	set news(background) $g
        confDirty $net
    }
}
#
proc confChanFt {net win} {
    tk_dialog .@ge Unsupported \
      "Sorry, not supported with this version of tcl/tk" \
      {} 0 [trans ok]
}
#
}
default {
#
proc confChanFG {net win} { confChanBG $net $win }
#
proc confChanFt {net win} { confChanBG $net $win }
#
proc confChanBG {net win} {
    tk_dialog .@ge Unsupported \
      "Sorry, not supported with this version of tcl/tk" \
      {} 0 [trans ok]
}
#
}
}
#
proc saveChan {net} {
    global selID
    switch nil $selID($net) return
    upvar #0 new$selID($net) new Configure.Channel cInfo
    set w .@conf$net.f.chld.values
    foreach x [array names cInfo] {
	switch [lindex $cInfo($x) 0] int - file - key {
	    set v [$w.$x.entry get]
	    switch -- $new($x) $v {} default {set new($x) $v}
	}
    }
    set v1 [list [$w.icon1.entry get] [$w.icon2.entry get]]
    switch {{} {}} $v1 {set v1 {}}
    switch -- $v1 $new(icon) {} default {set new(icon) $v1}
}
#
proc doConfButton {net indx} {
    global selID
    switch nil $selID($net) return
    upvar #0 new$selID($net) new confB$net confB
    set vdx [lsearch $new(msg) $indx]
    if {!$confB($indx)} {
	if {$vdx < 0} { lappend new(msg) $indx}
    } {
	if {$vdx >= 0} { listdel new(msg) $vdx }
    }
    confDirty $net
}
#
proc copybackChan {net} {
    global defChan
    saveChan $net
    foreach ch [info globals newchann*] {
        switch -- $net [uplevel #0 set ${ch}(net)] {
            set chan [string range $ch 3 end]
	    switch {} [info procs $chan] {} default {
                $chan unpack new
	    }
	    safeUnset $ch
	}
    }
    confDirty $net
}
#
proc confNotices {net win} {
}
#
proc confDCC {net win} {
}
#
proc confChat {net win} {
}
