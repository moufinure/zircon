#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Interface.tcl,v $
# $Date: 2001/07/10 15:36:11 $
# $Revision: 1.18.1.9 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide zircon 1.18
#
proc net_join {this chan args} {
    [Channel :: make $this $chan] sendJoin [lindex $args 0]
}
#
proc net_msg {this where what args} {
    switch {} $args { set chn nil } default { set chn [find $where $this] }
    switch $chn nil {
    	$this q1Send "PRIVMSG $where :$what"
    } default {
        $chn send $what
    }  
}
#
proc net_notice {this where what args} { $this q1Send "NOTICE $where :$what" }
#
proc net_echo {unusedthis where what args} {
    set chn [find $where $what]
    set pop 1
    set op addText
    foreach x $args {
        switch -- $x -nopop { set pop 0 ; set op doAddtext }
    }
    switch $chn nil { } default {
        $chn $op @me $what
    }
}
