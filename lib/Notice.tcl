#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Notice.tcl,v $
# $Date: 2001/07/10 15:36:12 $
# $Revision: 1.18.1.36 $
#
package provide zircon 1.18
#
proc Notice {name args} {
    switch -- $name :: {
	set op [lindex $args 0]
	switch [info procs Notice_$op] {} {return [eval Channel_$op [lrange $args 1 end] ]}
	return [eval Notice_$op [lrange $args 1 end] ]
    }
    global currentNet
    switch nil [set id [Notice :: find $name $currentNet]] {
	set id [makeNotice $name $currentNet]
    }
    eval $id configure $args
    return $id
}
#
proc notice_call {this op pars} {
    switch {} [info procs notice_$op] {
	return [objCall channel $this $op $pars]
    }
    return [eval notice_$op $this $pars]
}
#
proc makeNotice {chan net} {
    global defNotice
    set this [objName Notice]
    proc $this {unusedop args} "notice_call $this \$unusedop \$args "
    initObj $this Channel Notice
    set lchan [string tolower $chan]
    upvar #0 $this ndata NTO$net NTO
    switch *default* $lchan {} default {
	[User :: make $net $chan] join $this
    }
    if {[catch {set def $defNotice($net)}]} {
	global defaultNet defChan
	if {[catch {set def $defNotice($defaultNet)}]} {
	    set def $defChan($defaultNet)
	}
	set b 0
	set d 0
    } {
	set b [$def buttons]
	set d [$def draw]
    }
    array set ndata [uplevel #0 array get $def]
    array set ndata [list \
	keep	0 \
	buttons	$b \
	draw	$d \
	name	$chan \
	lname	$lchan \
	net	$net \
	ircIImode [$net ircIImode] \
    ]
    $net register notices $this
    set NTO($lchan) $this
    return $this
}
#
proc notice_onShow {unusedthis} { }
#
proc notice_setTitles {this} {
    set nam [$this name]
    set id {}
    switch nil [set usr [User :: find $nam [$this net]]] {} default {
	switch {} [set id [$usr id]] {} default { set id " ($id)"}
    }
    return [list $nam "Notice from $nam$id"]
}
#
proc notice_nickChange {this usr nnk} {
    switch -- [$this lname] [$usr lname] {$this nChange $nnk}
    channel_nickChange $this $usr $nnk
}
#
proc notice_nChange {this nnk} {
    set net [$this net]
    if {[$this active]} {
	[$this wid] setIcon $this $nnk
	switch nil [set usr [User :: find [$this name] $net]] {} default {
	    switch {} [set id [$usr id]] {} default { set id " ($id)"}
	}
	[$this wid] setTitle $this "Notice from $nnk$id"
    }
    upvar #0 NTO$net NTO $this cdata
    set ln [string tolower $nnk]
    unset NTO($cdata(lname))
    set NTO($ln) $this
    array set cdata [list \
	lname	$ln \
	name	$nnk \
    ]
}
#
proc notice_replace {this usr1 usr2} {
    $this nChange [$usr2 name]
    channel_replace $this $usr1 $usr2
}
#
proc notice_delete {this} { mcnDelete $this NTO[$this net] notices }
#
proc Notice_make {net nk} {
    upvar #0 NTO$net NTO
    set usr [User :: make $net $nk]
    set ln [string tolower $nk]
    if {[info exists NTO($ln)]} { set id $NTO($ln) } {
	set id [$net eval [list Notice $nk]]
    }
    $id configure -crypt [$usr crypt]
    $id show -nofocus
    $id addUser $usr 0 0
    return $id
}
#
proc Notice_find {nk net} {
    upvar #0 NTO$net NTO
    set ln [string tolower $nk]
    return [expr {[info exists NTO($ln)] ? $NTO($ln) : {nil}}]
}
#
proc Notice_save {desc net} { defSave $desc defNotice $net Notice }
#
proc notice_save {unusedthis unuseddesc} { }
#
proc notice_leave {this} {
    set chan [$this name]
    if {[askUser LEAVE "Leaving Notice $chan" "Really close notice from $chan?"]} {
	switch {} [info procs $this] {} default { $this delete }
    }
}
#
proc notice_heal {this} {
    if {[$this active]} {
	$this flag normal
	$this addText {} "*** netsplit : [$this name] may have left IRC."
    }
}
