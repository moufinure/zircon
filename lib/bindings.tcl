#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/bindings.tcl,v $
# $Date: 1998/09/08 15:02:28 $
# $Revision: 1.18.1.25 $
#
package provide zircon 1.18
#
proc doBindings {entry chid chan} {
    doInfoBindings $entry $chid
    doChanBindings $entry $chid $chan
}
#
proc winsrt {net win str} {notIdle $win $net ; tkEntryInsert $win $str}
#
proc ifSel {net win cmd} {
    notIdle $win $net
    if {[catch {selection get} bf]} return
    switch {} $bf return
    eval "$cmd {[cleanup $bf]}"
}
#
proc chDo {chan w net op data} {
    notIdle $w $net
    if {[$chan m] && ![$chan operator] && ![$chan speaker]} bell {$chan $op $data ; $w delete 0 end}
}
#
proc doChanBindings {entry chid chan} {
    global zircon
    set net [$chid net]
    bind $entry <$zircon(meta)-question> "notIdle %W $net ; $chid search" 
    bind $entry <$zircon(meta)-exclam> "notIdle %W $net ; $chid highlight"
    bind $entry <$zircon(meta)-slash> "notIdle %W $net; $chid search"
#
# Formatting codes
#
    bind $entry <$zircon(meta)-b> "winsrt $net %W \002 ; break"
    bind $entry <$zircon(meta)-c> "winsrt $net %W \003 ; break"
    bind $entry <$zircon(meta)-o> "winsrt $net %W \017 ; break"
    bind $entry <$zircon(meta)-v> "winsrt $net %W \026 ; break"
    bind $entry <$zircon(meta)-u> "winsrt $net %W \037 ; break"
#
# Smileys
#
    bind $entry <$zircon(meta)-s> "winsrt $net %W \$smiley ; break"
    bind $entry <Shift-$zircon(meta)-S> "winsrt $net %W \$scowl"
    bind $entry <Control-$zircon(meta)-s> "winsrt $net %W \$wink"
#
# Expansion
#
    bind $entry <Control-$zircon(meta)-i> "winsrt $net %W \[uExpand %W $net $chid\] ; break"
    bind $entry <$zircon(meta)-Tab> "winsrt $net %W \[uExpand %W $net $chid\] ; break"
#
# Fun stuff
#
    bind $entry <Control-c> "chDo $chid %W $net send \[toMorse \[%W get\]\]"
#
    bind $entry "<$zircon(action)>" "chDo $chid %W $net action \[%W get\] ; break"
#
    bind $entry <Control-p> "%W delete 0 end ; winsrt $net %W \[$chid getPrev\]"
    bind $entry <Up> [bind $entry <Control-p>]
    bind $entry <Control-n> "%W delete 0 end ; winsrt $net %W \[$chid getNext\]"
    bind $entry <Down> [bind $entry <Control-n>]
    foreach b [$net bindings] {
	bind $entry [lindex $b 0] "notIdle %W $net
	set net $net
	set channel $chid
	[lindex $b 1]
	unset net channel"
    }
    foreach b [$chid bindings] {
	bind $entry [lindex $b 0] "notIdle %W $net
	set net $net
	set channel $chid
	[lindex $b 1]
	unset net channel"
    }
}
#
proc doInfoBindings {entry chid} {
    global zircon
    set net [$chid net]
    bind $entry <Control-u> "notIdle %W $net ; %W delete 0 insert"
    bind Entry <Control-w> {
	notIdle %W
	set txt [%W get]
	set idx [%W index insert]
	%W delete [string wordstart $txt $idx] [string wordend $txt $idx]
    }
    bind $entry <$zircon(meta)-n> "ifSel $net %W {handleURL $net}"
    bind $entry <$zircon(meta)-j> "ifSel $net %W {channelJoin $net}"
    bind $entry <$zircon(meta)-m> "ifSel $net %W {Message :: make $net}"
    bind $entry <$zircon(meta)-f> "ifSel $net %W {finger $net}"
    bind $entry <$zircon(meta)-q> "oneLiner %W $chid"
    bind $entry <Control-g> "winsrt $net %W \007"
    bind $entry <Escape> "notIdle %W $net ; $chid makePalette Entry"
    bind $entry <Button-3> "notIdle %W $net ; $chid popPalette Entry %X %Y"
    bind $entry <$zircon(meta)-Return> "notIdle %W ; $net doMisc2 $chid %W ; break"
    bind $entry <Shift-$zircon(meta)-Return> "chDo $chid %W $net send \[strrev \[%W get\]\] "
    bind $entry <Control-$zircon(meta)-Return> "chDo $chid %W $net send \[rot13 \[%W get\]\]"
    bind $entry <ButtonRelease-2> "notIdle %W ; $chid insertSelect ; break"
    if {![$chid ircIImode]} {
	bind $entry <Return> "chDo $chid %W $net send \[%W get\]"
    } {
	bind $entry <Return> "notIdle %W $net ; $net doMisc2 $chid %W"
    }
#
    catch {bind $entry <KP_Next> "\[$chid text\] yview scroll 1 pages ; notIdle %W $net"}
    catch {bind $entry <Next> "\[$chid text\] yview scroll 1 pages ; notIdle %W $net"}
    catch {bind $entry <KP_Prior> "\[$chid text\] yview scroll -1 pages ; notIdle %W $net"}
    catch {bind $entry <Prior> "\[$chid text\] yview scroll -1 pages ; notIdle %W $net"}
}
