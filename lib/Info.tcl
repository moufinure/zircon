#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Info.tcl,v $
# $Date: 2001/07/10 15:36:11 $
# $Revision: 1.18.1.46 $
#
package provide zircon 1.18
#
# Build a Zircon Information Window.
#
proc MainInfo {} {
    global zlayout zInfo
    if {![winfo exists .@inf]} {
	set zInfo [Window .@inf -title {Zircon Information} -resizable {1 1}]
	set w [$zInfo name]
	catch {wm geometry .@inf $zlayout(default,info)}
        grid columnconfigure .@inf 0 -weight 1 -minsize 50
    }
    return .@inf
}
#
proc Info {args} {
    global BF Fg Bg Ft Bl OType defChan zInfo zII zNOII zInfo
    set name [makeObj {Channel Info} $args]
    upvar #0 $name idata
    set net $idata(net)
    if {[$net integrate]} {
        set w [[$net control] window]
	foreach {cl rw} [grid size $w] {}
	incr cl -1
	set sp 3
    } {
        set w [switchFrame [MainInfo] $name [$net name] 0 -handles]
        $zInfo register $name {} "deleteFrame [MainInfo] $name"
        grid rowconfigure $w 0 -weight 1
        grid columnconfigure $w 0 -weight 1
	set rw 0
	set cl 1
	set sp 1
    }
    set df [getDefault defChan $net]
    $name configure -hpos 0 -open [$idata(net) popInfo] \
      -close [$df close] -jump 1 -quiet 0 -draw 0 \
      -msg [$df msg] -actionmode 0 \
      -actions 0 -patterns {} -logfile {} -history [$df history] \
      -closetime [$df closetime] -log {} -menu 0 -join 0 \
      -ops {} -keep 1 -monitor 0 -wid $zInfo -window $w \
      -ircIImode [$net ircIImode]
    frame $w.icmd -borderwidth 0
    grid columnconfigure $w.icmd 0 -weight 1
    scrollbar $w.icmd.hs -orient horizontal -command "$w.icmd.entry xview"
    emacsEntry $w.icmd.entry -xscrollcommand "ghsSet $w.icmd.hs" \
      -relief sunken -state disabled
    button $w.icmd.clear -text [trans clear] -command "$name clear" -width 5 \
      -borderwidth 2 -highlightthickness 2 -pady 0
    grid $w.icmd.entry $w.icmd.clear -sticky ew
    grid $w.icmd.hs - -sticky ew
    if {[$net command]} { grid $w.icmd -row [expr {$rw+1}] -columnspan [expr {$sp+1}] -sticky ew }
    doInfoBindings $w.icmd.entry $name
    bind $w.icmd.entry <Return> "$name doCmd %W"
    set idata(text) [set oft $w.info]
    scrollbar $w.vscroller -command "$oft yview"
    text $oft -height 10 -width 80 \
      -yscrollcommand "$w.vscroller set" -borderwidth 2
    rebind $oft $idata(net)
    bind $oft <Configure> {%W see end ; notIdle %W}
    bind $w <Visibility> {notIdle %W}
    grid $w.vscroller -row $rw -column $cl -sticky ns
    grid $oft -row $rw -column 0 -columnspan $sp -sticky nsew
    set BF($name) [getOValue $name  $oft font boldFont Font]
    set Fg($name) [getOValue $name $oft foreground foreground Foreground]
    set Bg($name) [getOValue $name $oft background background Background]
    set Ft($name) [getOValue $name $oft font font Font]
    set Bl($name) [option get $oft bell Bell]
    confTag $oft {} $Fg($name) $Bg($name) $Ft($name) $BF($name)
    $name defaultTags
    return $name
}
#
proc info_delete {this} { mcnDelete $this ITO[$this net] info }
#
proc info_onShow {unusedthis} { }
#
proc info_clear {this args} {
   [$this text] delete 1.0 end
   update idletasks
}
#
proc info_send {this str args} {
    if {[[$this net] raw]} {
	$this configure -hpos 0
	$this addText {} ":>$str"
	$this addHist $str
	[$this net] sendRaw $str
    } { bell }
}
#
proc info_doCmd {this w} { [$this net] doMisc2 $this $w }
#
proc info_call {this op pars} {
    upvar #0 $this idata
    switch $op {
    lname { return $this }
    crypt { return {} }
    control { return [$idata(net) control] }
    command {
	    if {[lindex $pars 0]} {
		grid $idata(window).icmd
	    } {
		grid forget $idata(window).icmd
	    }
	    return {}
	}
    users { return [$idata(net) users] }
    }
    if {[info exists idata($op)]} { return $idata($op) }
    switch {} [info procs info_$op] {return [objCall channel $this $op $pars]}
    return [eval info_$op $this $pars]
}
#
proc info_popup {this} {
    switch nil [$this wid] {
        # integrated case.
	exposeFrame [MainControl] [$this net]
    } default {
        [$this wid] expose [$this net]
    }
    handleOn [$this net] POPINFO [list [$this lname]]
    return $this
}
#
proc info_insert {this txt} {
    if {$txt == {}} return
    set ent [$this window].icmd.entry
    while {[regexp "(\[^\n\]*)\n(.*)" $txt d line text]} {
	switch {} $line continue
	tkEntryInsert $ent $line
	$this doCmd $ent
	$ent delete 0 end
    }
    if {$txt != {}} {
	$ent insert insert $txt
	tkEntrySeeInsert $ent
    }
}
#
proc info_flagState {this state} {
    [$this window].icmd.entry configure -state $state
}
#
proc info_close {this} {
    $this flagState disabled
}
