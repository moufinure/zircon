#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Mac.tcl,v $
# $Date: 2000/03/30 13:14:51 $
# $Revision: 1.18.1.9 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
#
package provide macintosh 1.18
#
proc tmpdir {} {
    global env zircon
    if {[info exists zircon(tmp)]} { return $zircon(tmp) }
    if {[info exists env(TEMP)]} { return $env(TEMP) }
    set fl [file join $env(PREF_FOLDER) Zircon temp]
    if {![file exists $fl]} { file mkdir $fl }
    return $fl
}
#
proc prefdir {} {global env ; return [file join $env(PREF_FOLDER) Zircon]}
#
proc username {} {
    global env tcl_platform
    if {[info exists tcl_platform(user)]} {
        return $tcl_platform(user)
    }
    return $env(USER)
}
#
# No DCC on the Mac at the moment.
#
proc dccCheck {args} {}
#
proc net_dccClean {args} {}
