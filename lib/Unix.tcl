#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Unix.tcl,v $
# $Date: 2000/03/30 13:14:51 $
# $Revision: 1.18.1.8 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
#
package provide unix 1.18
#
proc tmpdir {} {
    global zircon
    if {[info exists zircon(tmp)]} { return $zircon(tmp) }
    return /tmp
}
#
proc prefdir {} { return [file join [lindex [glob ~] 0] .zircon] }
#
proc username {} {
    global env tcl_platform
    if {[info exists tcl_platform(user)]} {
        return $tcl_platform(user)
    }
    if {[info exists env(USER)]} { return $env(USER) }
    if {[catch {set w [exec whoami]}]} {
	set w [lindex [exec who am i] 0]
    }
    return $w
}
#
