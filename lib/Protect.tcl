#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Protect.tcl,v $
# $Date: 2001/07/10 15:36:12 $
# $Revision: 1.18.1.10 $
#
package provide zircon 1.18
#
proc friendAction {usr chan unusedculprit action} {
    set net [$chan net]
    foreach x $action {
	switch -- $x {
	reop	{ $net q1Send "MODE [$chan name] +o :[$usr name]" }
	deop	{ $net q1Send "MODE [$chan name] -o :[$usr name]" }
	speak	{ $net q1Send "MODE [$chan name] +v :[$usr name]" }
	silence	{ $net q1Send "MODE [$chan name] -v :[$usr name]" }
	unban	{ $net q1Send "MODE [$chan name] -b :[$usr name]!*@*" }
	reban	{ $net q1Send "MODE [$chan name] +b :[$usr name]!*@*" }
 	}
    }
}
#
proc culpritAction {prefix usr chan action} {
    set net [$chan net]
    if {![regexp {^(.*)!.*} $prefix m nk]} return
#
# We can only do something when we have a nick on which to operate!!
#
    foreach x $action {
	switch -glob -- $x {
	deop	{ $net q1Send "MODE [$chan name] -o :$nk" }
	kick*	{
		switch {} [set msg [string range $x 5 end]] {
		    set msg "[$usr name] is protected!"
		}
		$net q1Send "KICK [$chan name] $nk :$msg"
	    }
	silence	{ $net q1Send "MODE [$chan name] -v :$nk" }
	ban	{ $net q1Send "MODE [$chan name] +b :$prefix" }
	msg*	{ $net q1Send "PRIVMSG $nk :[string range $x 4 end]" }
	notice*	{ $net q1Send "PRIVMSG $nk :[string range $x 7 end]" }
	}
    }
}
#
proc banProtect {chan prefix flag banptn} {
    foreach x [$chan users] {
	if {[string match $banptn [$x name]![$x id]]} {
	    $x protect $chan $prefix ${flag}b
	}
    }
}
#
proc checkProtect {net frd lst} {
    set ok 1
    foreach x $lst {
	if {![checkRE [lindex $x 0]]} {
	    tellError $net {Regexp Error} "Regular expression\
	      [lindex $x 0] in -protect for Friend [$frd name] contains\
	      an error"
	    set ok 0
	}
	foreach y [lindex $x 1] {
	    if {![checkRE [lindex $y 0]]} {
		tellError $net {Regexp Error} "Regular expression\
		  [lindex $y 0] in -protect for Friend [$frd name] contains\
		  an error"
		set ok 0
	    }
	}
    }
    return $ok
}
