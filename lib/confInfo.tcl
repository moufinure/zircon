#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/confInfo.tcl,v $
# $Date: 2001/06/13 07:20:27 $
# $Revision: 1.18.1.12 $
#
package provide zircon 1.18
#
proc confInfo {net win} {
    global zircon Configure.Net
    confInit $net Info
    upvar #0 new$net newd confI$net confI
    frame $win.bools -relief raised
    set i 0
    set j 0
    foreach {nm val} [array get Configure.Net] {
        switch bool [lindex $val 0] {
	    switch {} [set tx [lindex $val 1]] continue
	    checkbutton $win.bools.$i@$j -text $tx \
	      -variable new${net}($nm) -command "confDirty $net"
	    grid $win.bools.$i@$j -row $j -column $i -sticky w
	    if {[incr i] > 3} { set i 0 ; incr j }
	}
    }
    frame $win.ms
    set i 0
    set j 0
    set cnt 0
    foreach {nm val} [array get Configure.Net] {
        switch mseconds [lindex $val 0] {
	    switch {} [set tx [lindex $val 1]] continue
	    labelNumber 0 $win.ms.$cnt "-text [list $tx]" \
	      [expr {$newd($nm) / 1000}] \
	      "set new${net}($nm) \[expr {\[%W get\]*1000}\] ; confDirty $net"
	    grid $win.ms.$cnt -row $i -column $j -sticky w
	    incr cnt
	    if {[incr j] > 1} { set j 0 ; incr i }
	}
    }
    frame $win.list -relief raised
    evenGrid $win.list row 0 1
    label $win.list.label1 -text "[trans {send to info}] :"
    grid $win.list.label1
    set i 0
    foreach ci [lindex [set Configure.Net(toInfo)] 1] {
	set uci [string toupper $ci]
	checkbutton $win.list.inf${i} -text $ci \
	  -variable confI(toInfo,$i) -command "doIList $net $i $uci toInfo"
	set confI(toInfo,$i) [expr {[lsearch [$net toInfo] $uci] >= 0}]
	grid $win.list.inf${i} -row 0 -column [incr i] -sticky ew
    }
    label $win.list.label2 -text "[trans {no confirm}] :"
    grid $win.list.label2
    set i 0
    foreach ci [lindex [set Configure.Net(noConfirm)] 1] {
	set uci [string toupper $ci]
	checkbutton $win.list.noc${i} -text $ci \
	  -variable confI${net}(noConfirm,$i) -command "doIList $net $i $uci noConfirm"
	set confI(noConfirm,$i) [expr {[lsearch [$net noConfirm] $uci] >= 0}]
	grid $win.list.noc${i} -row 1 -column [incr i] -sticky ew
    }

    frame $win.filter -relief raised
    evenGrid $win.filter column 0 5
    checkbutton $win.filter.public -variable new${net}(showPublic) \
      -text [trans public] -command "confDirty $net"

    checkbutton $win.filter.local -variable new${net}(showLocal) \
      -text [trans local] -command "confDirty $net"
    checkbutton $win.filter.private -variable new${net}(showPrivate) \
      -text [trans private] -command "confDirty $net"
    checkbutton $win.filter.topic -variable new${net}(topicOnly) \
      -text [trans {with topic}] -command "confDirty $net"
    checkbutton $win.filter.sorted -variable new${net}(sorted) \
      -text [trans sorted] -command "confDirty $net"
    checkbutton $win.filter.noc -variable new${net}(nocase) \
      -text [trans nocase] -command "confDirty $net"

    scale $win.filter.members1 \
      -from 1 -to 25 -label [trans {minimum number of members}] \
      -showvalue 1 -orient horizontal
    $win.filter.members1 set [$net minMembers]
    $win.filter.members1 configure \
      -command "$win.filter.members1 configure \
	-command {confDirty $net ; set new${net}(minMembers)} ; set x "
    scale $win.filter.members2 \
      -from 0 -to 25 -label [trans {maximum number of members}] \
      -showvalue 1 -orient horizontal
    $win.filter.members2 set [$net maxMembers]
    $win.filter.members2 configure \
      -command "$win.filter.members2 configure \
	-command {confDirty $net ; set new${net}(maxMembers)} ; set x "
    grid $win.filter.members1 - - $win.filter.members2 - - -sticky ew
    grid $win.filter.public $win.filter.local $win.filter.private \
      $win.filter.topic $win.filter.sorted $win.filter.noc
    frame $win.filter2
    labelEntry 0 $win.filter2.c "-text [list [trans {channel pattern}]] -width 16" \
      $newd(listPattern) "set new${net}(listPattern) \[%W get\] ; confDirty $net"
    labelEntry 0 $win.filter2.t "-text [list [trans {topic pattern}]] -width 16" \
      $newd(topicPattern) "set new${net}(topicPattern) \[%W get\] ; confDirty $net"
    grid $win.filter2.c $win.filter2.t -sticky ew
    evenGrid $win.filter2 column 0 1
    grid columnconfigure $win 0 -weight 1
    grid $win.bools -sticky ew
    grid $win.ms -sticky ew
    grid $win.list -sticky ew
    grid $win.filter -sticky ew
    grid $win.filter2 -sticky ew
}
#
proc doIList {net indx val var} {
    upvar #0 new$net newd confI$net bits
    if {$bits($var,$indx)} {
	lappend newd($var) $val
    } {
	listkill newd($var) $val
    }
    confDirty $net
}














