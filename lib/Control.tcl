#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Control.tcl,v $
# $Date: 2001/07/10 15:36:10 $
# $Revision: 1.18.1.109 $
#
#
package provide zircon 1.18
#
proc busyFlag {net} { $net configure -busy [expr {![$net busy]}] }
#
proc control_configure {this args} { confObj $this $args }
#
proc control_close {this} {
    $this flagState disabled
    [$this friends] close
}
#
proc control_lag {this secs} {
    set frm [winfo parent [$this window]].exp
    if {![winfo exists $frm.lag]} {
        grid [label $frm.lag -text {} -width 7] -row 0 -column 2
    }
    $frm.lag configure -text "$secs secs"
}
#
proc control_lagClear {this} {
    if {[winfo exists [set frm [winfo parent [$this window]].exp.lag]]} {
        $frm configure -text {}
    }
}
#
proc control_pickServer {this srv} { [$this net] changeServer $srv }
#
proc control_showServer {this srv args} {
    switch nil $srv return
    upvar #0 $this cdata
    switch {} [set port [lindex $args 0]] {set port [lindex [$srv port] 0]}
    set cdata(port) $port
    set cdata(server) [$srv host]
    set w $cdata(window).port.menu
    $w delete 0 end
    foreach x [lsort -integer [$srv port]] {
	$w add command -label $x -command "set ${this}(port) $x"
    }
    $w add separator
    $w add command -label [trans other] -command "$this askPort"
}
#
proc control_setPort {this args} {
    switch {} $args return
    upvar #0 $this cdata
    set cdata(port) [lindex $args 0]
}
#
proc control_setServer {this args} {
    switch {} $args return
    upvar #0 $this cdata
    set cdata(server) [lindex $args 0]
    set net [$this net]
    $net changeServer [Server :: make $net $cdata(server)]
}
#
proc control_askPort {this} {
    mkEntryBox {} [trans port] {Enter the port number you require:} \
      [list [list port {}]] [list ok "$this setPort"] [list cancel {}]
}
#
proc control_askServer {this} {
    mkEntryBox {} [trans server] {Enter the server name you require:} \
      [list [list server {}]] [list ok "$this setServer"] [list cancel {}]
}

#
proc leaveZircon {} {
    set quits 1
    foreach x [Net :: list] {
	if {[$x active]} { set quits [expr {($quits & [$x quit])}] }
	switch {} [$x confChange] {} default {
	    if {[askUser SAVECONF {Save Configuration} \
	      "You have made changes to the configuration of \
	      netspace \"[$x name].\"\nDo you wish to save them?"]} {
		saveCurrent $x
	    }
	}
    }
    if {$quits} exit
}
#
proc control_redit {this opt} {
    set rd [[$this net] setcolour]
    [$this window].$opt configure -foreground $rd -activeforeground $rd
}
#
proc control_blackit {this opt} {
    set bl [[$this net] foreground]
    [$this window].$opt configure -foreground $bl -activeforeground $bl
}
#
proc flagit {net opt w} {
    set rd [$net setcolour]
    if {[$net $opt]} {$w.$opt configure -foreground $rd -activeforeground $rd }
}
#
proc addToConf {net} {
    set mn [set ctl [MainControl]].helpFrm.info.menu.conf
    if {![catch {$mn.nets index [$net name]}]} return
    $mn.nets add cascade -label [$net name] -menu $mn.nets.$net
    menu $mn.nets.$net -tearoff 0
    $mn.nets.$net add command -label [trans configure] -command "configWin $net"
    $mn.nets.$net add command -label [trans {save current}] -command "saveCurrent $net"
    switch {} [$net helpService] {} default {
	$ctl.helpFrm.help.menu entryconfigure last -state normal
	set hm $ctl.helpFrm.help.menu.help
	if {![winfo exists $hm]} { menu $hm -tearoff 0 }
	$ctl.helpFrm.help.menu.help add command -label [$net name] \
	  -command "getHelp $net" -state disabled
    }
}
#
proc delFromConf {name} {
    set mn [MainControl].helpFrm.info.menu.conf
    if {[catch {$mn.nets index $name} indx]} return
    $mn.nets delete $indx
}
#
proc nickKeys {unusednet win} {
    foreach y [list exclam quotedbl sterling dollar percent ampersand \
      asterisk parenleft parenright plus equal notsign acute grave at apostrophe \
      numbersign asciitilde colon less greater comma period question slash] {
        catch {bind $win <KeyPress-${y}> break}
    }
}
#
proc MainControl {} {
    if {![winfo exists .@ctl]} {
	global zircon zlayout zCtl DEBUG argv0 argv
	set zCtl [Window .@ctl -title {Zircon Control Panel} -resizable {1 1}]
	set ctl [$zCtl name]
	catch {wm geometry $ctl $zlayout(default,control)}
	wm protocol .@ctl WM_DELETE_WINDOW leaveZircon
	wm protocol .@ctl WM_SAVE_YOURSELF "wm command .@ctl {$argv0 $argv}"
	grid columnconfigure .@ctl 0 -weight 1
	grid [set hf [frame $ctl.helpFrm -borderwidth 0]] -sticky ew
	set om [makeMB $hf.info "Zircon V$zircon(version)"]
	$om configure -tearoff 0
	$om add command -label [trans {about zircon}] -command credits
	$om add cascade -label [trans configure] -menu $om.conf
	$om add separator
	$om add command -label [trans debug] -command zDBGControl \
	  -state [expr {$DEBUG ? {normal} : {disabled}}]
	$om add separator
	$om add command -label [trans quit] -command leaveZircon \
	  -accelerator $zircon(meta)-q
	menu $om.conf -tearoff 0
	$om.conf add cascade -label [trans netspaces] -menu $om.conf.nets
	menu $om.conf.nets -tearoff 0
	$om.conf add command -label [trans {save current}] -command "saveCurrent {}"
	$om.conf add command -label [trans {save layout}] -command "saveLayout {}"
	$om.conf add command -label [trans {reread prefs}] -command reread -state disabled
	foreach x {0 1 2 3} {grid columnconfigure $hf $x -weight 1}
	netspaceMenu $hf
	set om [makeMB $hf.help Help]
	set hc 0
	foreach x [glob -nocomplain [file join $zircon(lib) help *]] {
	    set x [file tail $x]
	    set nm m[incr hc]
	    $om add cascade -label $x -menu $om.$nm
	    menu $om.$nm -tearoff 0 -postcommand "makeHelp $om.$nm $x"
	}
	$om add cascade -label "IRC Help Service" -state disabled -menu $om.help
	grid $hf.spaces $hf.info - $hf.help -sticky ew
	bind .@ctl <$zircon(meta)-q> leaveZircon
    }
    return .@ctl
}
#
proc Control {args} {
    global Ops OType defChan zII zNOII
    set this [makeObj Control $args]
    upvar #0 $this cdata
    set net $cdata(net)
    set ctl [set cdata(window) [switchFrame [MainControl] $net [$net name] 0]]
    evenGrid $ctl column 0 3
    $this serverFrame $ctl
#
    addSeparator $ctl -columnspan 4
#
    button $ctl.invisible -text [trans invisible] -command "$net toggleFlag invisible"
    $this NNSBuild $ctl Nickname nickname [$net nicks] $net  $ctl.invisible
    nickKeys $net $ctl.nickname
    set cdata(nfg) [$ctl.nickname cget -foreground]
    if {[$net ircIImode]} { set im $zII } { set im $zNOII }
    button $ctl.ii -command "ntoggleII $net $ctl.ii" -image $im
    set en [$this NNSBuild $ctl IRCName ircname [$net ircnames] $net $ctl.ii]
    bind $en <ButtonPress-3> "
	popPalette .@pp$net {} $en [list [$defChan($net) colour]] %X %Y
    "
    bind $en <Escape> "
    "
#
    button $ctl.away -text [trans away] -command "awayPanel $net"
    bind $ctl.away <2> "$net clearAway {}"

    set cdata(friends) [Friends -control $this -style [$net friendsStyle]]
    if {[$cdata(friends) menu]} {
	menubutton $ctl.friends -text [trans friends] -menu $ctl.friends.menu
	$cdata(friends) show -padx 4 -pady 5
    } {
	button $ctl.friends -text [trans friends] -command "$cdata(friends) show" 
	if {[$net showFriends]} { $ctl.friends invoke }
    }
    if {[info exists Ops(user)]} { $this buildUsers $net $ctl 3}
    set mn [makeMB $ctl.services Services]
    set i 5
    $mn add command -label [trans exec] -command "$net exec"
    $mn add cascade -label [trans script] -state disabled
    $mn add cascade -label [trans plugin] -state disabled
    $mn add separator
    foreach cn [$net services] {
	$mn add cascade -label [$cn name] -menu $mn.$i
	set m [menu $mn.$i -tearoff 0]
	foreach nn [$cn ops] {$m add command -label $nn -command "$cn do $nn"}
	incr i
    }
    grid $ctl.away $ctl.users $ctl.friends $ctl.services -sticky ew
#
    addSeparator $ctl -columnspan 4
    frame $ctl.cmd -borderwidth 0 -relief flat
    grid columnconfigure $ctl.cmd 1 -weight 1
    set cm [makeMB $ctl.channels Channels]
    $cm add command -label [trans favourites] -command "faves $net"
    $cm add separator    
    foreach cmd "Join Who List Mode Names Notice Monitor" {
	$cm add command -label [trans $cmd] \
	  -command "channel$cmd $net \[string trim \$${this}(channel)\]"
    }
    addChanCTCPMenu $cm $this
    $cm add separator
    $this setupChannels
    label $ctl.cmd.label -relief flat -text " [trans channel] "
    emacsEntry $ctl.cmd.channel -relief sunken -textvariable ${this}(channel)
    grid $ctl.channels -in $ctl.cmd -row 0 -column 0
    grid $ctl.cmd.channel -row 0 -column 1 -sticky ew
    grid $ctl.cmd - - - -sticky ew

    bind $ctl.cmd.channel <Return> "channelJoin $net \[$this channel\] {}"
    addToConf $net
    return $this
}
#
# Build the Nickname and Ircname entries for the control window
#
proc control_NNSBuild {this frm lbl var lst net wdw} {
    set name [string tolower $lbl]
    set mn [makeMB $frm.l$name $lbl]
    $this setupNames $name $lbl $net $lst
    emacsEntry $frm.$name -relief sunken -textvariable ${this}($name)
    bind $frm.$name <Return> "$net change$lbl \$${this}($name)"

    grid $frm.l$name $frm.$name - $wdw -sticky ew
    $frm.$name insert end [lindex $lst 0]
    $net configure -$var [lindex $lst 0]
    return $frm.$name
}
#
proc control_setupNames {this name which net lst} {
    set mn [$this window].l$name.menu
    $mn delete 0 end
    foreach x [lsort $lst] {
	$mn add command -label $x -command "$net change$which {$x}"
    }
}
#
proc control_setupChannels {this} {
    set cm [$this window].channels.menu
    set net [$this net]
    while {![catch {$cm entrycget last -label}]} {$cm delete last}
    set dfc [getDefault defChan $net]
    foreach chan [$net channels] {
	switch $dfc $chan continue
	if {[$chan menu]} {
	    $cm add command -label [$chan name] -command "$chan sendJoin"
	}
    }
}
#
proc reread {} {
}
#
proc faves {net} {
    if {[winfo exists [set w .@faves$net]]} {popup $w ; return}
    global defChan
    makeToplevel $w Favourites "killWindow $w" {}
    wm resizable $w 0 1
    scrollbar $w.vscroller -command "$w.chList yview" 
    text $w.chList -yscrollcommand "$w.vscroller set" -width 14 \
      -height 12
    bindtags $w.chList none
    button $w.ok -text [trans dismiss] -command "killWindow $w"
    grid $w.chList $w.vscroller  -sticky ns
    grid $w.ok -
    set dfc [getDefault defChan $net]
    foreach chan [$net channels] {
	switch $dfc $chan continue
	if {[$chan menu]} {
	    set fm $w.chList.$chan
	    set cm [makeMB $fm [$chan name]]
	    $cm configure -postcommand "favecheck $cm $chan"
	    foreach cmd "Join Who Names Notice Monitor" {
		$cm add command -label $cmd \
		  -command "channel${cmd} $net [$chan name]"
	    }
	    addCTCPMenu $net $cm $chan
	    $w.chList window create end -window $fm
  	}
    }
}
#
proc favecheck {gmenu chan} {
    if {[$chan active]} {
	foreach e {4 6} { $gmenu entryconfigure $e -state normal }
	foreach e {1 3 5} { $gmenu entryconfigure $e -state disabled }
    } {
	foreach e {4 6} { $gmenu entryconfigure $e -state disabled }
	foreach e {1 3 5} { $gmenu entryconfigure $e -state normal }
    }
}
#
proc keepLeave {chan v} {
    switch {} $v return
    [$chan window].quit.menu add command -label "[prune $v 15]" \
      -command "$chan doLeave [list $v]"
    [$chan net] configure +leaves $v +confChange Channels
}
#
proc getLeave {chan} {
    mkEntryBox {} [trans leave] {Enter your new leaving message:} \
      [list [list message {} {} palette]] [list keep "keepLeave $chan"] \
      [list leave "$chan doLeave"] [list cancel {}]
}
#
proc keepQuit {ctl v} {
    switch {} $v return
    set net [$ctl net]
    [$ctl window].quit.menu add command -label "[prune $v 15]" \
      -command "$net doQuit [list $v]"
    $net configure +signoffs $v +confChange Info
}
#
proc getQuit {ctl} {
    mkEntryBox {} [trans quit] {Enter your new signoff message:} \
      [list [list signoff {} {} palette]] [list keep "keepQuit $ctl"] \
      [list quit "[$ctl net] doQuit"] [list cancel {}]
}
#
proc control_ircItems {this state} {
    set net [$this net]
    foreach cid [$net channels] { $cid ircOp $state }
    set ctl [$this window]
    setState $ctl.servers.menu ircSrv $state
    setState $ctl.users.menu ircop $state
    switch $state {
    normal {$ctl.servers configure -fg [$net setcolour]}
    disabled {$ctl.servers configure -fg [$net foreground]}
    }
}
#
proc control_add2.9stuff {this} {
    global ch29
    set w [$this window]
    set net [$this net]
    set mn $w.servers.menu
    if {[$mn cget -tearoff]} { set ix 1 } { set ix 0 }
    if {![winfo exists $mn.msgs]} {
	$mn insert $ix cascade -label [trans messages] -menu $mn.msgs
	menu $mn.msgs -tearoff 0
	foreach cn $ch29 {
	    $mn.msgs add command -label $cn -command "channelJoin $net $cn {}"
	}
    }
    grid forget $w.srvmsg
    grid forget $w.wallops
    grid $w.wallops - -row [$this smrow] -column 2 -sticky ew
    $w.services.menu insert 5 command -label Squery -command "$net squery"
    $w.services.menu insert 6 command -label Servlist -command "$net servlist"
}
#
proc control_del2.9stuff {this} {
    upvar #0 $this cdata
    set mn [$this window].servers.menu
    if {[$mn cget -tearoff]} { set ix 1 } { set ix 0 }
    if {[winfo exists $mn.msgs]} {
	$mn delete [$mn index [trans messages]]
	destroy $mn.msgs
    }
    set w [ $this window]
    grid forget $w.wallops 
    grid $w.wallops -row $cdata(smrow) -column 2 -sticky ew
    grid $w.srvmsg -row $cdata(smrow) -column 3 -sticky ew
    if {![catch {set x [$w.services.menu index Squery]}]} {
	$w.services.menu delete $x
	$w.services.menu delete [$w.services.menu index Servlist]
    }
}
#
proc control_undernet {this flag} {
    set smn [$this window].servers.menu
    if {[$smn cget -tearoff]} { set st 1} { set st 0 }
    set six [indexHack $smn [trans flags] $st]
    set umn [$this window].users.menu
    if {$flag} {
	if {$six < 0} {
	    $smn insert $st command -label [trans flags] \
	      -command "underflags [$this net]"
	    $smn insert [expr {$st + 2}] command -label [trans map] \
	      -command "[$this net] q1Send MAP"
	    $umn insert 4 command -label [trans silence] \
	      -command "underSilence [$this net]"
	}
    } {
	if {$six >= 0} {
	    $smn delete $six
	    $smn delete [$smn index [trans map]]
	    $umn delete [$umn index [trans silence]]
	}
    }
}
#
proc control_setupServer {this net} {
    set sm [$this window].server.menu
    set sl {}
    foreach nn [$net servers] {	lappend sl [list [$nn host] $nn] }
    $sm delete 0 end
    foreach nn [lsort $sl] {
	$sm add command -label [lindex $nn 0] -command "$this pickServer [lindex $nn 1]"
    }
    $sm add separator
    $sm add command -label [trans other] -command "$this askServer"
    switch nil [set srv [$net hostid]] {
	switch {} [set srv [lindex [lindex $sl 0] 1]] {set srv nil}
    }
    return $srv
}
#
proc control_open {this} {
    upvar #0 $this cdata
    $this disableOpen
    $cdata(net) startIRC [$cdata(net) hostid] $cdata(port)
}
#
proc control_serverFrame {this ctl} {
    global Ops
    addSeparator $ctl -columnspan 4
#
    set nf $ctl
    set net [$this net]
    frame $nf.rfr -borderwidth 0
    label $nf.rfr.restricted -text {RESTRICTED CONNECTION} -fg [$net setcolour]
    grid $nf.rfr - - - -sticky ew
    set sm [tk_optionMenu $nf.server ${this}(server) {}]
    set srv [$this setupServer $net]
    set qm [buttonmenu $nf.quit -command "$this open" \
      -text [trans open] -padx 5 -highlightthickness 0 -state disabled]
    menu $qm.menu -tearoff 0
    $qm.menu add command -label [trans new] -command "getQuit $this" 
    $qm.menu add separator
    foreach x [$net signoffs] {
	$qm.menu add command -label [prune $x 15] -command "$net doQuit {$x}"
    }
    tk_optionMenu $nf.port ${this}(port) 6667
    $nf.port configure -width 4 -highlightthickness 0
    grid $nf.server - $nf.port $nf.quit -sticky ew
    $this showServer $srv
    if {[info exists Ops(server)]} { $this buildServers $net $nf 1 0}
    button $nf.wallops -text Wallops -command "$net toggleFlag wallops"
    button $nf.srvmsg -text SrvMsg -command "$net toggleFlag srvmsg"
    if {[$net nowallop] || ($srv != {nil} && [$srv nowallop])} {
	grid $nf.servers - $nf.srvmsg - -sticky ew
    } {
	grid $nf.servers - $nf.wallops $nf.srvmsg -sticky ew
    }
    $this configure -smrow [expr {[lindex [grid size $ctl] 1] - 1}]
    grid [frame $nf.svc -borderwidth 0] - - - -sticky ew
}
#
proc control_buildServers {unusedthis net ctl unusedrow unusedcol} {
    global Ops zircon
    switch $zircon(serversStyle) {
    panel {
	    set f [switchFrame $ctl.srv $net [trans servers] 0]
	}
    pulldown {
	    set mn [makeMB $ctl.servers "Server Information"]
	    foreach cmd $Ops(server) {
		$mn add command -label [trans $cmd] \
		  -command "serverCmd $net $cmd"
	    }
	    if {[info exists Ops(ircSrv)]} {
		set rd [$net setcolour]
		foreach cmd $Ops(ircSrv) {
		  $mn add command -label [trans $cmd] -foreground $rd \
		    -activeforeground $rd -command "serverCmd $net $cmd"
		}
	    }
	    if {![$net ircop]} { setState $ctl.servers.menu ircSrv disabled }
	}
    }
}
#
proc control_buildUsers {unusedthis net ctl unusedrow} {
    global Ops zircon
    switch $zircon(usersStyle) {
    pulldown {
	    set mn [makeMB $ctl.users Users]
	    foreach cmd $Ops(user) {
		switch -exact -- $cmd {
		DCC {
		       $mn add cascade -label [trans dcc] -menu $mn.dcc
			menu $mn.dcc -tearoff 0
			foreach nn {List Send Chat Close} {
			    $mn.dcc add command -label [trans $nn] \
			      -command "usersDCC $net $nn"
			}
			$mn.dcc add cascade -label [trans auto] -menu $mn.dcc.menu
			menu $mn.dcc.menu -tearoff 0
			$mn.dcc.menu add command -label [trans users] \
			   -command "dccSetAutoUser $net"
			$mn.dcc.menu add command -label [trans directory] \
			   -command "dccSetAutoDir $net" -state disabled
		    }
		CTCP { addCTCPMenu $net $mn {{}} }
		default {
			$mn add command -label [trans $cmd] \
			  -command "userCmd $net $cmd"
		    }
		}
	    }
	    if {[info exists Ops(ircop)]} {
		set rd [$net setcolour]
		foreach cmd $Ops(ircop) {
		    $mn add command -label [trans $cmd] -foreground $rd \
		      -activeforeground $rd -command "userCmd $net $cmd"
		}
	    }
	    if {![$net ircop]} { setState $ctl.users.menu ircop disabled }
	}
    }
}
#
proc control_startup {this} {
    set ctl [$this window]
    set cl [[$this net] foreground]
    foreach x {wallops invisible srvmsg} {
	$ctl.$x configure -foreground $cl -activeforeground $cl
    }
}
#
proc nsSubMenu {m name w} {
    set mn [menu $m.$w -tearoff 0]
    $mn add command -label [trans load] -command "nsLoad [list $name] 1 {}"
    $mn add command -label [trans unload] -command "nsUnload [list $name]" -state disabled
    $mn add command -label [trans clone] -command "nsClone [list $name]"
    $mn add command -label [trans delete] -command "nsDelete [list $name]"
}
#
proc addToNsM {name} {
    set m [MainControl].helpFrm.spaces.menu
    set i 0
    while {1} {
	if {[catch {set nm [$m entrycget $i -label]}]} break
	if {[string compare $name $nm] < 0} break
	incr i
    }
    set wname [newName nsh]
    $m insert $i cascade -label $name -menu $m.$wname
    nsSubMenu $m $name $wname
}
#
proc delFromNsM {name} {
    set m [MainControl].helpFrm.spaces.menu
    set i 0
    while {1} {
	if {[catch {set nm [$m entrycget $i -label]}]} return
	switch -- $name $nm break
	incr i
    }
    destroy [$m entrycget $i -menu]
    $m delete $i
}
#
proc netspaceMenu {w} {
    global zircon
    set pf1 {}
    set pf2 {}
    [set om [makeMB $w.spaces Netspaces]] configure -tearoff 0
    if {![file exists [set ns [file join $zircon(prefdir) netspaces]]]} {
	$w.spaces configure -state disabled
    } {
	set cnt 0
	foreach x [lsort [glob -nocomplain [file join $ns *]]] {
	    if {[incr cnt] >= 20} {
		set cnt 0
		$om add cascade -label [trans more] \
		  -menu [set m [menu $om.more -tearoff 0]]
		set om $m
	    }
	    addNS $om $x
	}
	$om add separator
    }
}
#
proc addToNM {name} { addNS [MainControl].helpFrm.spaces.menu $name }
#
proc setUnload {name state} {
    set m [MainControl].helpFrm.spaces.menu
    set i 0
    while {1} {
	if {[catch {set nm [$m entrycget $i -label]}]} return
	switch -- $name $nm break
	incr i
    }
    [$m entrycget $i -menu]  entryconfigure 1 -state $state	;# Unload
}
#
proc addNS {om x} {
    global zircon
    set fn [file tail $x]
    foreach x $zircon(nsignore) {
        if {[string match $x $fn]} return
    }
    set w [newName nsh]
    $om add cascade -label $fn -menu $om.$w
    nsSubMenu $om $fn $w
}
#
proc control_channel {this} { upvar #0 $this cdata ; return $cdata(channel) }
#
proc control_flagState {this state} {
    set ctl [$this window]
    set net [$this net]
    switch {} [$net helpService] {} default {
	set hm [MainControl].helpFrm.help.menu.help
	set ix [$hm index [$net name]]
	$hm entryconfigure $ix -state $state
    }
    foreach x [winfo children $ctl] {
	foreach y [winfo children $x] {catch {$y configure -state $state }}
	catch {$x configure -state $state}
    }
    switch $state normal {} disabled {
	foreach x {server nickname lnickname ircname lircname port quit} {
	    $ctl.$x configure -state normal
	}
	$this lagClear
    }
    [$this friends] flagState $state
}
#
proc control_setQuit {this txt cmd} {
    catch {[$this window].quit configure -text [trans $txt] \
       -command $cmd -state normal}
}
#
proc control_disableOpen {this} {
    [$this window].quit configure -state disabled
}
#
proc control_enableOpen {this} {
    [$this window].quit configure -state normal
}
#
proc control_setOpen {this state} {
    [$this window].quit configure -state $state
}
#
proc control_openState {this} {
    return [[$this window].quit cget -state]
}
#
proc control_delete {this} {
    deleteFrame [MainControl] [$this net]
    [$this friends] delete
    uplevel #0 unset $this
    rename $this {}
}
#
proc control_flagSet {this flg val} {
    if {$val} {
	set cl setcolour
    } {
	set cl foreground
    }
    set cl [[$this net] $cl]
    [$this window].$flg configure -foreground $cl -activeforeground $cl
}
#
proc control_restricted {this val} {
    set net [$this net]
    if {$val} {
	grid [$this window].rfr.restricted -sticky ew
    } {
	grid forget [$this window].rfr.restricted
    }	
}
#
proc ntoggleII {net win} {
    global zII zNOII
    if {[$net ircIImode]} {
	set im $zNOII
	set val 0
    } {
	set val 1
	set im $zII
    }
    $net configure -ircIImode $val
    $win configure -image $im
    foreach y {channels messages notices} {
	foreach x [$net $y] {
	    if {[$x ircIImode] != $val} {
		if {[$x active]} {
		    toggleII $x [$x window].cmd.ii
		} {
		    $x configure -ircIImode $val
		}
	    }
	}
    }
}
#
proc control_flagNick {this} {
    [$this window].nickname configure -foreground red
}
#
proc control_confirmNick {this nk} {
    upvar #0 $this cdata
    set cdata(nickname) $nk
    [$this window].nickname configure -foreground $cdata(nfg)
}
