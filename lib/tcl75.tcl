#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/tcl75.tcl,v $
# $Date: 2001/07/20 06:52:42 $
# $Revision: 1.18.1.25 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide interp 7.5
#
proc sconf {sk enc} {
    global hostIPaddress
    fconfigure $sk -buffering line -translation auto -blocking 0
    if {![info exists hostIPaddress]} {
	if {![catch {fconfigure $sk -sockname} xx]} {
	    set hostIPaddress [lindex $xx 0]
	}
    }
}
#
proc filecopy {f1 f2} { exec cp $f1 $f2 }
#
proc filerename {f1 f2} { exec mv $f1 $f2 }
#
proc filemkdir {f1} { exec mkdir $f1 }
#
proc filedelete {f1} { exec rm $f1 }
#
proc filedirdel {f1} { exec rm -fr $f1 }
#
proc sendSound {net nk} {
    global SoundDir zircon
    set msg "Play sound file for $nk"
    if {![info exists SoundDir($net)]} {
	if {[file exists [file join $zircon(prefdir) sounds]]} {
	    set SoundDir($net) $zircon(prefdir)/sounds
	} {
	    set SoundDir($net) [file join $zircon(lib) sounds]
	}
    }
    tkwait window [mkFileBox {} SoundDir($net) {.*\.[wW][aA][vV]} \
	[trans sound] $msg {} \
	"send {doSendSound $net $nk}" "cancel {}"]
}
#
proc native {fl} { return $fl }
#
proc noCase {t1 t2} {
    return [string compare [string tolower $t1] [string tolower $t2]]
}
#
proc ncsort {lst} { return [lsort -command noCase $lst] }
#
proc safeUnset {args} { foreach x $args { catch {uplevel #0 unset [list $x]} }}
