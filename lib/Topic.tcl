#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Topic.tcl,v $
# $Date: 2001/07/10 15:36:12 $
# $Revision: 1.18.1.8 $
#
package provide zircon 1.18
#
# Topic handling procs
#
proc keepTopic {this value} {
    switch {} $value return
    set chan [$this name]
    regsub -all "\[\r\n\]" $value {} value
    $this configure -topic $value
    set t [$this topics]
    if {[lsearch $t $value] < 0} {
	lappend t $value
	$this configure -topics [lsort $t]
	[$this window].topic.label.menu add command \
	  -label "[prune $value 15]" \
	  -command "$this configure -topic {$value}"
	if {[$this keep]} { [$this net] configure +confChange Channels }	
    }
}
#
proc getTopic {this} {
    set chan [$this name]
    mkEntryBox .@tp$this "$chan Topic" "Enter your new topic for $chan:" \
      [list [list topic {}]] [list ok "$this configure -topic"] \
      [list keep "keepTopic $this"] [list cancel {}]
}
#
proc sendTopic {this win} {
    if {[normal $win]} {$this configure -topic [$win get 1.0 end]}
}
#
proc irc331 {net unusedprefix unusedparam pargs} {
    switch nil [set chn [Channel :: find [lindex $pargs 1] $net]] return
    $chn setTopic {}
}
#
proc irc332 {net unusedprefix param pargs} {
    switch nil [set chn [Channel :: find [lindex $pargs 1] $net]] return
    $chn setTopic $param
}
