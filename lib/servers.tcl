#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/servers.tcl,v $
# $Date: 2001/06/13 07:20:28 $
# $Revision: 1.18.1.22 $
#
package provide zircon 1.18
#
#  Server Operations
#
proc sendStats {net srv param var win} {
    upvar #0 $var array
    if {$param == {}} {
        set val $array($win)
    } {
        set val $param
    }
    $net STATS $val $srv
    unset array($win)
}
#
proc askStats {net def srv} {
    switch -glob [$net sVersion] 2.9* {
	set ops {c d h k i l L m o p q s t u y z}
	set dfl d
    } default {
	set ops {c h k i l L m o q t u y z}
	set dfl c
    }
    mkRadioBox .@Stats Stats "Enter Stats parameters $def:" \
      $ops $dfl [list [list server $srv] [list parameter {}]] \
      [list ok "sendStats $net"] [list cancel {}]
}
#
proc serverCmd {net cmd} {
    global zircon
    set srv [[set hst [$net hostid]] host]
    set def "(Default host is $srv)"
    switch $cmd {
    Oper {
	    if {[$net ircop]} {
		if {[askUser {} {IRC OP} {Really stop being an IRC Operator?}]} {
		    $net deIRCOp
		}
	    } {
		set opStuff [$hst oper]
		set nk [lindex $opStuff 0]
		set pw [lindex $opStuff 1]
		mkEntryBox .@oper Oper "Enter name and password for $srv:" \
		  [list [list name [expr {$nk == {} ? [[$net myid] name] : $nk}]] \
		  [list password $pw {} secret]] [list ok "$net OPER"] [list cancel {}]
	    }
	}
    Rehash -
    Restart {
    	    set ucmd [string toupper $cmd]
	    mkDialog $ucmd .@$cmd $cmd "Really $cmd $srv?" \
	     {}  [list ok "$net q1Send $ucmd"] [list cancel {}]
	}
    Stats { askStats $net $def {}}
    Links {
	    global linksInfo
	    set linksInfo($net) {}
	    mkEntryBox {} [trans links] "Enter Links parameters $def:" \
	      [list [list server {}] [list Mask {}]] \
	      [list ok "$net LINKS"] [list cancel {}]
	}
    Connect {
	    mkEntryBox {} [trans connect] "Enter Connect parameters $def:" \
	      [list [list server {}] [list port {}] [list Remote {}]] \
	      [list ok "$net CONNECT"] [list cancel {}]
	}
    Info {
	    global infoInfo
	    set infoInfo($net) {}
	    mkEntryBox {} [trans info] "Enter Server name $def:" \
	      [list [list server {}]] [list ok "$net nsend INFO"] \
	      [list cancel {}]
	}
    Squit {
	    mkEntryBox {} [trans squit] {Enter Server name :} \
	      [list [list server {}]] [list ok "doSquit $net"] \
	      [list cancel {}]
	}
    Trace {
	    mkEntryBox {} [trans Trace] "Enter server name $def:" \
	      [list [list server {}]] [list ok "$net nsend TRACE"] \
	      [list cancel {}]
	}
    Motd {
	    mkEntryBox {} MOTD "Enter server name $def:" \
	      [list [list server {}]] [list ok "$net nsend MOTD"] \
	      [list cancel {}]
	}
    default {
	    set ucmd [string toupper $cmd]
	    mkEntryBox {} $cmd "Enter server pattern $def:" \
	      [list [list server {}]] \
	      [list ok "$net nsend $ucmd"] [list cancel {}]
	}
    }
}
#
proc doSquit {net srv} {
    switch {} $srv return
    tkwait window [mkEntryBox {} SQUIT "Really Squit server \"$srv\"?" \
      [list [list message {}]] [list ok "$net SQUIT $srv"] [list cancel {}]]
}
#
proc kill {net who} {
    mkDialog {} {} [trans kill] "Really kill $who?" \
      [list [list message {}]] [list ok "doKill $net [list $who]"] \
      [list cancel {}]
}
#
proc doKill {net nk msg} {switch {} $nk {} default {$net KILL $nk $msg}}
#
proc userKill {net} {
    mkEntryBox {} [trans kill] {Enter user name and message:} \
      [list [list user {}] [list message {}]] \
      [list ok "doKill $net"] [list cancel {}]
}
#
proc irc200 {net prefix param pargs} {
    tracewin t $net $prefix $param $pargs [lindex $pargs 3]
}
proc irc201 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc202 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc203 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc204 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc205 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc206 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc207 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc208 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc209 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc210 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc261 {net prefix param pargs} { tracewin t $net $prefix $param $pargs }
proc irc262 {net prefix param pargs} {
    global twin
    tracewin t $net $prefix $param $pargs
    catch {unset twin($net)}
}

#
proc irc211 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc212 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc213 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc214 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc215 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc216 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc217 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc218 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc219 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc241 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc242 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc243 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc244 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc245 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc246 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc248 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
proc irc249 {net prefix param pargs} { tracewin s $net $prefix $param $pargs }
#
proc tracewin {op net prefix param pargs args} {
    set prefix [string range $prefix 1 end]
    switch {} $args {
	set tsite $prefix
    } default {
	set tsite [lindex $args 0]
    }
    set tsite [string tolower $tsite]
    switch -exact -- $op {
    t	{ set var twin ; set type Trace ; set cmd "$net q1Send {TRACE :$tsite}"}
    s	{ set var swin ; set type Stats ; set cmd "askStats $net {} $tsite"}
    }
    upvar #0 $var wstat
    if {![info exists wstat($net)]} {
	makeToplevel [set w .@[newName trace]] "$type for $tsite" \
	  "destroy $w ; catch {unset ${var}($net)}" {}
	grid columnconfigure $w 0 -weight 1
	grid rowconfigure $w 0 -weight 1
	scrollbar $w.vs -command "$w.txt yview"
        text $w.txt -yscrollcommand "$w.vs set" -width 80 -height 10 \
	  -takefocus 0
	bindtags $w.txt ROText
	grid $w.txt -row 0 -column 0 -sticky nsew
	grid $w.vs -row 0 -column 1 -sticky ns
	grid [frame $w.f2] - -sticky ew
	grid [button $w.f2.xit -text [trans dismiss] -command "destroy $w ; \
	   catch {unset ${var}($net)}" -width 10] \
	  [button $w.f2.trc -text [trans $type] \
	  -command $cmd -width 10] -sticky ew
	$w.txt insert end "[getDate]\n"
	set wstat($net) $w
    } {
	set w $wstat($net)
    }
    $w.txt insert end "$prefix :   "
    foreach a [lrange $pargs 1 end] {
	switch {} $a {} default {$w.txt insert end " $a"}
    }
    switch {} $param {} default {$w.txt insert end " $param"}
    $w.txt insert end "\n"
}
#
proc irc219 {net prefix param pargs} { }
#
proc irc364 {net prefix param pargs} {
    global linksInfo
    append linksInfo($net) "[lindex $pargs 1] [lindex $pargs 2] ${param}\n"
}
#
proc irc365 {net prefix param pargs} {
    global linksInfo
    if {![info exists linksInfo($net)]} { set linksInfo($net) {No Links} }
    mkInfoBox $net LINKS {} "[string range $prefix 1 end] Links [getDate]" $linksInfo($net)
    unset linksInfo($net)
}
#
proc irc371 {net prefix param pargs} {
    global infoInfo
    append infoInfo($net) "${param}\n"
}
#
proc irc374 {net prefix param pargs} {
    global infoInfo
    mkInfoBox $net INFO {} "[string range $prefix 1 end] Info [getDate]" $infoInfo($net)
    unset infoInfo($net)
}
