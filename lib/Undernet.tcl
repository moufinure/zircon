#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Undernet.tcl,v $
# $Date: 2001/07/10 15:36:12 $
# $Revision: 1.18.1.16 $
#
package provide zircon 1.18
#
# Undernet specific calls - 5, 6 and 7 handle the MAP command.
#
proc underInit {net} {
    $net configure -undernet 1
    $net MODE [$net nickname] +ss +2147483648
}
#
proc irc005 {net prefix param pargs} {
    uplevel #0 "lappend umap($net) {$param}"
}
#
proc irc006 {net prefix param pargs} {
    uplevel #0 "lappend umap($net) {$param}"
}
#
proc irc007 {net prefix param pargs} {
    global umap
    if {![winfo exists [set w .@map$net]]} {
	makeToplevel $w "Undernet Map from [$net host]" {} {}
	scrollbar $w.vs -command "$w.txt yview"
	scrollbar $w.hs -command "$w.txt xview" -orient horizontal
	text $w.txt -yscrollcommand "$w.vs set" -wrap none \
	  -xscrollcommand "$w.hs set" -width 30
	grid rowconfigure $w 0 -weight 1
	grid columnconfigure $w 0 -weight 1
	grid $w.txt -row 0 -column 0 -sticky nsew
	grid $w.vs -row 0 -column 1 -sticky ns
	grid $w.hs -row 1 -column 0 -sticky ew
	set f [frame $w.f1]
	button $f.ok -text [trans dismiss] \
	  -command "destroy .@map$net" -width 10
	button $f.map -text [trans map] -command "$net q1Send MAP" -width 10
	grid $f.ok $f.map -row 0
	grid $f -row 2 -columnspan 2 -sticky ew
    }
    $w.txt delete 0.0 end
    foreach x $umap($net) { $w.txt insert end "$x\n" }
    unset umap($net)
}
#
proc underList {net chan} {
    set min [$net minMembers]
    set max [$net maxMembers]
    set par {}
    if {$max > 0} { set par <[incr max],}
    switch {} $chan {
	$net q1Send "LIST :$par>[incr min -1]"
    } default {
	$net q1Send "LIST $par>[incr min] :$chan"
    }
}
#
proc underSilence {net} {
    mkEntryBox .@sil$net [trans silence] \
      {Enter user pattern:} [list [list user {}]] \
      [list silence "doSilence + $net"] \
      [list delete "doSilence - $net"] [list cancel {}]
}
#
proc doSilence {op net args} {
   switch {} $args return
   $net q1Send "SILENCE :$op[lindex $args 0]"
}
#
proc underUSilence {op net usr} {  $net q1Send "SILENCE :$op[$usr name]" }
#
proc ircSILENCE {net prefix param pargs} {
    switch -glob -- [set v [lindex $pargs 0]] {
    -* {set txt unsilenced}
    +* {set txt silenced}
    }
    $net inform "User [string range $v 1 end] $txt."
}
#
proc irc249 {net prefix param pargs} {
    global underflags
    upvar #0 und$net und
    set flag [lindex $param 2]
    foreach x [array names underflags] {
	scan $underflags($x) %x val
	if {$flag & $val} {
	    set und($x) $val
	} {
	    set und($x) 0
	}
    }
}
#
proc underflags {net} {
    if {[winfo exists [set w .@und$net]]} { popup $w ; return }
    global underflags zircon
    makeToplevel $w "Server message flags for [$net name]" "underreset $net" {}
    wm resizable $w 0 0
    upvar #0 und$net und undt$net tmp
    array set tmp [array get und]
    set row 0
    set col 0
    foreach x [array names underflags] {
	scan $underflags($x) %x val
	set cb [checkbutton $w.[string tolower $x] -text $x \
	  -variable undt${net}($x) -on $val -off 0]
	grid $cb -row $row -column $col -sticky w -padx 5 -pady 5
	if {[incr col] == 4} { set col 0 ; incr row }
    }
    grid [frame $w.sep -bg $zircon(sepColor) -borderwidth 2] \
      -sticky ew -columnspan 4 -pady 5
    grid [frame $w.btn] -columnspan 4 -sticky ew -pady 5
    button $w.btn.ok -text [trans ok] \
      -command "undersend $net" -width 8
    button $w.btn.dflt -text [trans default] \
      -command "underdefault $net" -width 8
    button $w.btn.dismiss -text [trans dismiss] \
      -command "underreset $net" -width 8
    grid $w.btn.ok $w.btn.dflt $w.btn.dismiss -sticky ew
}
#
proc undersend {net} {
    upvar #0 und$net und undt$net tmp
    array set und [array get tmp]
    set flag 0
    foreach x [array names und] {
	set flag [expr {$flag | $und($x)}]
    }
    $net MODE [$net nickname] +s $flag
    unset tmp
    destroy .@und$net
}
#
proc underdefault {net} {
    $net MODE [$net nickname] +s
    catch {uplevel #0 unset undt$net}
    destroy .@und$net
}
#
proc underreset {net} {
    catch {uplevel #0 unset undt$net}
    destroy .@und$net
}
