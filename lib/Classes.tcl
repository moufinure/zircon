#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Classes.tcl,v $
# $Date: 2001/06/11 12:02:59 $
# $Revision: 1.18.1.103 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 1993-2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide zircon 1.18
#
proc InitClasses {} {
    dispAd {Initialising classes...}
#
class Channel {
    selfpattern	0	{bool {My msg check for -patterns}}
    timestamp	0	{bool {Display Timestamps}}
    noshare	0	{bool {Do Not Share}}
    nomerge	0	{bool {Do Not Merge}}
    monitor	0	{bool}
    menu	0	{bool {Add to Channel Menu}}
    join	0	{bool {Join at Startup}}
    nocase	0	{bool {Ignore nick case}}
    nosort	0	{bool {Do not sort nicks}}
    nopriv	0	{bool {Do not show ops first}}
    noshow	0	{bool}
    open	0	{bool {Pop up when active}}
    close	0	{bool {Close if inactive}}
    draw	1	{bool {Enable whiteboard}}
    jump	1	{bool {Scroll}}
    quiet	0	{bool {No Bell}}
    actions	0	{bool {Action mode}}
    buttons	1	{bool {Show Name Buttons}}
    closeleave	0	{bool {Leave if inactive}}
    colour	{}	{checks {mirc ansi ctcp2} {Colour Support}}
    crypt	{}	{sedkey {}}
    key		{}	{key {}}
    logfile	{}	{file {}}
    history	10	{int {}}
    scrollback	50	{int {}}
    closetime	0	{mseconds {}}
    patterns	{}	{patterns {}}
    messages	{}	{texts {}}
    topics	{}	{texts {}}
    ops		{}	{users {}}
    ban		{}	{users {}}
    msg		{}	{list {Join Kick Kill Leave Lost Back Mode Quit Topic} {}}
    bindings	{}	{special {}}
    icon	{}	{icon {}}
    foreground	{}	{colour {}}
    background	{}	{colour {}}
    font	{}	{font {}}
    boldfont	{}	{font {}}
    geometry	{}	{geometry {}}
    height	24	{int {}}
    width	80	{int {}}
    timeformat  {%m/%d/%Y %H:%M:%S}	{format {}}
    joinat	{}	{time {}}
    mytag	{>}	{prompt {}}
    logtag	{>}	{prompt {}}
    iconify	{iconify} {operator {}}
    nickactions	{}	{}
} {
    name	{}
    lname	{}
    limit	{}
    text	{}
    ufrm	{}
    stampMsg	0
    logdir	{}
    logactual	{}
    drawdir	{}
    modewait	0
    users	{}
    splitusers	{}
    net		{}
    sys		0
    actionmode	0
    keep	0
    logfd	{}
    window	{}
    wid		{}
    closemsec	0
    a		0
    i		0
    m		0
    n		0
    p		0
    q		0
    s		0
    t		0
    lcla	0
    lcli	0
    lclm	0
    lcln	0
    lclp	0
    lclq	0
    lcls	0
    lclt	0
    ircIImode	0
    histbuff	{}
    hpos	-1
    mwin	{}
    dwin	{}
    leaving	0
}
#
class Message {
    buttons	0	{bool}
    width	80	{int}
    height	10	{int}
} {
    away	{}
}
#
class Notice {
    buttons	0	{bool}
    draw	0	{bool}
    width	80	{int}
    height	10	{int}
} {
    away	{}
}
#
class DChat {
    buttons	0	{bool}
    draw	0	{bool}
    height	10	{int}
    width	80	{int}
    mytag	=	{prompt}
} {
    caller	nil
    sock	{}
}
#
class Server {
    host	{}	{host}
    port	6667	{port}
    ip		{}	{ipaddr}
    oper	{}	{id}
    operpw	{}	{pass}
    script	{}	{script}
    passwd	{}	{pass}
    invisible	0	{bool}
    wallops	0	{bool}
    nowallop	0	{bool}
    srvmsg	0	{bool}
    undernet	0	{bool}
    irc2.9	0	{bool}
    encoding	{}	{string}
} {
    name	{}
    sys		0
    net		{}
    selport	6667
}
#
#
class Info {
    icon	{}	{icon}
} {
    net		{}
}
#
class Service {
    host	{}	{host}
    nick	{}	{nick}
    ops		{}	{texts}
} {
    addr	{}
    name	{}
    net		{}
    sys		0
}
#
class Window {
} {
    name	{}
    closecount	0
    closetime	0
    children	{}
    timed	1
    iconop	{iconify}
    cols	1
    curcol	0
}
#
class User {} {
    name	{}
    lname	{}
    channels	{}
    refcount	0
    crypt	{}
    net		{}
    fobj	nil
    menu	0
    notify	0
    id		{}
}
#
class Friend {
    notify	1	{bool}
    menu	1	{bool}
    id		{}	{userhost}
    protect	{}	{list}
    image	{}	{filename}
    actions	{}	{patterns}
} {
    nick	{}
    lnick	{}
    net		{}
    usr		nil
    keep	1
    ison	0
    limbo	0
    images	{}
}
#
global zircon
class Net "
    beep	BEEP	{text}
    busymsg	{I am busy and am not accepting calls at the moment.} {text}
    players	{}	{list}
    setcolour	red	{}
    foreground	$zircon(fg)	{colour}
    activefg	red	{colour}
    activebg	$zircon(bg)	{colour}
    passivefg	$zircon(fg)	{colour}
    passivebg	$zircon(bg)	{colour}
    balloonfg	black	{colour}
    balloonbg	yellow	{colour}
    myaddr	{}	{host}
    myport	{}	{portnumber}
    multion	0	{bool {Multiple Ons}}
    nosplit	0	{bool {Don't Handle Splits}}
    ircIImode	0	{bool {Support / Commands}}
    ircIIops	0	{bool {Show ops using @}}
    command	0	{bool {Info Command Line}}
    raw		0	{bool {Allow Raw IRC}}
    wallops	0	{bool {See Wallops}}
    nowallop	0	{bool {No support for Wallop}}
    srvmsg	0	{bool {See Server Messages}}
    invisible	0	{bool {Invisible}}
    showFriends 0	{bool {Show Friends Window}}
    friendsOn	1	{bool {Notify All friends}}
    verboseCTCP	0	{bool {Verbose CTCP}}
    noRefresh	1	{bool {No Auto Channel List}}
    killPath	1	{bool {Kill Path}}
    noPopup	0	{bool {Flag Pop Up}}
    popInfo	0	{bool {Pop Up Info}}
    undernet	0	{bool {Undernet Server}}
    srvcycle	0	{bool {Try All Servers}}
    lagmeter	0	{Bool {Display Lag Meter}}
    autoclearaway 0	{bool}
    cleargone   0	{bool}
    norestrict	0	{bool}
    images	0	{bool}
    busy	0	{bool}
    reconnect	0	{bool}
    nocontrol	0	{bool}
    monitorIn	0	{bool}
    monitorOut	0	{bool}
    helpService	{}	{nick}
    showPublic	1	{bool}
    showLocal	1	{bool}
    showPrivate	0	{bool}
    topicOnly	0	{bool}
    minMembers	3	{int}
    maxMembers	0	{int}
    sorted	0	{bool}
    sortNames	1	{bool}
    nocase	0	{bool}
    integrate	0	{bool}
    listPattern .*	{pattern}
    topicPattern	.*	{pattern}
    friendsStyle	window	{option {window menu}}
    namewidth	12	{int}
    noConfirm	{}	{list {Quit Leave Kill SaveConf}}
    toInfo	{}	{list {Ctcp Signoff Who Whois Whowas Error Ison Info Kill Close}}
    alwayslog	{}	{list {Ctcp Signoff Who Whois Whowas Error Ison Info Kill Close}}
    autoget	{}	{users}
    autogetdir	{}	{directory}
    autochat	{}	{users}
    ctcpPattern	.+	{pattern}
    monitorTime	60000	{mseconds {Monitoring Interval}}
    notifyInterval	30000	{mseconds {ISON Interval}}
    antiflood   2000	{mseconds {Anti-flood Delay}}
    autoAway	0	{mseconds}
    testTime	30000	{mseconds}
    quitwait	10000	{mseconds}
    dccTime	600000	{mseconds}
    ping	0	{mseconds}
    helpers	{}	{pairlist}
    beeplimit	4	{int {Max. No. of Beeps}}
    dcclimit	10	{int {Max. No. of DCCs}}
    maxQueue	50	{int {Outbound Queue Limit}}
    nicksize	9	{int {Max. Nickname Size}}
    dccblock	2048	{int {DCC Blocksize}}
    msgmax	0	{int {Max. No. of Messages}}
    ctcpmax	1	{int {Max. No. of CTCPs at once}}
    nickactions	{}	{patterns}
    urlcolour	{blue}	{colour}
" {
    memos	{}
    globfriend	{}
    whohandler	{}
    uhhandler	{}
    restricted	0
    name	{}
    sock	{}
    away	0
    monitorlst	{}
    aways	{}
    signoffs	{}
    actions	{}
    ons		{}
    bindings	{}
    ignores	{}
    leaves	{}
    ircop	0
    gircop	0
    info	{}
    control	{}
    debug	{}
    nickname	{}
    nicks	{}
    ircname	{}
    ircnames	{}
    versioninfo	{}
    fingerinfo	{}
    userinfo	{}
    myid	nil
    startup	1
    hostid	nil
    sVersion	{}
    sUmodes	{}
    sCmodes	{}
    idle	0
    confChange	{}
    nickwait	0
    timercount	0
    msgQueue	{}
    msgQLen	0
    msgQTag	{}
    sysQDelay	500
    sysQueue	{}
    sysQLen	0
    sysQTag	{}
    splits	{}
    pinged	0
    allChannels {}
    channels	{}
    messages	{}
    notices	{}
    chats	{}
    users	{}
    friends	{}
    servers	{}
    services	{}
    motdSeen	0
    lastuser	{}
    lastwho	{}
    atchn	0
    atsrv	0
    atcll	0
    msgclear	{}
    closing	{}
    showlist	0
    listid	{}
    sockshost	{}
    socksport	{}
    sockcmd	socket
}
#
class Control {} {
    net		{}
    window	{}
    friends	{}
    nickname	{}
    ircname	{}
    server	{}
    port	{}
    channel	{}
    smrow	0
}
#
class Friends {} {
    name	{}
    window	{}
    noshare	0
    close	0
    control	{}
    wid		{}
    style	window
}
#
class Monitor { } {
    name	{}
    net		{}
    channel	{}
    net		{}
    window	{}
    wid		{}
    close	0
}
#
class List { } {
    name	{}
    net		{}
    wid		{}
    close	0
    listPattern .*
    topicPattern	.*
    showPublic	1
    showLocal	1
    showPrivate	0
    topicOnly	0
    minMembers	3
    maxMembers	0
    sorted	0
    sortNames	1
    nocase	0
    listfile	{}
    filename	{}
    channel	{}
}
#
class WBoard { } {
    name	{}
    channel	{}
    wid		{}
    close	0
    mode	line
    fill	none
    oln		black
    start	{}
    last	{}
    points	{}
}
#
}
