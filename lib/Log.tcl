#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Log.tcl,v $
# $Date: 1998/05/21 12:55:26 $
# $Revision: 1.18.1.18 $
#
package provide zircon 1.18
#
proc logOpen {this mode save fl} {
    global openLogs openLfd openLMap
    switch -- $fl {} {return 0}
    if {[catch {fullName $fl} file]} {
	tellError {} {Log File Error} "Cannot get full pathname for $fl : $file"
	return 0
    }
    regsub -all %n $file [[$this net] name] file
    regsub -all %c $file [$this name] file
    set t [clock seconds]
    regsub -all %d $file [clock format $t -format {%d%b%Y}] file
    regsub -all %t $file [clock format $t -format {%H%M%S}] file
    upvar #0 $this cdata
    if {[info exists openLogs($file)]} {
	incr openLogs($file)
	set cdata(logfd) $openLfd($file)
    } {
	if {[catch {open $file $mode} cdata(logfd)]} {
	    tellError {} {Log File Error} \
	      "Cannot open log file for channel [$this name] : $cdata(logfd)"
	    set cdata(logfd) {}
	    return 0
	}
	set openLogs($file) 1
	set openLfd($file) $cdata(logfd)
	set openLMap($cdata(logfd)) $file
    }
    set cdata(logactual) $file
    set w [$this window].channel.menu.log
    $w entryconfigure 0 -state normal
    $w entryconfigure 1 -state disabled
    $w entryconfigure 2 -state normal
    $w entryconfigure 3 -state normal
    puts $cdata(logfd) "**** Logging Started : [getDate]"
    if {$save} {
	puts $cdata(logfd) "**** Saving Window"
	puts $cdata(logfd) "[[$this text] get 1.0 end]"
	puts $cdata(logfd) "**** End of Window Text"
    }
    $this showFlags log on
    return 1
}
#
proc channel_doLog {this op} {
    global openLogs openLfd openLMap
    upvar #0 $this cdata
    set w [$this window].channel.menu.log
    switch $op {
    Close {
	    switch {} $cdata(logfd) {} default {
		set fl $openLMap($cdata(logfd))
		if {[incr openLogs($fl) -1] == 0} {
		    close $cdata(logfd)
		    unset openLogs($fl) openLfd($fl) openLMap($cdata(logfd))
		}
		set cdata(logfd) {}
	    }
	    if {[winfo exists $w]} {
		$w entryconfigure 0 -state disabled
		$w entryconfigure 1 -state normal
		$w entryconfigure 2 -state disabled
		$w entryconfigure 3 -state disabled
	    }
	    $this showFlags log {}
	}
    Empty {
	    switch {} $cdata(logfd) {} default {
		set ofd $cdata(logfd)
		set fl $openLMap($ofd)
		if {$openLogs($fl) > 1} {
		    tellError {Shared File} "The file \"$fl\" is\
			currently in use by $openLogs($fl) channels\
			so cannot be emptied."
		    return
		}
		close $ofd
		unset openLMap($ofd)
		set cdata(logfd) [open $cdata(logactual) w]
		set openLfd($fl) $cdata(logfd)
		set openLMap($cdata(logfd)) $fl
	    }
	}
    Flush {
	    switch {} cdata(logfd) {} default {
		if {[catch {flush $cdata(logfd)} msg]} {
		    [$this net] errmsg \
		      "Error flushing log file for channel [$this name] : $msg"
		    catch {close $cdata(logfd)}
		    switch {} $cdata(logfile) {set cdata(logfd) {}} default {
			set cdata(logfd) [open $cdata(logfile) a]
		    }
		}
	    }
	}
    Open {
	    set chan [$this name]
	    set fl [expr {[string match {} $cdata(logfile)] ? \
	      $cdata(logfile) : "%c.log"}]
	    mkFileBox .@log$this ${this}(logdir) .* "Log $chan" \
	      "Log file for channel $chan:" {} \
	      [list append "logOpen $this a 0 "]\
	      [list {Append All} "logOpen $this a 1"]\
	      [list open "logOpen $this w 0"] \
	      [list {Open from Start} "logOpen $this w 1 "] [list cancel {}]
	}
    }
}
