#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/DALnet.tcl,v $
# $Date: 2001/07/10 15:36:10 $
# $Revision: 1.18.1.5 $
#
package provide zircon 1.18
#
proc irc328 {net unusedprefix param pargs} {
    set chan [lindex $pargs 1]
    switch nil [set chn [Channel :: find $chan $net]] {
	$net inform "Url for $chan : $param"
    } default {
	if {[$chn active]} {$chn showInfo url $param}
    }
}
#
proc irc329 {net unusedprefix unusedparam pargs} {
    set chan [lindex $pargs 1]
    if {[catch {set crt [clock format [lindex $pargs 2]]}]} return
    switch nil [set chn [Channel :: find $chan $net]] {
	$net inform "$chan created at $crt"
    } default {
	if {[$chn active]} {$chn showInfo create "Created at $crt"}
    }
}
#
proc irc333 {net unusedprefix unusedparam pargs} {
    set chan [lindex $pargs 1]
    set who [lindex $pargs 2]
    if {[catch {set crt [clock format [lindex $pargs 3]]}]} return
    switch nil [set chn [Channel :: find $chan $net]] {
	$net inform "$chan topic set by $who at $crt" 
    } default {
	if {[$chn active]} {$chn showInfo topic "Topic set by $who at $crt"}
    }
}
