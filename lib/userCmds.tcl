#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/userCmds.tcl,v $
# $Date: 2001/07/10 15:36:14 $
# $Revision: 1.18.1.15 $
#
package provide zircon 1.18
#
#	Users cmds
#
proc doInvite {net nk chan} {
    switch {} $nk return
    switch {} $chan return
    $net configure -lastuser $nk
    $net INVITE $nk $chan
}

proc userInvite {net chan} {
    set lu [$net lastuser]
    mkEntryBox .@inv$net [trans invite] {Enter user and channel:} \
      [list [list user $lu] [list channel $chan]] \
      [list ok "doInvite $net"] [list cancel {}]
}
#
proc doMemo {net nk msg} {
    switch {} $nk return
    switch {} $msg return
    $net addMemo $nk $msg
    $net configure -lastuser $nk
}
#
proc doMsg {net nk} {
    switch {} $nk return
    Message :: make $net $nk
    $net configure -lastuser $nk
}
#
proc userMemo {net} {
    mkEntryBox .@msg$net [trans message] {Enter user name:} \
      [list [list user [$net lastuser]] [list message {}]] \
      [list ok "doMemo $net"] [list cancel {}]
}
#
proc userMessage {net} {
    mkEntryBox .@msg$net [trans message] {Enter user name:} \
      [list [list user [$net lastuser]]] \
      [list ok "doMsg $net"] [list cancel {}]
}
#
proc doFing {net nk} {
    switch {} $nk return
    $net configure -lastuser $nk
    finger $net $nk
} 
#
proc userFinger {net} {
    mkEntryBox .@fng$net [trans finger] {Enter user name:} \
      [list [list user [$net lastuser]]] \
      [list ok "doFing $net"] [list cancel {}]
}
#
proc userNotice {net} {
    mkEntryBox .@not$net [trans notice] {Enter user name and notice text:} \
      [list [list user [$net lastuser]] [list notice {}]] \
      [list ok "doNet $net NOTICE"] [list cancel {}]
}
#
proc lastWho {net} {
    switch {} [set lw [$net lastwho]] {
        set lw [$net lastuser]
    }
    return $lw
}
#
proc doNet {net cmd nk op} {
    switch {} $nk return
    $net configure -lastwho $nk
    $net $cmd $nk $op
}
#
proc doNet1 {net cmd nk} {
    switch {} $nk return
    $net configure -lastwho $nk
    $net $cmd $nk
}
#
proc userWho {net} {
    mkEntryBox .@wq$net [trans who] {Enter user name} \
      [list [list user [lastWho $net]]] \
      [list ok "doNet1 $net WHO"] [list cancel {}]
}
#
proc userWhois {net} {
    mkEntryBox .@wis$net [trans whois] {Enter user name and server:} \
      [list [list user [lastWho $net]] [list Where {}]] \
      [list ok "doNet $net WHOIS"] [list cancel {}]
}
#
proc userWhowas {net} {
    mkEntryBox .@was$net [trans whowas] {Enter user name and count:} \
      [list [list user [lastWho $net]] [list Count {}]] \
      [list ok "doNet $net WHOWAS"] [list cancel {}]
}
#
proc doUMode {net nk mode} {
    if {![string match {} $nk] && [string match {[+-]*} mode]} {
	$net configure -lastuser $nk
	$net q1Send "MODE $nk :$mode"
    }
}
#
proc userMode {net} {
    mkEntryBox .@umd$net {User Mode} {Enter user and mode:} \
      [list [list user [$net lastuser]] [list mode {}]] \
      [list ok "doUMode $net"] [list cancel {}]
}
#
proc userCmd {net cmd args} {
    switch -exact -- $cmd {
    Mode	{userMode $net}
    Finger	{userFinger $net}
    Invite	{userInvite $net {}}
    Memo	{userMemo $net}
    Message	{userMessage $net}
    Notice	{userNotice $net}
    Kill	{userKill $net}
    Who		{userWho $net}
    Whois	{userWhois $net}
    Whowas	{userWhowas $net}
    default {
	    mkEntryBox .@$cmd$net [trans $cmd] {Enter user pattern:} \
	      [list [list user [$net lastuser]]] \
	      [list ok "uSend $net [string toupper $cmd]"] [list cancel {}]
	}
    }
}
#
proc uSend {net cmd nk} {
    switch {} $nk return
    $net configure -lastuser $nk
    $net q1Send "$cmd :$nk"
}


