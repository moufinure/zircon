#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Monitor.tcl,v $
# $Date: 2002/01/18 12:22:23 $
# $Revision: 1.18.1.22 $
#
package provide zircon 1.18
#
proc Monitor {args} {
    set th [makeObj Monitor $args]
    $th show
    return $th
}
#
proc monitor_setState {this usr state} {
    catch {[$this window].usr.$usr configure -state $state}
}
#
proc monitor_remove {this usr} {
    if {![catch {destroy [$this window].usr.$usr}]} { $usr deref }
}
#
proc msortIns {win un usr tg} {
    set wnk $win.$usr
    set fn name
    set fn1 nop
    set op 0
    set spk 0
    switch -- $tg {
      @ { set op 1 }
      + { set spk 1 }
    }
    if {[[$usr net] ircIIops]} {
	set txt $tg$un
    } {
	set txt $un
    }
    set srt 1
    set top text
    set wd [$win cget -width]
    switch nil [set fr [$usr fobj]] {} default {
	switch {} [set fg [$fr images]] {} default {
	   set top image
	   set img [lindex $fg 0]
	   set wd {}
	   switch -glob -- $txt {
	   @* {
		    switch {} [set oi [lindex $fg 1]] {} default {set img $oi}
	       }
	   +* {
		    switch {} [set oi [lindex $fg 2]] {} default {set img $oi}
	       }
	   }
	   set txt $img
	}
    }
    menubutton $wnk -$top $txt -highlightthickness 0 -borderwidth 2 -pady 1 -padx 2
    switch {} $wd {} default { $wnk configure -width $wd }
    $wnk configure -menu [makeUserMenu nil $wnk.menu $usr]
    if {$srt} {
	set y 1
	while {![catch {set x [$win window cget 1.$y -window]}]} {
	    switch {} $x {} default {
	    	set u2 [winfo name $x]
		switch [$fn1 $op $spk $u2] {
		0 { }
		1 {
		    if {[string compare $un [$u2 $fn]] < 0} {
		        $win window create [$win index $x] -window $wnk -padx 0
		        return
		    }
		  }
		2 {
		    $win window create [$win index $x] -window $wnk -padx 0
		    return
		   }
		}
	    }
	    incr y
	}
    }
    $win window create end -window $wnk -padx 0
}
#
proc monitor_update {this names} {
    if {![winfo exists [set w [$this window]]]} return
    set xist {}
    foreach n [winfo children $w.usr] { lappend xist [winfo name $n] }
    set net [$this net]
    foreach n $names {
	switch {} $n continue
	regexp {^([@+]?)(.*)} $n m mrk n
	set usr [User :: make $net $n]
	if {![winfo exists [set winu $w.usr.$usr]]} {
	    msortIns $w.usr $n [$usr ref] $mrk
	} {
	    if {![normal $winu]} {
		$winu configure -state normal
		$usr heal
	    }
	    listkill xist $usr
	}
	switch $mrk {
	@ { set mrk operator} 
	+ { set mrk speaker}
	}
	markButton $net $winu $mrk
    }
    foreach n $xist {if {[normal $w.usr.$n]} {destroy $w.usr.$n ; $n deref}}
}
#
proc monitor_configure {this args} { confObj $this $args }
#
proc monitor_show {this} {
    upvar #0 $this mdata
    switch {} $mdata(wid) {
	set chan [$mdata(channel) name]
	set net $mdata(net)
	set wndw [Window .$this]
	$wndw configure -title "$chan [trans monitor]" -resizable {0 1}
	set w [$wndw name]
	array set mdata "name $this window $w wid $wndw"
	$wndw register $this "$mdata(channel) configure -monitor 0" {}
	grid columnconfigure $w 0 -weight 1
	grid rowconfigure $w 0 -weight 1 
	scrollbar $w.vscroller -command "$w.usr yview" 
	text $w.usr -yscrollcommand "$w.vscroller set" -relief flat -borderwidth 0 -width 14 -height 10
	grid $w.usr $w.vscroller -sticky ns
	grid [frame $w.btns -relief raised] - -sticky ew
	grid columnconfigure $w.btns 0 -weight 1
	grid columnconfigure $w.btns 1 -weight 1
	button $w.btns.cancel -text [trans dismiss] \
	  -command "$mdata(channel) configure -monitor 0" -width 6
	button $w.btns.join -text [trans join] \
	  -command "$mdata(channel) sendJoin {} " -width 6
	grid $w.btns.cancel $w.btns.join -sticky ew
	grid $w.btns - -sticky ew
    } default {
	$mdata(wid) popup
    }
    return $this
}
#
proc monitor_delete {this} {
    switch {} [set wn [$this wid]] {} default {
        if {![catch {$wn name} w]} {
            catch {
	        foreach x [winfo children $w.usr] {catch {[winfo name $x] deref}}
	        destroy $w
                $wn deregister $this
            }
	}
    }
    safeUnset $this
    rename $this {}
}

