#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/upgrade.tcl,v $
# $Date: 1998/02/10 10:48:50 $
# $Revision: 1.18.1.4 $
#
package provide zircon 1.18
#
# Upgrade the rc file
#
proc upgradeRC {} {
    switch [tk_dialog .@upgrade Upgrade \
      {Zircon now stores its configuration files in a directory called \
	.zircon in your home directory. Upgrade?} {} 0 Upgrade Cancel ] {
 	0 { upgrade2 }
 	1 { }
    }
}
#
proc upgrade2 {} {
    global zircon
    set x $zircon(prefdir)
    if {[catch {filemkdir $x} msg]} {
	puts stderr "**** Could not make directory $x - $msg"
	return
    }
    if {[catch {filerename  [file join ~ .zirconrc] [file join  $x preferences]} msg]} {
	puts stderr "**** Could not move .zirconrc to $x/preferences - $msg"
    }
}
