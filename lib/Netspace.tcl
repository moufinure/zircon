#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Netspace.tcl,v $
# $Date: 2001/07/10 15:36:12 $
# $Revision: 1.18.1.28 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide zircon 1.18
#
proc Netspace {name args} {
    global currentNet
    switch {} $currentNet {} default {
	if {[notDefaultNet]} { error "Nested Netspace directive for $name."}
    }
    nsLoad $name 0 $args
}
#
proc nsLoad {name show unusedopts} {
    global XTO currentNet DEBUG zircon
    set f [file join $zircon(prefdir) netspaces $name]
    if {![file exists $f]} {
	tellError {} Netspace "Cannot find Netspace $name"
	return 0
    }
    if {[file isdirectory $f]} {
	set f [file join $f preferences]
	if {![file exists $f]} {
	    tellError {} Netspace "Cannot find Netspace preferences for $name"
	    return 0
	}
    }
    set ln [string tolower $name]
    if {[info exists XTO($ln)]} {
	for {set i 1} {[info exists XTO($ln$i)]} {incr i} {}
	set nn1 $name$i
	if {![askUser {} {Netspace Load} "Netspace \"$name\" is already loaded.\
It will be loaded under the name \"$nn1\"."]} { return 0 }
	addToNM $nn1
    } {
	set nn1 $name
    }
    set sc $currentNet
    set currentNet [Net $nn1]
    makeDefaults $currentNet
    if {[catch {source $f} msg]} {
	tellError {} {Netspace error} "Error in netspace $nn1 - $msg"
	set ret 0
    } {
	InitNet $currentNet
	$currentNet show
	if {$show} {
	    exposeFrame [MainControl] $currentNet
	    if {![$currentNet integrate]} {
	        exposeFrame [MainInfo] [$currentNet info]
	    }
	}
	if {$DEBUG} { zDBGAdd $currentNet }
	set ret 1
    }
    set currentNet $sc
    setUnload $nn1 normal
    return $ret
}
#
proc nsDelete {name} {
    if {![askUser {} {Delete Netspace} \
       "Are you sure you want to delete Netspace \"$name\""]} return
    global zircon
    filedirdel [file join $zircon(prefdir) netspaces $name]
    delFromNsM $name
    delFromConf $name
}
#
proc nsClone {name} {
    mkEntryBox {} "Cloning $name" \
      {Enter the name for the cloned Netspace:} \
      [list [list name {}]] [list ok [list nsAddClone $name]] [list cancel {}]
}
#
proc nsAddClone {old new} {
    switch {} $new return
    global zircon
    foreach x [Net :: list] {
	switch -- [$x name] $new {
	    tellError {} {Clone Error} "Netspace \"$new\" already exists."
	    return
	}
    }
    set dir [file join $zircon(prefdir) netspaces]
    set of [file join $dir $old]
    set nf [file join $dir $new]
    if {[file isdirectory $of]} {
	if {[catch {filemkdir $nf} msg]} {
	    tellError {} "Clone Error" $msg
	    return
	}
	foreach x [glob -nocomplain [file join $of *]] {
	    filecopy $x $nf
	}
    } {
	if {[catch {filecopy $of $nf} msg]} {
	    tellError {} "Clone Error" $msg
	}
    }
    addToNsM $new
}
#
proc nsUnload {name} {
    global zircon
    foreach x [Net :: list] {
	switch -- [$x name] $name {
	    switch {} [$x sock] {} default {
	    	tellInfo $x {Active Netspace} "Netspace $name is active. Please\
		close the connection before trying to unload it."
		return
	    }
	    if {![file exists [file join $zircon(prefdir) netspaces $name]]} {
		delFromNsM $name
	    } {
		setUnload $name disabled
	    }
	    delFromConf $name
	    $x delete
	    break
	}
    }
}
#
proc notDefaultNet {} {
    global currentNet defaultNet
    switch $currentNet $defaultNet {return 0}
    return 1
}
#
proc isNotDefaultNet {net} {
    global defaultNet
    switch $net $defaultNet {return 0}
    return 1
}
