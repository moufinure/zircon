#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Windows.tcl,v $
# $Date: 2000/03/30 13:14:51 $
# $Revision: 1.18.1.8 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
#
package provide windows 1.18
#
proc tmpdir {} {
    global env zircon
    if {[info exists zircon(tmp)]} { return $zircon(tmp) }
    return $env(TEMP)
}
#
proc prefdir {} { return [file join [lindex [glob ~] 0] .zircon] }
#
proc username {} {
    global tcl_platform
    if {[info exists tcl_platform(user)]} {
        return $tcl_platform(user)
    }
    return {ZirconUser}
}
#
