#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Message.tcl,v $
# $Date: 2001/07/10 15:36:11 $
# $Revision: 1.18.1.39 $
#
package provide zircon 1.18
#
proc Message {name args} {
    switch -- $name :: {
	set op [lindex $args 0]
	switch [info procs Message_$op] {} {return [eval Channel_$op [lrange $args 1 end] ]}
	return [eval Message_$op [lrange $args 1 end] ]
    }
    global currentNet
    switch nil [set id [Message :: find $name $currentNet]] {
	set id [makeMessage $name $currentNet]
    }
    eval $id configure $args
    return $id
}
#
proc message_call {this op pars} {
    switch {} [info procs message_$op] {
	return [objCall channel $this $op $pars]
    }
    return [eval message_$op $this $pars]
}
#
proc makeMessage {chan net} {
    global defMsg
    set this [objName Message]
    proc $this {unusedop args} "message_call $this \$unusedop \$args "
    initObj $this Channel Message
    upvar #0 $this mdata MTO$net MTO
    switch *default* [set lchan [string tolower $chan]] {} default {
	[User :: make $net $chan] join $this
    }
    if {[catch {set def $defMsg($net)}]} {
	global defaultNet defChan
	if {[catch {set def $defMsg($defaultNet)}]} {
	    set def $defChan($defaultNet)
	}
	set b 0
    } {
	set b [$def buttons]
    }
    array set mdata [uplevel #0 array get $def]
    array set mdata [list \
	keep	0 \
	buttons	$b \
	name	$chan \
	lname	$lchan \
	net	$net \
	ircIImode [$net ircIImode] \
    ]
    $net register messages $this 
    set MTO($lchan) $this
    return $this
}
#
proc message_onShow {unusedthis} { }
#
proc message_setCrypt {this key} {
    $this configure -crypt $key
    [User :: find [$this name] [$this net]] configure -crypt $key
}
#
proc message_setTitles {this} {
    set nam [$this name]
    set id {}
    switch nil [set usr [User :: find $nam [$this net]]] {} default {
	switch {} [set id [$usr id]] {} default { set id " ($id)"}
    }
    return [list $nam "Conversation with $nam$id"]
}
#
proc message_nickChange {this usr nnk} {
    switch -- [$this lname] [$usr lname] {$this nChange $nnk}
    channel_nickChange $this $usr $nnk
}
#
proc message_nChange {this nnk} {
    set net [$this net]
    if {[$this active]} {
	[$this wid] setIcon $this $nnk
	switch nil [set usr [User :: find [$this name] $net]] {} default {
	    switch {} [set id [$usr id]] {} default { set id " ($id)"}
	}
	[$this wid] setTitle $this "Conversation with $nnk$id"
    }
    upvar #0 MTO$net MTO $this cdata
    set ln [string tolower $nnk]
    unset MTO($cdata(lname))
    set MTO($ln) $this
    array set cdata [list\
	lname	$ln \
	name	$nnk \
    ]
}
#
proc message_replace {this usr1 usr2} {
    $this nChange [$usr2 name]
    channel_replace $this $usr1 $usr2
}
#
proc message_delete {this} {mcnDelete $this MTO[$this net] messages}
#
proc Message_make {net nk} {
    upvar #0 MTO$net MTO
    set nk [$net trimNick [cleanup $nk]]
    set usr [User :: make $net $nk]
    set ln [string tolower $nk]
    if {[info exists MTO($ln)]} { set id $MTO($ln) } {
	set id [$net eval [list Message $nk]]
    }
    $id configure -crypt [$usr crypt]
    $id show -nofocus
    $id addUser $usr 0 0
    return $id
}
#
proc Message_find {nk net} {
    upvar #0 MTO$net MTO
    set ln [string tolower $nk]
    return [expr {[info exists MTO($ln)] ? $MTO($ln) : {nil}}]
}
#
proc Message_save {desc net} {
    global defMsg
    defSave $desc defMsg $net Message
    foreach ch [$net messages] {
	if {[string compare $ch $defMsg($net)] && [$ch keep]} {
	    $ch save $desc
	}
    }
}
#
proc message_save {this desc} {
    mncSave $desc $this defMsg Message
    foreach  b [$this bindings] {
	puts $desc "zbind [$this name] [lindex $b 0] {[lindex $b 1]}"
    }
}
#
proc message_awayMsg {this msg} {
    upvar #0 $this mdata
    switch -- $mdata(away) $msg {} default {
	set mdata(away) $msg
	if {[$this active]} {$this addText {} "*** [$this name] is away : $msg"}
    }
}
#
proc message_leave {this} {
    set chan [$this name]
    if {[askUser LEAVE "Leaving conversation with $chan" \
      "Really close conversation with $chan?"]} {
        switch {} [info procs $this] {} default { $this delete }
    }
}
#
proc message_heal {this} {
    if {[$this active]} {
	$this flag normal
	$this addText {} "*** netsplit : [$this name] may have left IRC."
    }
}
