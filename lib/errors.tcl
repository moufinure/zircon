#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/errors.tcl,v $
# $Date: 1998/04/27 11:46:10 $
# $Revision: 1.18.1.21 $
#
package provide zircon 1.18
#
proc irc401 {net prefix param pargs} {
    if {[[set msg [Message :: find [set chn [lindex $pargs 1]] $net]] active]} {
	$msg addText @ERROR "*** $chn is not on IRC"
    } {
	$net errmsg "$chn - $param"
    }
    switch nil [set usr [User :: find $chn $net]] {} default {
	upvar #0 Offer$net Offer
	catch {
	    foreach v $Offer($usr) { killDel Offer$net $usr [lindex $v 1] }
	}
    }
}
#
proc irc404 {net prefix param pargs} {
    if {[[set chn [Channel :: find [set chan [lindex $pargs 1]] $net]] active]} {
	$chn addText @ERROR "*** $param"
    } {
	$net errmsg "Cannot send to channel $chan"
    }
}
#
proc irc406 {net prefix param pargs} {
    global whois
    set whois($net,err) [lindex $pargs 1]
}
#
proc resetNick {net} {
    [$net control] confirmNick [$net nickname]
    $net configure -nickwait 0
}
#
proc irc422 {net prefix param pargs} {
    $net errmsg $param
    afterMOTD $net $prefix $param $pargs
}
#
proc irc432 {net prefix param pargs} {
    resetNick $net
    tellError $net {Nickname Error} "[lindex $pargs 1] : $param"
}
#
proc irc433 {net prefix param pargs} {
    cantUseNick $net $prefix $param $pargs {in use}
}
#
proc irc437 {net prefix param pargs} {
    switch -glob -- [set nm [lindex $pargs 1]] {
	{[#&+]*} {tellError $net {Channel Error} "$nm : $param"}
	default {cantUseNick $net $prefix $param $pargs {temporarily unavailable}}
    }
}
#
proc irc438 {net prefix param pargs} {
    resetNick $net
    tellError $net {Nickname Error} "[lindex $pargs 1] : $param"
}
#
proc tryNick {net nk} {
    $net NICK $nk
    $net configure -nickname $nk
}
#
proc cantUseNick {net prefix param pargs msg} {
    set unk [lindex $pargs 1]
    if {[$net startup]} {
	global user
	$net fast
	set nk $user
	foreach x [$net nicks] {
	    switch -- $param $x continue
	    set nk $x
	    break
	}
	mkDialog SERROR .@ne$net {Nick in use} \
	  "The nickname \"$unk\" is $msg. Try another." \
	  [list [list nickname $nk [$net nicks]]] \
	  [list ok "tryNick $net"] [list cancel "$net close"]
    } {
	resetNick $net
	tellError $net {Nickname Error} "$unk : $param"
    }
}
#
proc irc442 {net prefix param pargs} {
    if {[[set ch [Channel :: find [set nm [lindex $pargs 1]] $net]] active]} {
        if {[$ch leaving]} {
	    $ch delete
	} {
	    $ch addText @ERROR "*** Error 442 : $nm $param [lindex $pargs 2]"
	}
    } {
	tellError $net {Channel Error} "$nm $param [lindex $pargs 2]"
    }
}

proc irc443 {net prefix param pargs} {
    set who [lindex $pargs 1]
    if {[[set ch [Channel :: find [set cn [lindex $pargs 2]] $net]] active]} {
	$ch addText @ERROR "*** $who $param $cn"
    } {
	tellError $net {Invite Error} "$who $param $cn"
    }
}

proc irc471 {net prefix param pargs} {
    set chn [Channel :: find [set chan [lindex $pargs 1]] $net]
    if {[tk_dialog .@f$net {Channel Full} "Channel ${chan} is full!" \
      error 0 OK {Try Again}]} {$chn sendJoin}
}

proc irc473 {net prefix param pargs} {
    if {[[set ch [Channel :: find [lindex $pargs 1] $net]] active]} {
	$ch addText @ERROR "*** \007Channel is invitation only!"
    } {
	tellError $net {Invitation Only} "Channel [lindex $pargs 1] is invitation only!"
    }
}
#
proc irc474 {net prefix param pargs} {
    if {[[set ch [Channel :: find [lindex $pargs 1] $net]] active]} {
	$ch flag disabled
	$ch addText @ERROR "*** \007You are banned from this channel!"
    } {
	tellError $net Banned "You are banned from channel [lindex $pargs 1]!"
    }
}

proc irc475 {net prefix param pargs} {
    if {[[set chn [Channel :: find [set chan [lindex $pargs 1]] $net]] key] == {}} {
	mkEntryBox .@key$net [trans key] "Enter key for channel $chan:" \
	  [list [list key {}]] [list join "$chn sendJoin"] \
	  [list cancel {}]
    } {
	mkDialog {} .@key$net "Bad Key" \
	  "Bad key for channel $chan!" [list [list key [$chn key]]] \
	  [list {Try Again} "$chn sendJoin"] [list cancel {}]
    }
}



