#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Split.tcl,v $
# $Date: 2002/01/18 12:22:24 $
# $Revision: 1.18.1.13 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000-2002 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
#
package provide zircon 1.18
#
proc user_split {this nsplit} {
    set net [$this net]
    upvar #0 Split$net Split
    if {![info exists Split($nsplit)]} { $net newSplit $nsplit }
    foreach id [$net channels] {$id userSplit $this $nsplit}
    foreach x {Notice Message} {
	switch nil [set id [$x :: find [$this lname] $net]] continue
	if {[$id active]} {
	    $id addText {} "*** Netsplit - $nsplit"
	    $id flag disabled
	}
    }
    switch nil [set fobj [$this fobj]] {} default {
	[$net finfo] disable $fobj
	$fobj configure -limbo 1
    }
    lappend Split($nsplit) [$this ref]
    handleOn $net USPLIT [list $nsplit [$this lname]]
}
#
proc user_heal {this} {
    set net [$this net]
    upvar #0 Split$net Split Heal$net Heal TSplit$net TSplit
    handleOn $net BACK [$this name]
    foreach sl [array names Split] {
	if {[set x [lsearch $Split($sl) $this]] >= 0} {
	    catch {after cancel $TSplit($sl) ; unset TSplit($sl)}
#	    if {![info exists Heal($sl)]} {
#		[$net info] optText HEAL "*** Heal - $sl"
#		handleOn $net BACK $sl
#	    }
	    set v $Split($sl)
	    listdel v $x
	    $this deref
	    switch {} $v {
		    unset Split($sl)
		    catch {after cancel Heal($sl) ; unset Heal($sl)}
	    } default {
		set Split($sl) $v
		catch {after cancel $Heal($sl)}
		set Heal($sl) [after 120000 "catch {$net cleanSplit \"{$sl}\"}"]
	    }
	}
    }
    foreach x {Notice Message} {
	switch nil [set id [$x :: find [$this lname] $net]] continue 
	if {[$id active]} {
	    $id addText {} "*** [$id name] is back"
	    $id flag normal
	}
    }
    foreach x [$net channels] { $x heal $this }
}
#
proc channel_heal {this usr} {
    switch {} [$this mwin] {} default {[$this mwin] setState $usr normal}
}
#
proc channel_userSplit {this usr nsplit} {
    if {[$this isJoined $usr]} {
	set w [$this window]
	catch {
	    if {[set x [indexHack $w.users.menu [$usr name] 3]] >=0} {
		$w.users.menu entryconfigure $x -state disabled
	    }
	}
	catch {[$this ufrm].userBtn.$usr conf -state disabled}
	uplevel #0 listincl ${this}(splitusers) $usr
	$this optText LOST "*** [$usr name] is lost - ($nsplit)"
    } \
    elseif {[$this monitor]} {
        switch {} [$this mwin] {} default {[$this mwin] setState $usr disabled}
    }
}
#
proc net_newSplit {this nsplit} {
    upvar #0 Split$this Split TSplit$this TSplit Heal$this Heal $this ndata
    $ndata(info) optText SPLIT "*** Netsplit - $nsplit"
    set TSplit($nsplit) [after 600000 $this cleanSplit [list $nsplit]]
    catch {after cancel $Heal($nsplit) ; unset Heal($nsplit)}
    handleOn $this SPLIT $nsplit
    lappend ndata(splits) [list $nsplit]
}
#
proc net_cleanSplit {this h} {
    upvar #0 Split$this Split TSplit$this TSplit Heal$this Heal $this ndata
    if {[info exists Split($h)]} {
	set frnd [$this finfo]
	foreach user $Split($h) {
	    if {[catch {set nk [$user name]}]} continue
	    foreach x {Chat Message Notice} {
		switch nil [set msg [$x :: find $nk $this]] {} default {
		    $msg heal
		}
	    }
	    foreach id [$this channels] { $id remove $user }
	    $frnd remove [$user fobj]
	    $user deref
	}
	unset Split($h)
	listkill ndata(splits) [list $h]
    }
    catch { after cancel $TSplit($h) ; unset TSplit($h) }
    catch { after cancel $Heal($h) ; unset Heal($h) }
}
