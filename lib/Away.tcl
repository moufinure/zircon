#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Away.tcl,v $
# $Date: 2000/02/02 10:09:10 $
# $Revision: 1.18.1.24 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide zircon 1.18
#
proc awayPanel {net} {
    if {[winfo exists [set w .@aw$net]]} { popup $w ; return }
    set wid [Window $w -title {Away Messages}]
    upvar #0 $net ndata
    grid rowconfigure $w 1 -weight 1
    foreach x {0 1 2} {grid columnconfigure $w $x -weight 1}
    checkbutton $w.srv -text {Tell Server} -variable ${net}(atsrv) 
    checkbutton $w.bcst -text {Tell Channels} -variable ${net}(atchn) \
	-command "asAction $net $w"
    checkbutton $w.call -text {Tell Callers} -variable ${net}(atcll)
    grid $w.srv $w.bcst $w.call -sticky w
    grid configure [checkbutton $w.auto -text {Auto Clear} \
      -variable ${net}(autoclearaway) -state disabled] -row 1 -column 1 -sticky w
    makeLB $w.aways -height 12 -width 40
    bind $w.aways.l <Button-1> "+ awayEnab $w"
    foreach act [$net aways] { $w.aways.l insert end $act }
    bind $w.aways.l <Double-1> "$net sendAway $w"
    grid $w.aways - - -sticky nsew
    set ndata(atact) 0
    checkbutton $w.actn -text {Send as Action} -variable ${net}(atact)
    grid $w.actn -row 2 -column 1 -sticky ew
    if {!$ndata(atchn)} {grid forget $w.actn}
    button $w.set -text [trans set] -command "$net sendAway $w" -state disabled
    button $w.clear -text [trans clear] -command "$net clearAway $w" \
      -state disabled
    if {$ndata(busy) || $ndata(away)} { $w.clear configure -state normal }
    button $w.cancel -text [trans dismiss] -command "$wid delete"
    button $w.new -text [trans new] -command "$net getAway $w.aways.l"
    button $w.edit -text [trans edit] -command "$net editAway $w.aways.l" -state disabled
    button $w.del -text [trans delete] -command "$net delAway $w.aways.l" -state disabled
    grid $w.new $w.edit $w.del -sticky ew
    grid $w.set $w.clear $w.cancel -sticky ew
}
#
proc awayEnab {w} {
   foreach x {set del edit} { $w.$x configure -state normal }
}
#
proc awayDisab {w} {
   foreach x {set del edit} { $w.$x configure -state disabled }

}
#
proc asAction {net w} {
    upvar #0 $net ndata
    if {$ndata(atchn)} {
	grid $w.actn -row 2 -column 1 -sticky ew
    } {
	grid forget $w.actn
    }
}
#
proc net_editAway {this w} {
    switch {} [set t [$w curselection]] return
    set msg [$w get $t]
    mkEntryBox {} "Edit Away Message" "Edit the message:" \
      [list [list message $msg]] \
      [list ok "doEdit $this [list $t] $w"] [list cancel {}]
}
#
proc doEdit {net pos w args} {
    upvar #0 $net ndata
    set msg [lindex $args 0]
    $w delete $pos
    $w selection clear 0 end
    switch {} $msg {
	set ndata(aways) [lreplace $ndata(aways) $pos $pos]
    } default {
	$w insert $pos $msg
	$w selection set $pos
	set ndata(aways) [lreplace $ndata(aways) $pos $pos $msg]
    }
}
#
proc net_delAway {this w} {
    switch {} [set t [$w curselection]] return
    upvar #0 $this ndata
    if {![catch {set ndata(aways) [lreplace $ndata(aways) $t $t]}]} {
        $w delete $t
        $w selection clear 0 end
        awayDisab [winfo toplevel $w]
    }
}
#
proc net_clearAway {this w} {
    upvar #0 $this ndata
    set ndata(busy) 0
    if {$ndata(away)} {$this AWAY}
    set ndata(away) 0
    if {$ndata(atchn)} {
	set op send
	if {$ndata(atact)} {set op action}
	foreach x {channels notices messages chats} {
	    foreach y [$this $x] {if {[$y active]} { $y $op [trans back]	}}
	}
    }
    [$this control] blackit away
    switch {} $w {} default {destroy $w}
}
#
proc net_sendAway {this w} {
    upvar #0 $this ndata
    switch {} [set sel [$w.aways.l curselection]] { bell ; return }
    set value [$w.aways.l get $sel]
    set done 0
    if {$ndata(atsrv)} {$this AWAY $value ; set done 1}
    if {$ndata(atchn)} {
	set op send
	if {$ndata(atact)} {set op action}
	foreach x {channels notices messages chats} {
	    foreach y [$this $x] {
		if {[$y active]} {$y $op $value}
	    }
	}
	set done 1
    }
    if {$ndata(atcll)} {
	set ndata(busy) 1
	set ndata(busymsg) $value
	set done 1
    }
    if {$done} {
	[$this control] redit away
        destroy $w
    } {
        bell
    }
}
#
proc net_keepAway {this win value} {
    $this configure +aways $value +confChange Info
    $win insert end $value
    $win selection clear 0 end
    $win selection set end
    awayEnab [winfo toplevel $win]
}
#
proc net_getAway {this w} {
    mkEntryBox .@away$this {Away Message} {Enter your new away message:} \
      [list [list away {}]] [list ok "$this keepAway $w"] [list cancel {}]
}
