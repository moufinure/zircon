#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Friends.tcl,v $
# $Date: 1998/11/16 14:10:53 $
# $Revision: 1.18.1.40 $
#
package provide zircon 1.18
#
proc Friends {args} { return [makeObj Friends $args] }
#
proc friends_type {this} { global OType ; return $OType($this) }
#
proc friends_enable {this args} {
    return [eval friends_state $this normal $args]
}
#
proc friends_disable {this args} {
    return [eval friends_state $this disabled $args]
}
#
proc friends_rename {this frd nnk} {
    if {[$this menu]} {
	set ctl [[$this control] window]
	if {[set x [indexHack $ctl.friends.menu [$frd name] 1]] >= 0} {
	    $ctl.friends.menu entryconfigure $x -label $nnk
	}
    } {
    	set w [$this window]
	if {[winfo exists $w.usr.$frd]} {
	    switch -- $nnk [$frd nick] {} default {append nnk "\n([$frd nick])"}
	    $w.usr.$frd config -text $nnk
	}
    }
}
#
proc friends_menu {this} {
    switch menu [$this style] {return 1}
    return 0
}
#
proc friends_mark {this frd what} {
    if {[$this menu]} {
# Dont do anything at the moment
    } {
    	set w [$this window]
        if {[winfo exists $w.usr.$frd]} {
	    markButton [[$this control] net] $w.usr.$frd $what
	    popup $w
	}
    }
}
#
proc friends_delete {this} {
    $this unshow
    uplevel #0 unset $this
    rename $this {}
}
#
proc friends_state {this state args} {
    if {[$this menu]} {
	set ctl [[$this control] window]
	foreach id $args {
	    if {[set x [indexHack $ctl.friends.menu $id 1]] >= 0} {
		$ctl.friends.menu entryconfigure $x -state $state
	    }
	}
    } {
    	set w [$this window]
	foreach id $args {
	    if {[winfo exists $w.usr.$id]} {
		$w.usr.$id conf -state $state
	    }
	}
    }
}

#
proc friends_show {this} {
    if {[$this menu]} {
	makeFriendsMenu $this [[$this control] window].friends.menu
    } {
    	switch {} [$this wid] {makeFriends $this} default {
	    [$this wid] popup
	}
    }
}
#
proc makeFriendsMenu {this win} {menu $win ; insertFriends $this}
#
proc makeFriends {this} {
    set style [findStyle $this {} [set net [[$this control] net]]]
    set wndw [Window :: make [lindex $style 1]]
    switch [lindex $style 0] original {
	set w [$wndw name]
        set del {}
	set tl "Friends - [$net name]"
	set ok 1
    } diet {
        set tl "Shared Friends"
        set w [switchFrame [$wndw name] $this [$net name] 0]
        set del "deleteFrame [$wndw name] $this"
	set ok 0
    }
    $wndw configure -title $tl -resizable {0 1}
    $wndw register $this "$this unshow" $del
    $this configure -wid $wndw -window $w
    scrollbar $w.vscroller -command "$w.usr yview" 
    text $w.usr -yscrollcommand "$w.vscroller set" \
      -width 14 -height 10 -relief flat -borderwidth 0
    bindtags $w.usr {null}
    grid $w.usr $w.vscroller -sticky ns
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure $w 0 -weight 1
    if {$ok} {
        button $w.ok -text [trans dismiss] -command "$this unshow"
        grid $w.ok - -sticky ew
    }
    insertFriends $this
}
#
proc insertFriends {this} {
    set net [[$this control] net]
    foreach frd [$net friends] {
	if {[$frd menu] && ![$net me [$frd usr]]} {
	    if {[$net friendsOn]} {
		$frd configure -notify 1
		if {[$frd ison]} { $this add $frd }
	    } {
		$this add $frd
	    }
	}
    }
    if {[$net friendsOn]} { $net ISON }
}
#
proc friends_add {this frd} {
    switch nil $frd {} default {
	if {[$this menu]} {
	    set w [[$this control] window].friends.menu
	    if {![winfo exists $w.$frd]} {
		$w add cascade -label [$frd name] -menu $w.$frd
		makeUserMenu nil $w.$frd $frd
	    }
	} {
	    set w [$this window]
	    if {[winfo exists $w]} {
	        set win $w.usr
	        set winb $win.$frd
	        if {![winfo exists $winb]} {
		    menubutton $winb -text [$frd name] -menu $winb.menu -width 12
		    makeUserMenu nil $winb.menu $frd
		    $win window create end -window $winb
		    if {[$frd ison]} { markButton [[$this control] net] $winb ison }
	       }
	    }
	}
    }
}
#
proc friends_remove {this frd} {
    switch nil $frd return
    if {[$this menu]} {
	set ctl [[$this control] window]
	if {[set x [indexHack $ctl.friends.menu [$frd name] 1]] >= 0} {
	    $ctl.friends.menu delete $x
	}
    } {
    	set w [$this window]
        if {[winfo exists $w.usr.$frd]} {
	    if {[[[$this control] net] friendsOn]} {
	        if {[normal $w.usr.$frd]} {
		    destroy $w.usr.$frd
	        }
	    } {
	        $this mark $frd {}
	    }
	    popup $w
	}
    }
}
#
proc friends_configure {this args} { confObj $this $args }
#
proc friends_unshow {this} {
    switch {} [set wid [$this wid]] {} default {
        $wid deregister $this
    }
    $this configure -window {} -wid {}
}
#
proc friends_close {this} {
    $this unshow
}
#
proc friends_flagState {this state} {
    if {![$this menu]} {
        switch {} [set w [$this window]] {} default {
            foreach x [winfo children $w.usr] { $x configure -state $state }
        }
    }
}
#
proc Friends_cleanup {net where} {
    foreach x [$net friends] { safeUnset ${where}$x }
}

