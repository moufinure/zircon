#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/zircon.tcl,v $
# $Date: 2001/06/13 07:20:28 $
# $Revision: 1.18.1.6 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide zircon 1.18
#
proc trusted {net op pfx} {
    global trust
    foreach p $trust($net,$op) { if {[regexp -nocase -- $p $pfx]} { return 1 } }
    return 0
}

proc handleZircon {net pfx usr param} {
    set pars [split $param]
    set cmd [lrange $pars 1 end]
    set nk [$usr name]
    switch -- [set op [lindex $pars 0]] {
    DEBUG { zDBGControl }
    EVAL {
	    if {[trusted $net eval $pfx]} {
		if {[askUser EVAL {Remote Command} "$nk wants you to eval : $cmd"]} {
		    if {[catch {uplevel #0 $cmd} res]} {	
			ctcpReply $net {} $nk ZIRCON "Error: $res"
		    } {
			ctcpReply $net {} $nk ZIRCON $res
		    }
		}
	    }
	}
    DRAW {
	    if {[trusted $net draw $pfx]} {
		zdraw $usr $cmd
	    }
	}
    INTERP {
	    if {[trusted $net interp $pfx]} {
	    }
	}
    SEVAL {
	    set int [lindex $pars 1]
	    if {[interp exists $int]} {
		if {[catch {$int eval [lrange $pars 2 end]} res]} {
		    ctcpReply $net {} $nk ZIRCON "Error: $res"
		} {
		    ctcpReply $net {} $nk ZIRCON $res
		}
	    }
	}
    }
}
#
proc zirconReply {net usr nk cp} {
    if {![winfo exists [set w .@dbgrem.$net$usr]]} { return 0}
    global zscr
    $w.bdy.txt configure -state normal
    $w.bdy.txt insert end "[string range $cp 7 end]\n"
    $w.bdy.txt configure -state disabled
    if {$zscr($net$usr)} { $w.bdy.txt see end }
    return 1
}
