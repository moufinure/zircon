#
# $Source: /home/nlfm/Working/Zircon/Released/lib/RCS/Service.tcl,v $
# $Date: 1998/03/03 09:21:38 $
# $Revision: 1.18.1.15 $
#
package provide zircon 1.18
#
proc Service {name args} {
    switch -- $name :: {
	return [eval Service_[lindex $args 0] [lrange $args 1 end]]
    }
    global currentNet
    set this [makeNObj $name $currentNet Service $args]
    upvar #0 $this sdata
    array set sdata [list \
        nick	$name \
        addr	$name \
    ]
    switch {} $sdata(host) {} default {
	global Secure
	set sdata(addr) $sdata(nick)@$sdata(host)
	set Secure([string tolower $sdata(nick)]) $sdata(addr)
    }
    return $this
}
#
proc service_configure {this args} { confObj $this $args }
#
proc service_delete {this} {
    [$this net] deregister services $this
    uplevel #0 unset $this
    rename $this {}
}
#
proc service_send {this op par} { [$this net] msg [$this addr] "$op $par" }
#
proc service_do {this op} {
    set sv [$this name]
    mkEntryBox {} $sv "Enter any parameters needed for $sv:" \
      [list [list $op {}]] [list ok "$this send $op"] [list cancel {}]
}
#
proc Service_save {desc net} {
    set cm 1
    foreach sv [$net services] {
	if {[$sv sys]} continue
	regsub -all {[][\\{\"}]} [$sv name] {\\&} nm
	set ln "Service $nm"
	foreach p {nick host ops} {
	    switch {} [set n [$sv $p]] {} default {append ln " -$p [list $n]"}
	}
	if {$cm} { puts $desc "#\n# Services\n#" ; set cm 0 }
	puts $desc $ln
    }
}
