Zircon Control Window
---------------------

This window provides a set of buttons and menus that let you control
your IRC session. The various fields are :

Zircon VX.X : brings up a menu with the following items:

About Zircon : This tells you the version of Zircon you are
   running and brings up the credits panel which also tells you the
   version of tcl and tk that you are using.

Configure : Selecting from this menu allows you to configure various
   aspects of the Zircon system. IRC lets you configure your nickname,
   IRC name and server lists. People lets you configure your Friends list
   and your ignore list. Channels sets up characteristics of your
   favourite channels and Info controls various miscellaneous features of
   the system. Save Current will save your current stae in your rc file.

Debug: This is greyed out unless you have debugging enabled. It
    controls the presence or absence of the Zircon debugger window.

Quit: This quits all your connections and leaves zircon.

----

Help: Clicking here brings up a dialog asking for a topic on which you
wish help. This will be sent to the help service nickname indicated in
the other field of the dialog. The default service is configurable
from your rc file and can be found on the Info configure window.

----

Busy: Clicking this will mark you as busy (or not busy) which will
currently cause unsolicited notices and messages to be sent to the
info window and for messages a reply sent to the originator telling
them you are busy.

Invisible, Wallop, SrvMsg: Checking these buttons will cause you to be
marked as Invisible, to receive wallops and to receive server
messages. You normally would not want to do this!

IRC Op: This button lights up when you are an IRC operator. When lit,
clicking it causes you to have operator status removed from you.

------

Nickname: The menu from this button has your favourite nicknames on
it. Select one and it will be sent to IRC. By default the first item
is chosen by Zircon. Typing in the entry box to the right of the
button will also change your nickname when you hit return.

IRCname: Ditto for IRC names, but these cannot be changed on the fly.
Changes will only take place on a server change.

Server: A list of your favourite servers. The first one is chosen by
default. Selecting from the menu or typing in the entry will close
your current session and connect to that server. Typing Escape instead
of Return in the entry box will prompt you for a port number of the
server - again you would not normally use this feature.

Away: This menu contains your favourite away messages. The item Back
marks you as no longer being away. Other prompts you for a message and
the other items are the messages you have saved in your rc file. When
you are away this button is lit.

BRB: Clicking this sends the message BRB to all the channels you are
currently on. The button label changes to back and clicking on it
again sends the message "back" to all the channels.

Friends: Clicking this brings up the Friends window. This has buttons
for all the users you chose to have in your userInfo variable
(configurable from the People panel). If the user is on IRC the button
wil be lit if you have configured the isonForeground, isonFont and
isonBackground X-resources. If the friendsOn variable is set then only
those friends that are on IRC will have buttons displayed.

Quit: Quits IRC. You will be prompted to confirm the quit and to enter
a new signoff message. You can set up a default message by setting the
variable signoff in your rc file or from the Info panel. You can
turn off the request for confirmation from the Info panel as well. If
you have altered your configuration and not saved it you will be asked
whether or not you wish to do so. NB the default action is to NOT
save the file.

Servers: This menu allows you to perform various IRC server operations.

Users : This allows you to perform various IRC user operations.

Channels : This allows you to perform various IRC Channel operations.
Also on this menu are the names of channels you asked to be put here.
Selecting one of those will join that channel.

Services: This menu provides access to various IRC service providers.
Currently nickserv and noteserv are there by default, and you can add
your own by setting the services variable in your rc file.

Channel: This entry is where you can type in the name of a channel you
wish to join. Type here, hit return and a window will pop up, assuming
you are allowed access to the channel that is!
