#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/windows.tcl,v $
# $Date: 2001/06/13 07:20:29 $
# $Revision: 1.18.1.20 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide windows 1.18
#
proc install {argv} {
    global Type types Lib Bin CC LD Version Patchlevel Wish types cflags\
      libs share SCflag GCC SLDflag CFlags TIDir NoGUI Xtn go env
#
    package require support
    package require winfile
#
    switch -glob -- [info tclversion] 7.* {
        tk_dialog .wdz {Windows Installation} {Sorry, you  must have tcl/tk version\
	8.0p2 or later to install under Windows 95 or NT.} info 0 OK
	exit 1
    }
#
    set argv [doGlobals $argv]
#
# Get System Root drive info
#
    if {[catch {set tmp_SD $env(SystemDrive)}]} {
        set tmp_SD C:
    }
    set Lib [file join ${tmp_SD}/ {Program Files} Zircon]
    set Bin $Lib
    trace variable Lib w traceLib
    set Xtn .tcl
#
    set w [mainWindow]
    set i 0
#
# Zircon library directory
#
    makeEntry $w lib {Installation Directory} Lib [incr i] Null
#
# Info
#
    if {!$NoGUI} {
	grid [frame $w.sep2 -bg red] - -sticky ew -pady 2
	grid [label $w.info] -columnspan 2 -sticky ew
    }
    if {$go} { Go ; Quit }
#
# Buttons
#
    putButtons $w
}
#
proc traceLib {args} { global Bin Lib ; set Bin $Lib }
#
proc prog7 {args} { }
#
proc sed {args} { }
#
proc whereLib {} { return {[pwd]} }
#
proc makeExecutable {file} { }





