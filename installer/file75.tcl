#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/file75.tcl,v $
# $Date: 2000/02/02 10:09:15 $
# $Revision: 1.18.1.15 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide unixfile 1.18.7.5
#
proc filemkdir {dir} {if ![file exists $dir] {exec mkdir $dir}}
#
proc filedelete {f} {exec rm -f $f}
#
proc filerename {from to} {exec mv -f $from $to}
#
proc copyfile {from to} {
    if [file exists $to] { exec rm -f $to }
    exec cp $from $to
}
#
proc mkReadable {file} { exec chmod a+r $file }
#
proc mkExecutable {file} { exec chmod a+rx $file }
