#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/manifest.tcl,v $
# $Date: 2002/05/09 21:18:05 $
# $Revision: 1.18.1.320 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
array set Manifest [list  \
    README					{} \
    [file join doc README.ONS]			{} \
    [file join doc README.SOCKS]		{} \
    [file join doc README.Y2K]			{} \
    [file join doc README.load]			{} \
    [file join doc README.look]			{} \
    [file join doc README.prefs]		{} \
    CHANGES					{} \
    winstall.tcl				{} \
    [file join doc MANUAL]			{} \
    [file join doc TODO]			{} \
    [file join doc BUGS]			{} \
    installZircon				{} \
    [file join doc COPYRIGHT]			{} \
    [file join doc FAQ]				{} \
    [file join doc MIRRORS]			{} \
    [file join doc IRCinfo tao.of.irc]		{} \
    [file join doc IRCinfo IRC.faq]		{} \
    [file join doc IRCinfo rfc1459]		{} \
    [file join gifs zircon.gif]			{copy} \
    [file join gifs off1.gif]			{copy} \
    [file join gifs on1.gif]			{copy} \
    [file join gifs arc.gif]			{copy} \
    [file join gifs line.gif]			{copy} \
    [file join gifs none.gif]			{copy} \
    [file join gifs oval.gif]			{copy} \
    [file join gifs polygon.gif]		{copy} \
    [file join gifs rectangle.gif]		{copy} \
    [file join gifs text.gif]			{copy} \
    [file join gifs key.gif]			{copy} \
    [file join gifs speak.gif]			{copy} \
    [file join gifs nospeak.gif]		{copy} \
    [file join gifs off.gif]			{copy} \
    [file join gifs on.gif]			{copy} \
    [file join gifs button.gif]			{copy} \
    [file join gifs noii.gif]			{copy} \
    [file join gifs ii.gif]			{copy} \
    [file join gifs lock.gif]			{copy} \
    [file join gifs log.gif]			{copy} \
    [file join gifs unlock.gif]			{copy} \
    [file join gifs zlogo.gif]			{copy} \
    [file join help Netspaces		]	{copy} \
    [file join help Zircon Windows Info]	{copy} \
    [file join help Zircon Windows Control]	{copy} \
    [file join help Zircon Windows Channel]	{copy} \
    [file join help Zircon Windows Message]	{copy} \
    [file join help Zircon Ons]			{copy} \
    [file join help Zircon CommandLine]		{copy} \
    [file join help Zircon Environment]		{copy} \
    [file join help Zircon Bindings]		{copy} \
    [file join help Users]			{dmake} \
    [file join help Channels]			{dmake} \
    [file join help Servers]			{dmake} \
    [file join help Services]			{dmake} \
    [file join installer install.tcl]		{} \
    [file join installer unix.tcl]		{} \
    [file join installer file75.tcl]		{} \
    [file join installer file76.tcl]		{} \
    [file join installer file80.tcl]		{} \
    [file join installer file81.tcl]		{} \
    [file join installer file82.tcl]		{} \
    [file join installer file83.tcl]		{} \
    [file join installer file84.tcl]		{} \
    [file join installer support.tcl]		{} \
    [file join installer mac.tcl]		{} \
    [file join installer macfile.tcl]		{} \
    [file join installer manifest.tcl]		{} \
    [file join installer pkgIndex.tcl]		{} \
    [file join installer windows.tcl]		{} \
    [file join installer winfile.tcl]		{} \
    [file join installer zircon.tcl]		{} \
    [file join lang english.tcl]		{copy} \
    [file join lang norsk.tcl]			{copy} \
    [file join lang francais.tcl]		{copy} \
    [file join lang deutsch.tcl]		{copy} \
    [file join lang esperanto.tcl]		{copy} \
    [file join lang nihongo.tcl]		{copy} \
    [file join lang bulgar.tcl]			{copy} \
    [file join lang magyar.tcl]			{copy} \
    [file join lang espanol.tcl]		{copy} \
    [file join lang italiano.tcl]		{copy} \
    [file join lang suomi.tcl]			{copy} \
    [file join lang catala.tcl]			{copy} \
    [file join lib Channel.tcl]			{copy} \
    [file join lib Chat.tcl]			{copy} \
    [file join lib Consts.tcl]			{copy} \
    [file join lib Control.tcl]			{copy} \
    [file join lib DALnet.tcl]			{copy} \
    [file join lib Entry.tcl]			{copy} \
    [file join lib Frame.tcl]			{copy} \
    [file join lib Friend.tcl]			{copy} \
    [file join lib Friends.tcl]			{copy} \
    [file join lib IRC.tcl]			{copy} \
    [file join lib Ignore.tcl]			{copy} \
    [file join lib Info.tcl]			{copy} \
    [file join lib Interface.tcl]		{copy} \
    [file join lib List.tcl]			{copy} \
    [file join lib Log.tcl]			{copy} \
    [file join lib Message.tcl]			{copy} \
    [file join lib Monitor.tcl]			{copy} \
    [file join lib Net.tcl]			{copy} \
    [file join lib Notice.tcl]			{copy} \
    [file join lib On.tcl]			{copy} \
    [file join lib Protect.tcl]			{copy} \
    [file join lib SOCKS.tcl]			{copy} \
    [file join lib Server.tcl]			{copy} \
    [file join lib Service.tcl]			{copy} \
    [file join lib Sound.tcl]			{copy} \
    [file join lib Split.tcl]			{copy} \
    [file join lib Tags.tcl]			{copy} \
    [file join lib Topic.tcl]			{copy} \
    [file join lib User.tcl]			{copy} \
    [file join lib Util.tcl]			{copy} \
    [file join lib Window.tcl]			{copy} \
    [file join lib bindings.tcl]		{copy} \
    [file join lib confChan.tcl]		{copy} \
    [file join lib confInfo.tcl]		{copy} \
    [file join lib confTab.tcl]			{copy} \
    [file join lib config.tcl]			{copy} \
    [file join lib ctcp.tcl]			{copy} \
    [file join lib dialog.tcl]			{copy} \
    [file join lib errors.tcl]			{copy} \
    [file join lib main.tcl]			{copy} \
    [file join lib misc.tcl]			{copy} \
    [file join lib misc2.tcl]			{copy} \
    [file join lib plugin.tcl]			{copy} \
    [file join lib saverc.tcl]			{copy} \
    [file join lib servers.tcl]			{copy} \
    [file join lib upgrade.tcl]			{copy} \
    [file join lib userCmds.tcl]		{copy} \
    [file join lib WBoard.tcl]			{copy} \
    [file join lib zircon.tcl]			{copy} \
    [file join lib frivol.tcl]			{copy} \
    [file join lib Help.tcl]			{copy} \
    [file join lib Button.tcl]			{copy} \
    [file join lib Object.tcl]			{copy} \
    [file join lib initial.tcl]			{copy} \
    [file join lib Undernet.tcl]		{copy} \
    [file join lib dbgsrv.tcl]			{copy} \
    [file join lib Netspace.tcl]		{copy} \
    [file join lib Look.tcl]			{copy} \
    [file join lib Classes.tcl]			{copy} \
    [file join lib Away.tcl]			{copy} \
    [file join lib tcl75.tcl]			{copy} \
    [file join lib Mac.tcl]			{copy} \
    [file join lib tcl76.tcl]			{copy} \
    [file join lib tcl80.tcl]			{copy} \
    [file join lib tcl81.tcl]			{copy} \
    [file join lib tcl82.tcl]			{copy} \
    [file join lib tcl83.tcl]			{copy} \
    [file join lib tcl84.tcl]			{copy} \
    [file join lib Unix.tcl]			{copy} \
    [file join lib Windows.tcl]			{copy} \
    [file join lib debug.tcl]			{copy} \
    [file join lib Tracing.tcl]			{copy} \
    [file join lib Notrace.tcl]			{copy} \
    [file join look standard.tcl]		{copy} \
    [file join plugins MsgOps setup.tcl]	{copy}  \
    [file join plugins NoticeOps setup.tcl]	{copy}  \
    [file join plugins Dcc auto.tcl]		{copy}  \
    [file join plugins Dcc chat.tcl]		{copy} \
    [file join plugins Dcc dcc.tcl]		{copy} \
    [file join plugins Dcc dccget.tcl]		{copy} \
    [file join plugins Dcc dccsend.tcl]		{copy} \
    [file join plugins Dcc send.tcl]		{copy} \
    [file join plugins Dcc dccget.c]		{prog7} \
    [file join plugins Dcc dccsend.c]		{prog7} \
    [file join plugins Sed decrypt.tcl]		{copy} \
    [file join plugins Sed sed.c]		{sed} \
    [file join plugins Sed crypt.c]		{prog7} \
    [file join samples zircon.ad]		{ncopy} \
    [file join samples Preferences]		{} \
    [file join samples Netspaces]		{} \
    scripts					{dmake} \
    sounds					{dmake} \
    [file join util findMeta]			{} \
]
#
set Index "lib [file join plugins Dcc] [file join plugins Sed]"
#
set Version 1.18
set Patchlevel 256

