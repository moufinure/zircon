#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/unix.tcl,v $
# $Date: 2001/06/13 07:20:29 $
# $Revision: 1.18.1.55 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide unix 1.18
#
proc install {argv} {
#
    package require support
    package require -exact unixfile 1.18.[info tclversion]
#
    global Type types Lib Bin CC LD Version Patchlevel Wish types cflags\
      libs share SCflag GCC SLDflag CFlags TIDir v7 SED go NoGUI Xtn
#
    set types {AIX Alpha BSDI FreeBSD HP-UX IRIX Linux OSF1 {SunOS 4} {SunOS 5} UNIX_SV}
#
    array set cflags {
	HP-UX		{+O3 -Aa -D_HPUX_SOURCE}
	BSDI		{+O3}
    }

    array set scflags {
	HP-UX		{+z}
	IRIX		{-KPIC}
	FreeBSD		{-fpic}
	SCO		{-Kpic -belf}
	{SunOS 4}	{-PIC}
	{SunOS 5}	{-KPIC}
	UNIX_SV		{-KPIC}
    }
    set v7 0
    set Xtn {}
#
    switch -glob -- [info tclversion] 7.* {
        array set libs {
	    UNIX_SV	{-lsocket -lnsl}
	    IRIX	{-lnsl}
	    {SunOS 5}	{-lsocket -lnsl}
	}
	set v7 1
    }
#
    array set share {
	{SunOS 5}	{-G -z text}
	UNIX_SV		{-G -z text}
	IRIX		{-shared -rdata_shared}
	AIX		{-bhalt:4 -bM:SRE -bE:lib.exp -H512 -T512}
	FreeBSD		{-Bshareable -x}
	SCO		{-G}
	OSF1		{-shared -expect_unresolved}
	Linux		{-shared}
    }
#
    set argv [doGlobals $argv]
    checkSCF
#
# Handle OS name change for Suns
#
    switch $Type {
	SunOS { set Type {SunOS 4} }
	Solaris { set Type {SunOS 5} }
    }
#
    set w [mainWindow]
    set i 0
#
# Zircon library directory
#
    makeEntry $w lib {Library Directory} Lib [incr i] getLib
#
# Zircon binary directory
#
    makeEntry $w bin {Binary Directory} Bin [incr i] getBin
#
# Wish
#
    makeEntry $w wish {Tcl/Tk Interpreter} Wish [incr i] getWish
#
# Info
#
    if {$v7 || $SED} {
#
# System Type
#
	if {!$NoGUI} {
	    menubutton $w.type -text {System Type} -menu $w.type.menu -relief raised
	    menu $w.type.menu -tearoff 0
	    foreach x $types {
		$w.type.menu add command -label $x -command "set Type $x"
	    }
	    entry $w.typeentry -textvariable Type -state disabled -width 30
	}
	getType
	if {!$NoGUI} { gridIt $w type [incr i] }
#
# C compiler
#
	makeEntry $w cc {C Compiler} CC [incr i] getCC
#
# C compiler Flags
#
	makeEntry $w flg {C Compiler Flags} CFlags [incr i] Null
#
# C Shared compile
#
	makeEntry $w scf {C Shared Code} SCflag [incr i] Null
#
# Tcl.h include directory
#
	makeEntry $w tid {tcl Header Directory} TIDir [incr i] Null
#
# Shared load 
#
	makeEntry $w dl {Shared Code Loading} SLDFlag [incr i] Null
    }
#
# Info
#
    if {!$NoGUI} {
	grid [frame $w.sep2 -bg red] - -sticky ew -pady 2
	grid [label $w.info] -columnspan 2 -sticky ew
    }
    if {$go} { Go ; Quit }
#
# Buttons
#
    putButtons $w
}
#
proc getType {} {
    global tcl_platform Type
    set Type $tcl_platform(os)
    switch $Type {
	SunOS {
	    switch -glob $tcl_platform(osVersion) {
		4* { set Type {SunOS 4} }
		5* { set Type {SunOS 5} }
	    }
	}
    }
}
#
proc getLib {} {
    global Lib
    switch {} $Lib {set Lib /usr/local/lib/zircon}
}
#
proc getWish {} {
    global Wish
    switch {} $Wish {set Wish [findWish]}
}
#
proc getBin {} {
    global Bin env Lib Wish
    switch {} $Bin {} default return
    foreach x [split $env(PATH) :] {
	set z [file join $x zircon]
	if {[file exists $z]  && [file executable $z]  && \
	  ![file isdirectory $z]} {
	    set Bin $x
	    if {![catch {open $z r} fd]} {
		gets $fd wish
		set Wish [string range $wish 2 end]
		gets $fd lib
		close $fd
		set Lib [lindex $lib 2]	
	    }
	    return
	}
    }
    set Bin /usr/local/bin
}
#
proc getCC {} {
    global CC GCC SCflag CFlags Type SLDflag share scflags
    switch {} $CC {} default return
    set CFlags -O
    catch {set CFlags $cflags($Type)}
    switch BSDI $Type {
	set CC gcc2
	set GCC 1
	set SCflag -fPIC
    } default {
	switch {} [set CC [searchX gcc]] {
	    set GCC 0
	    set CC [searchX cc]
	    set SCflag -pic
	    if {[info exists scflags($Type)]} { set SCflag $scflags($Type) }
	} default {
	    set GCC 1
	    set SCflag -fPIC
	}
    }
    if {[info exists share($Type)]} {
	set SLDflag $share($Type)
    }
}
#
proc searchX {prog} {
    global env
    switch [file pathtype $prog] {
    relative {
	    foreach x [split $env(PATH) :] {
		if {[file exists [file join $x $prog]]} {
		    return [file join $x $prog]
		}
	    }
	}
    }
    return {}
}
#
proc findWish {} {
    global env
    foreach x [split $env(PATH) :] {
	foreach y {wish8.3 wish8.2 wish8.1 wish8.0 wish4.2 wish4.1 wish wishx} {
	    if {[file exists [set f [file join $x $y]]]} {
		return $f
	    }
	}
    }
    return {}
}
#
proc prog7 {file to} {
    switch -glob -- [info tclversion] 8.* return
    zinfo "Compiling $file..."
    global CC libs Type CFlags
    set to [dmake [set dt [file dirname $file]] $to]
    set lb {}
    if {[info exists libs($Type)]} { set lb $libs($Type) }
    set tg [file rootname [file join $to [file tail $file]]]
    if {[catch {eval exec $CC $CFlags -o $tg $file $lb} msg]} {
	fail "Compilation of $file failed - $msg"
    }
}
#
proc prog {file to} {
    global CC Type CFlags v7 SED
    if {!$v7 && !SED} { return }
    zinfo "Compiling $file..."
    global CC Type CFlags
    set to [dmake [set dt [file dirname $file]] $to]
    set lb {}
    set tg [file rootname [file join $to [file tail $file]]]
    if {[catch {eval exec $CC $CFlags -o $tg $file} msg]} {
	fail "Compilation of $file failed - $msg"
    }
}
#
proc checkSCF {} {
    global SCflag CC GCC CFlags SLDflag Type share Type

    switch -glob $CC {
	/*/gcc -
	gcc { set SCflag -fPIC ; set GCC 1}
	default {
	    switch {} $SCflag {
		if {[info exists share($Type)]} {
		    set SCflag $share($Type)
		} {
		    set SCflag -pic
		}
	    }
	    set GCC 0
	}
    }
    if {[info exists share($Type)]} { set SLDflag $share($Type) }
}
#
proc sed {file to} {
    global CC Type SCflag CFlags SLDflag TIDir LD v7 SED
    if {!$v7 && !$SED} return
    zinfo "Compiling $file..."
    global CC Type SCflag CFlags SLDflag TIDir LD
    checkSCF
    set cfl "$CFlags $SCflag"
    switch {} $TIDir {} default {append cfl " -I$TIDir"}
    set to [dmake [set dt [file dirname $file]] $to]
    if {[catch {eval exec $CC $cfl -c $file} msg]} {
	fail "Compile $file failed - $msg"
    }
    zinfo "Linking $file..."
    if {[catch {eval exec $LD $SLDflag \
      -o [file join $to lib[file tail $dt][info sharedlibextension]]\
      [file rootname [file tail $file]].o} msg]} {
	fail "Link $file failed - $msg"
    }
    filedelete [file rootname [file tail $file]].o
}
#
proc whereLib {} { global Lib ; return [list $Lib] }
#
proc makeExecutable {file} { mkExecutable $file }

