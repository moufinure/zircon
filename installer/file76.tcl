#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/file76.tcl,v $
# $Date: 2000/02/02 10:09:15 $
# $Revision: 1.18.1.16 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide unixfile 1.18.7.6
#
proc filemkdir {dir} {file mkdir $dir}
#
proc filerename {from to} {file rename -force $from $to}
#
proc filedelete {f} {file delete $f}
#
proc copyfile {from to} {file copy -force $from $to}
#
proc mkReadable {file} { exec chmod a+r $file }
#
proc mkExecutable {file} { exec chmod a+rx $file }
