#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/zircon.tcl,v $
# $Date: 2000/02/05 17:41:58 $
# $Revision: 1.18.1.17 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
#
proc zircon {} {
#
#	ZIRCON - an X11 interface to the Internet Relay Chat
#
#	Written by Lindsay (Lindsay F. Marshall)
#
#	Copyright (c) 2000
#
#	See the file COPYRIGHT for details
#
set lib [file join $zircon(lib) lib]
set auto_path [linsert $auto_path 0 $lib]
package require -exact interp [info tclversion]
package require $tcl_platform(platform)
switch macintosh $tcl_platform(platform) { console hide }
package require zircon
package require Debug
#
proc zUnknown {args} {
    switch -glob [set cmd [lindex $args 0]] irc* {
	set net [lindex $args 1]
	if {[$net startup]} {$net fast}
	if {![auto_load $cmd]} { ircNUM $net $cmd $args } { return [uplevel $args] }
    } /* {
        global currentNet
        return [doircII $currentNet [$currentNet info] $args]
    } default {
	return [uplevel sys_unknown $args]
    }
}
#
Initialise
#
# save some space!!! it will reload if necessary.
#
rename Initialise {}
rename InitGlobals {}
rename InitLook {}
rename InitImages {}
rename InitConsts {}
#
if {[set l [llength [set nets [Net :: list]]]] == 1} {
    $defaultNet show
    if {[$defaultNet nocontrol]} {
	wm withdraw [MainControl]
	catch {wm withdraw [MainInfo]}
    } elseif {![$defaultNet integrate]} { 
	exposeFrame [MainControl] $defaultNet
	exposeFrame [MainInfo] [$defaultNet info]
	hideFrame [MainControl] $defaultNet
	hideFrame [MainInfo] [$defaultNet info]
    } {
	exposeFrame [MainControl] $defaultNet
	hideFrame [MainControl] $defaultNet
    }
} {
    addToConf $defaultNet
    if {$l == 2} {
	set net [lindex $nets 1]
	if {[$net nocontrol]} {
	    wm withdraw [MainControl]
	    wm withdraw [MainInfo]
	} {
	    if {![$net integrate]} {
		exposeFrame [MainControl] $net
		exposeFrame [MainInfo] [$net info]
	    } {
		exposeFrame [MainControl] $net
	    }
	}
    }
}
#
safeUnset l net nets
#
rename unknown sys_unknown
rename zUnknown unknown
#
if {!$zircon(C)} {
    foreach x $znetList {
	switch nil [set hst [$x hostid]] {} default {
	    set s [$x servers]
	    set v [lsearch $s $hst]
	    listdel s $v
	    while {![$x startIRC $hst -noretry]} {
		switch {} $s {
		    [$x control] enableOpen
		    break
		}
		$x configure -hostid [set hst [lindex $s 0]]
		set s [lrange $s 1 end]
		update
	    }
	}
    }
    safeUnset s x v hst
}
}
