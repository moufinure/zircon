#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/winfile.tcl,v $
# $Date: 2000/02/02 10:09:16 $
# $Revision: 1.18.1.2 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide winfile 1.18.8.0
#
proc filemkdir {dir} {file mkdir $dir}
#
proc filedelete {f} {file delete $f}
#
proc filerename {from to} {file rename -force $from $to}
#
proc copyfile {from to} {file copy -force $from $to}
#
proc mkReadable {file} { }
