#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/mac.tcl,v $
# $Date: 2001/06/13 07:20:29 $
# $Revision: 1.18.1.12 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide macintosh 1.18
#
proc install {argv} {
    global Type types Lib Bin CC LD Version Patchlevel Wish types cflags\
      libs share SCflag GCC SLDflag CFlags TIDir NoGUI Xtn env go
#
    package require support
    package require macfile
#
    switch -glob -- [info tclversion] 7.* {
        tk_dialog .wdz {Macintosh Installation} {Sorry, you  must have tcl/tk version\
	8.0p2 or later to install under MacOS.} info 0 OK
	exit 1
    }
#
    beforeInstall
    set argv [doGlobals $argv]
    set Xtn {}
    set Lib [file join $env(SYS_FOLDER) Extensions Zircon]
    set Bin $env(DESK_FOLDER)
#
    set w [mainWindow]
#
# Info
#
    if {!$NoGUI} {
	grid [frame $w.sep2 -bg red] - -sticky ew -pady 2
	grid [label $w.info] -columnspan 2 -sticky ew
    }
    if {$go} { Go ; Quit }
#
# Buttons
#
    putButtons $w
}
#
proc prog7 {args} { }
#
proc sed {args} { }
#
proc setDir {dir creat type} {
    set here [pwd]
    cd $dir
    foreach x [glob -nocomplain *] {
        if {[file isdirectory $x]} {
	    setDir $x $creat $type
	} {
	    switch ttxt $creat {
	    	set fd [open $x]
		set ofd [open $x.new w]
		while {![eof $fd]} { puts $ofd [gets $fd] }
		close $ofd
		close $fd
		file rename -force $x.new $x
	    }
	    file attributes $x -creator $creat -type $type
	}
    }
    cd $here
}
#
proc beforeInstall {args} {
    global Lib
    set here [pwd]
    setDir doc ttxt TEXT
    setDir gifs 8BIM GIFf
    foreach x {help installer lib look lang plugins samples} {
        setDir $x R*ch TEXT
    }
    file attributes README -creator ttxt -type TEXT
    file attributes CHANGES -creator ttxt -type TEXT
}
#
proc whereLib {} { return {[pwd]} }
#
proc makeExecutable {file} { }






