#
# $Source: /home/nlfm/Working/Zircon/Released/installer/RCS/install.tcl,v $
# $Date: 2000/02/02 10:09:15 $
# $Revision: 1.18.1.13 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 2000 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
#
set NoGUI 0
if {[catch {wm withdraw .}]} {
    set NoGUI 1
    proc tk_dialog {a1 a2 msg args} {
	puts stderr $msg
    }
    proc update {} {}
}
#
if {![file exists installer] || ![file isdirectory installer]} {
    tk_dialog .err {Zircon Installer} {You must run the installer\
      with the zircon distribution directory as your current \
      directory!} error 0 Quit
    update
    exit 0
}
#
switch -glob [info tclversion] {
{[123456]\.*} -
{7\.[1234]} {
	tk_dialog .err {Zircon Installer} {You must have tcl7.5 or newer\
          in order to run Zircon. I recommend tcl 8.0 or better} info 0 Quit
	update
	exit 0
    }
7\.5 { 
	tk_dialog .err {Zircon Installer} {I see you are running \
          tcl7.5/tk4.1. Time is running out! Future versions of zircon will not support tcl7.*.} info 0 OK
	update
    }
7\.6 { 
	tk_dialog .err {Zircon Installer} {I see you are running \
          tcl7.6/tk4.2. Time is running out and you really ought to upgrade to the latest \
          release! Future versions of zircon will not support tcl7.*.} info 0 OK
	update
    }
    default {}
}
#
set auto_path [linsert $auto_path 0 [file join [pwd] installer]]
#
source [file join installer manifest.tcl]
#
foreach x [array names Manifest] {
    if {![file exists $x]} {
        switch dmake $Manifest($x) {} default {
	    tk_dialog .err {Zircon Installer} "Missing file - $x" error 0 Quit
	    update
	    exit 0
	}	
    }
}
#
package require $tcl_platform(platform) 1.18
#
install $argv
