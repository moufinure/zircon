#
# $Source: /home/nlfm/Working/Zircon/Released/plugins/MsgOps/setup.tcl,v $
# $Date: 1997/04/21 11:22:28 $
# $Revision: 1.2 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 1997 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
plugin MsgOps {channel} {{msg Message}} {
    foreach x [$channel users] {
	if {![$net me $x] && [$channel isOp $x]} {$net msg [$x name] $msg}
    }
}
