#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <fcntl.h>

extern int errno;

int main(argc, argv)
int argc;
char *argv[];
{
    extern int errno;
    int fd, flags, leng, posn;
    char buffer[4096], host[128];
    int l, tl = 0, ntl, pid = 0, out, elt;
    long dt;
    time_t st;

    leng = atoi(argv[4]);
    posn = atoi(argv[5]);
    st = time((time_t *) 0);
    l = -1;
    flags = O_WRONLY | O_CREAT;
    if (posn == 0)
    {
	flags |= O_TRUNC;
    }
    if ((out = open(argv[1], flags, 0600)) >= 0)
    {
	if (posn != 0) {
	    if (lseek(out, posn, 0) != posn)
	    {
		sprintf(host,
		  "%d DCCError Get: %s from %s Cannot seek on output file.\n", pid,
		  argv[1], argv[3]);
		goto error;
	    }
	    leng -= posn;
	}
	while ((l = read(0, buffer, 4096)) > 0) 
	{
	    tl += l;
	    if ((dt = (time((time_t*) 0) - st)) == 0)
	    {
                elt = 0;
	    }
	    else
	    {
	        elt = (int) ((leng - tl) / (((float) tl / dt )));
	    }

	    printf("%d DCC Get progress to %s %d %2.1f %d %d\n",
	      pid, argv[3], tl, ((float) tl / (float) leng) * 100.0,
	      elt / 60,  elt % 60);
	    fflush(stdout);
	    if (write(out, buffer, l) != l) 
	    {
		sprintf(host,
		  "%d DCCError Get: %s from %s Write error on output.\n", pid,
		  argv[1], argv[3]);
		l = -1;
		break;
	    }
	    ntl = htonl(tl);
	    if (write(2, (char *) &ntl, sizeof(int)) != sizeof(int))
	    {
		sprintf(host,
		  "%d DCCError Get: %s from %s Write error on transfer.\n", pid,
		  argv[1], argv[3]);
		l = -1;
		break;
	    }
	}
    }
    else
    {
	sprintf(host,
	  "%d DCCError Get: %s from %s Cannot create output file.\n", pid,
	  argv[1], argv[3]);
    }
    if (l < 0)
    {
      /*	(void) unlink(argv[1]); */
	sprintf(host,
	  "%d DCCError Get: %s from %s I/O Error %d - transfer aborted.\n", pid,
	  argv[1], argv[3], errno);
    }
    else if (tl < leng)
    {
	sprintf(host,
	  "%d DCCError Get: %s from %s Transfer interrupted.\n", pid,
	  argv[1], argv[3]);
    }
    else if (tl > leng) {
	sprintf(host,
	  "%d DCCError Get: %s from %s Transfer too long!!.\n", pid,
	  argv[1], argv[3]);
      
    }
    else 
    {
	if ((st = time((time_t *) 0) - st) == 0)
	{
	    st = 1;
	}
	sprintf(host, "%d DCC Get %s from %s completed. %f Kbytes/sec\n", pid,
	    argv[1], argv[3], ((float) tl /1000.0) / (float) st);
    }
error:
    (void) close(out);
    (void) shutdown(0, 0);
    (void) close(0);
    (void) shutdown(2, 1);
    (void) close(2);
    printf("%s", host);
    fclose(stdout);
    exit(0);
}
