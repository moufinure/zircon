#
# $Source: /home/nlfm/Zircon/plugins/Dcc/RCS/auto.tcl,v $
# $Date: 1998/07/10 13:34:06 $
# $Revision: 1.18.1.9 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 1997 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
package provide Dcc 1.18
#
proc dccSaveA {w net op} {
    set oag [$net $op]
    set ag {}
    foreach x [$w.lb.l get 0 end] {
	switch -- *NEW* $x continue
	lappend ag $x
    }
    $net configure -$op $ag
    switch -- $ag $oag {} default {$net configure +confChange DCC}
    catch {destroy $w}
}
#
proc dccSetAutoUser {net} {
    dccBuildGet ag $net From autoget dccAAG
}
#
proc dccSetAutoDir {net} {
    dccBuildGet agd $net Into autogetdir dccAADG
}
#
proc dccBuildGet {tg net msg op add} {
    if {[info exists [set w .$tg$net]]} { popup $w ; return }
    makeToplevel $w "Auto-accept Sends $msg" {} {}
    grid rowconfigure $w 0 -weight 1
    makeLB $w.lb -height 10 -width 20
    foreach x [$net $op] {$w.lb.l insert end $x}
    $w.lb.l insert end *NEW*
    bind $w.lb.l <Double-Button-1> "$add $net %W %y"
    bind $w.lb.l <Delete> "dccDAG $net %W"
    bind $w.lb.l <BackSpace> [bind $w.lb.l <Delete>]
    bind $w.lb.l <Control-h> [bind $w.lb.l <Delete>]
    grid $w.lb - -sticky nsew
    frame $w.btns
    button $w.btns.ok -text [trans ok] -command "dccSaveA $w $net $op" -width 6
    button $w.btns.cn -text [trans cancel] -command "destroy $w" -width 6
    button $w.btns.hl -text [trans help] -state disabled -width 6
    grid $w.btns.ok $w.btns.cn $w.btns.hl -sticky ew
    grid $w.btns -sticky ew
}
#
proc dccAAG {net w y} {
    set x [$w nearest $y]
    switch *NEW* [set nm [$w get $x]] {
	mkEntryBox {} {User Pattern} \
	   {Enter a regular expression for matching a nickname:} \
	   [list [list regexp {}]] [list ok "dccRXAG $w $net"] [list cancel {}]
    } default {
    }
}
#
proc dccAADG {net w y} {
    set x [$w nearest $y]
    switch *NEW* [set nm [$w get $x]] {
	mkEntryBox {} {Directory} \
	   {Enter a Directory name and a regular expression to match\
	    filenames to be put there.:} \
	   [list [list Directory {}] [list Regexp {.*}]] \
	   [list ok "dccDRXAG $w $net"] [list cancel {}]
    } default {
    }
}
#
proc dccRXAG {w net val} {
    switch {} $val return
    if {[catch {regexp $val aaaa}]} {
	tellError {} {Regexp error} {Invalid regular expression!}
    } {
	$w insert 0 $val
	$w selection clear 0 end
	$w selection set 0
    }
}
#
proc dccDRXAG {w net dir val} {
    switch {} $val return
    if {![file exists $dir]} {
	tellError {} {Directory error} "$dir does not exist."
    }
    if {![file isdirectory $dir]} {
	tellError {} {Directory error} "$dir is not a directory."
    }
    if {![file writable $dir]} {
	tellError {} {Directory error} "You cannot write to $dir."
    }
    if {[catch {regexp $val aaaa}]} {
	tellError {} {Regexp error} {Invalid regular expression!}
    } {
	$w insert 0 "$dir  {$val}"
	$w selection clear 0 end
	$w selection set 0
    }
}
#
proc dccDAG {net w} {catch {$w delete [$w curselection]}}
