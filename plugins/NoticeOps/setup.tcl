#
# $Source: /home/nlfm/Working/Zircon/Released/plugins/NoticeOps/setup.tcl,v $
# $Date: 1997/05/19 09:42:24 $
# $Revision: 1.3 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 1997 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
plugin NoticeOps {channel} {{msg Message}} {
    foreach x [$channel users] {
	if {![$net me $x] && [$channel isOp $x]} {
	    $net notice [$x name] "-CHANOPS- [$channel name] : $msg"
	}
    }
}
