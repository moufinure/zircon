# Zircon Release 1.18

This is a release of Zircon, an X11 interface to Internet Relay Chat. The
software is written in tcl/tk and uses its native features to provide
network communications. Suggestions for improvements to Zircon are
welcome.

Zircon now supports SOCKS - see the file doc/README.SOCKS for more details.

You will find that there are two versions of the zircon tar archive. One
of them has the tag "min" in its name - this version has been run through
my tcl minimiser program Frink and (in theory) should run faster on 7.* 
tcl versions as all redundant spaces etc. have been removed. The code in
this verion is pretty well unreadable. Tcl 8.* users do not need this version.

There are also Source RPM and Zip versions of the archive if these are more
suitable for you. Debian users can get a Debian package from the Debian FTP
site.

Zircon has more features than you could possibly imagine and
implements nearly everything that the irc II clients support as well
as many other useful and interesting features. This reflects the power
of tcl/tk *not* the fact that I am a wonderful programmer (I am, but
that is a side issue :-)) tcl/tk is a wonderful system and you need it
on your machine *NOW*.

Please send all reports, comments and suggestions to: 

	Lindsay.Marshall@newcastle.ac.uk

N.B. Users whose nick is zircon have nothing to do with this progam. I never
use the nick zircon on the increasingly rare times I go on IRC.

### Mail

I maintain a mailing list to which I very infrequently make announcements.
If you want to be on it, then send mail to me asking to be added. I am the
only person who can send to the list and I do not forward general questions
to it and you cannot be spammed from it.

Lindsay
Lindsay.Marshall@newcastle.ac.uk

## REQUIREMENTS

The software has been developed using:

	tcl	Version Version 8.0p2
	tk	Version Version 8.0p2

N.B. This version of zircon will not run with releases of tcl earlier than
7.5/4.1. It will run with alphas of 8.1, but it hits bugs in the tcl
interpreter some times.

tcl/tk is available from ftp.smli.com in the directory /pub/tcl.  The
latest version can always be found on catless.ncl.ac.uk in /pub which
is also the home of Zircon. You will also find here the latest
development snapshot of zircon - this will always have bug fixes and
new developements in it.

You can configure lots of stuff using X resources, thus it is possible
to make buttons and text associated with particular users and channels
appear in special colours and fonts. The system is much easier to use
than the usual UNIX interfaces to irc and a lot of people are using it
on a day to day basis. Zircon also supports a distributed drawing
board, but please do not use this when there are non-Zircon users
around as they get annoyed when they cant see the pictures.

For people who really cannot do without their ircII style commands a
subset of these are also supported both in an optional command line
and from Channel windows - see the MANUAL for details.

## INSTALLATION

These instructions are for Unix systesms. Zircon will run on Windows
machines - however you will need to do a small amount tweaking of the
software. If you want to do this and perhaps can help me set up proper
distributions for these systems then get in touch. There is a Macintosh
distribution that comes pre-installed.

1. Make sure you have tcl and tk installed.
1. Run the "installZircon" program - this is a tcl/tk program and it
   will look for a suitable interpreter using one of the usual names.
   if it cant find one then it will tell you. If you are on a Windows
   system then double click on "winstall" - if you have tcl/tk setup
   correctly this should run the installer.
1. A window will appear. Set the values to ones that are suitable for
   your system.
1. Now click "Install".
   If youa re not using tcl/tk V8.0 or better then you may find that
   you get compilation or link errors for the modules in Sed. This
   does not matter and will not affect the running of zircon. If you
   do get errors and can identify the cause please let me know.
1. The file Preferences shows you what you might want to put in your
   .zircon/preferences file in your HOME directory. But see the
   information about Multiple network support. Read the file
   README.prefs for information about what you can configure.
   You will also want to edit the file zircon.ad which can be found in
   the zircon library directory. This contains application defaults for
   X resources and is read by zircon on startup. 
   Zircon still supports the existence of a system wide preferences
   file, but this can cause problems and I would advise people not to
   use this feature. I will be working on it for the next version to
   make it work better with netspaces than it does at the moment - I
   appreciate that the feature is useful where many people are using
   zircon on the same machine.
   WARNING: IN THE NEAR FUTURE I WILL BE PROVIDING A DIFFERENT WAY OF
   TAILORING ZIRCON'S APPEARANCE. ONE THAT WILL BE PORTABLE TO SYSTEMS
   OTHER THAN X-WINDOWS - BE PREPARED FOR THIS CHANGE!!!! 

1. Off you go. Have fun!

Also included is a little program that will help you find the "Meta"
key that tcl/tk uses. This is called findMeta - run it and do what it
says if you don't know which is your Meta modifier. It is in the util
directory. Be warned that on some systems it doesn't help much!

