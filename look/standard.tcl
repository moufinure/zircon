#
# This file defines some values that control the look of the standard
# Zircon. This is just the start of the changes and this not what it will
# all be like in the future!!! I will let you know when it is safe to
# start tinkering with it...
#
array set Ops {
    server	{Links Version Motd Time Trace Admin Lusers Info Stats Oper}
    ircSrv	{Connect Rehash Restart Squit}
    user	{Who Whois Whowas Mode CTCP DCC Invite Memo Message Notice \
	         Finger Time Trace Userhost}
    ircop	{Kill}
    userMenu {Whois Message Notice Action Time CTCP DCC Notify Ignore Silence Finger}
    chanop	{Speak ChanOp Kick Ban BanKick}
}
