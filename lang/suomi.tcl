#
# $Source: /home/nlfm/Working/Zircon/Released/lang/RCS/suomi.tcl,v $
# $Date: 1999/11/26 09:36:42 $
# $Revision: 1.18.1.1 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 1996 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
array set ztrans {
    abort	Keskeyt�
    accept	Hyv�ksy
    action	Toiminta
    actions	Toiminnat
    active      Aktiivinen
    admin       Yll�pito
    all         Kaikki
    anonymous	Nimet�n
    append      Lis��
    apply       K�yt�
    away	Poissaolo
    back	Takaisin
    background  Tausta
    ban		Kielto
    brb		BRB
    busy	Kiireinen
    buttons	Painikkeet
    call	Kutsu
    cancel	Peruuta
    channel	Kanava
    channels	Kanavat
    chanop	KanavaOp
    chat	Keskustelu
    clear	Tyhjenn�
    clientinfo  Klienttitiedot
    clone	Klooni
    close	Sulje
    configure	M��rit�
    connect	Kytkeydy
    conversation Keskustelu
    crypt	Salaus
    ctcp	CTCP
    current     Viimeisin
    dcc		DCC
    debug	Korjaa
    default	Oletus
    delete	Poista
    dismiss	Poistu
    draw	Piirr�
    dump	H�vit�
    edit	Muokkaa
    empty	Tyhjenn�
    error	Virhe
    exec	{Suorita komento}
    favourites	Suosikit
    file        Tiedosto
    filename    {Tiedoston nimi}
    filter	Suodatin
    finger	Finger
    flush	Huuhtele
    font        Fontti
    foreground  Edusta
    friends     Yst�v�t
    get		Saada
    height      Korkeus
    help	Apu
    history	Historia
    hostname	{Koneen nimi}
    icon        Ikoni
    ignore      {�l� huomioi}
    ignores     {�l� huomioi}
    info	Tiedot
    information Informaatio
    invisible	N�kym�tt�myys
    invite	Kutsu
    irc		IRC
    ircname	{Irc nimi}
    ircnames    {Irc nimet}
    join	Liity
    jump	Hypp��
    keep	Pid�
    key		Avain
    kick	Potki
    kill	Tapa
    leave	Poistu
    load	Kuormitus
    log		Loki
    lost        Kadonnut
    limit	Rajoitus
    link        Linkki
    links       Linkit
    list	Lista
    lusers      K�ytt�j�t
    map		Kartta
    message	Viesti
    messages	Viestit
    mode	Tila
    moderated	Hillitty
    monitor	Tarkkaile
    motd        {P�iv�n sanoma}
    name	Nimi
    names	Nimet
    new		Uusi
    nickname	Lis�nimi
    nicknames   Lis�nimet
    no          Ei
    nocase	{Pienet kirjaimet}
    noshare     {�l� jaa}
    notice	Ilmoitus
    notify	Huomioi
    offer	Tarjoa
    ok		Hyv�ksy
    open	Avaa
    oper        Operoi
    operator	Operaattori
    other	Muu
    parameter	Parametri
    parameters	Parametrit
    password	Salasana
    pattern	Kuvio
    people	Ihmiset
    plugin	{Plug In}
    port	Portti
    print       Tulosta
    private	Yksityinen
    query	Kysely
    quiet	Vaiti
    quit	Poistu
    reconnecting	{Kytkeytyy uudelleen}
    refresh	Virkist�
    register	Kirjaudu
    rehash      Virkist�
    reject	Hylk��
    rejoin	{Liity uudelleen}
    restart     {K�ynnist� uudelleen}
    revert      Palaa
    save	Tallenna
    script	Skripti
    scroll	Vierit�
    secret	Salainen
    send	L�hetys
    server	Serveri
    servers	Serverit
    service	Palvelu
    services	Palvelut
    set	        Aseta
    share       Jaa
    shutdown    Sammuta
    signoff     {L�hde pois}
    silence     Hiljaisuus
    sound       ��ni
    speak       Puhu
    stats       Statistiikka
    tell        Kerro
    text        Teksti
    time        Aika
    trace       J�ljit�
    type        Tyyppi
    topic       Teema
    unban       {Perua kielto}
    unload	Pura
    user	K�ytt�j�
    userinfo    K�ytt�j�tiedot
    users	K�ytt�j�t
    version     Versio
    view	Katso
    who		Kuka
    whois	{Kuka on}
    whowas	{Kuka oli}
    width       Leveys
    windows	Ikkunat
    yes         Kyll�
}
#
# Compounds of the above that are no language dependent
#
array set ztrans "
    bankick	{$ztrans(ban)+$ztrans(kick)}
"

