#
# $Source: /home/nlfm/Zircon/lang/RCS/bulgar.tcl,v $
# $Date: 1996/12/12 14:07:39 $
# $Revision: 1.18.1.1 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
#   TRANSLATOR: Myshadow <root@myshadow.up.ac.za>
# ----------------------------------------------------------------------
# Copyright 1996 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
#
# Compounds of the above that are no language dependent
#
# For convenience and unification instead the Cyrrilic font the normal 
# international symbols have been used  
#
array set ztrans {
    actions	Dejstvie
    Admin       Administracija
    away	"Njama me"
    back	"Tuk sam"
    ban		Zabrana
    brb		"Shte se varna skoro"
    busy        Zaet
    buttons	Butoni
    call        Povikvam
    cancel	Otkaz
    channel	Kanal
    channels	Kanali
    chanop	Kanalen Operator
    chat	Razgovor
    clear	Izchistvam
    close	Zatvarjam
    connect	Verbinden
    crypt	Taen
    ctcp	CTCP
    dcc		DCC
    dismiss	Razpuskam
    draw	Risuvam
    empty	Izprazvam
    error	Greshka
    exec	Izpalnjavam
    favourites	Ljubimi
    finger	Prast
    flush	Izprazvam
    friend      Prijatel
    friends     Prijateli
    get		Vzemam
    help	Pomosht
    history	Istoria
    info	Informacija
    invite	Pokanvam
    irc		IRC
    ircname	Ircname
    join	Ptisaedinjavam se
    jump	Skacham
    keep	Pazja
    key	        Kljuch
    kick	Ritam
    kill	Ubivam
    leave	Izostavjam
    log		Log
    limit	Ogranichenie
    links       Vrazki
    list	Spisak
    message	Saobshtenie
    mode	Mode
    moderated	Umeren
    monitor	Nabljudavam
    name        Ime
    names       Imena
    new		Nov
    nickname	Prjakor
    notice	Belezhka
    notify	Zabeljazvam
    offer	Predlozhenie
    open        Otvarjam
    operator	Operator
    parameters	Parametri
    password	Parola
    people	Hora
    port	Port
    private	Chasten
    public      Obshtestven
    quiet	Tix
    quit	Napuskam
    reconnectin "Vrazvam se otnovo"
    refresh	Osvezhavam
    save        Zapazvam
    "save current"    "Zapazvam Tekusht"
    "save layout"     "Zapazvam Plan"
    script	Script 
    secret	Taen
    send	Izprashtam
    server	Server
    servers	Serveri
    service	Obsluzhvane
    services	Obsluzhvanija
    shutdown	Izkljuchvam
    speak	Govorja
    time	Vreme
    topic	Tema
    trace       Prosledjavam
    user	Potrebitel
    users	Potrebiteli
    version     Versija
    who		Koj
    whois	{Koj e}
    whowas	{Koj beshe}
    windows	Prozortci
}

