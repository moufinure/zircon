#
# $Source: /home/nlfm/Working/Zircon/Released/lang/RCS/deutsch.tcl,v $
# $Date: 1999/08/16 14:02:22 $
# $Revision: 1.18.1.4 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
#   TRANSLATOR: Klaus Luft <Klaus.Luft@stud.uni-karlsruhe.de>
#               Dirk F�rsterling <dirk@informatik.uni-frankfurt.de>
#
#   Note from Dirk:
#		Some translated words will sound horrible to native
#               germans (even worse then IBM or M$ translations).
#		This is due to limited space in the buttons.
#		I prefer the perfect outlook instead of the perfect
#		translation. This may be corrected in later versions.
#
#               some words are translated but not yet clear:
#		"type" (may be "Typ" or "Eingeben").
#
# ----------------------------------------------------------------------
# Copyright 1996 The University of Newcastle upon Tyne (see COPYRIGHT)
# 
#
array set ztrans {
    abort	Abbrechen
    accept	Akzeptieren
    action	Aktion
    actions	Aktionen
    anonymous	Anonym
    append	Anh�ngen
    away	{Bin weg}
    back	{Wieder da}
    ban		Verbannen
    brb		BGZ
    busy	Besch�ftigt
    buttons	Kn�pfe
    call        Aufrufen
    cancel	Abbrechen
    channel	Kanal
    channels	Kan�le
    chanop	KanalOp
    chat	Chat
    clear	L�schen
    clone	Duplizieren
    close	Schlie�en
    configure	Konfigurieren
    connect	Verbinden
    crypt	Verschl�sseln
    ctcp	CTCP
    dcc		DCC
    debug	Debug
    default	Vorgabe
    delete	L�schen
    dismiss	Ende
    draw	Zeichnen
    dump	Dump
    edit	Bearbeiten
    empty	Leeren
    error	Fehler
    exec	Ausf�hren
    favourites	Favoriten
    filename	Dateiname
    filter	Filter
    finger	Finger
    flush	Aktualisieren
    get		Empfangen
    help	Hilfe
    history	Historie
    hostname	Hostname
    info	Info
    invisible	Unsichtbar
    invite	Einladen
    irc		IRC
    ircname	IRC-Name
    join	�ffnen
    jump	Springen
    keep	Behalten
    key		Schl�ssel
    kick	Kicken
    kill	Kill
    leave	Verlassen
    load	Laden
    log		Mitschnitt
    limit	Limit
    list	Liste
    map		Map
    message	{Nachricht}
    messages	Nachrichten
    mode	Modus
    moderated	Moderiert
    monitor	�berwachen
    name	Name
    names	Namen
    new		Neu
    nickname	Nickname
    nocase	Kleinschreibung
    notice	Mitteilung
    notify	Benachrichtigen
    offer	Anbieten
    ok		OK
    open	�ffnen
    operator	Operator
    other	Andere
    parameter	Parameter
    parameters	Parameter
    password	Passwort
    pattern	Muster
    people	Personen
    plugin	Programmzusatz
    port	Port
    private	Privat
    query	Nachfrage
    quiet	Stumm
    quit	Beenden
    reconnecting	{Erneut verbinden}
    refresh	Auffrischen
    register	Registrieren
    reject	Ablehnen
    rejoin	{Erneut anw�hlen}
    save	Speichern
    script	Skript
    scroll	Scroll
    secret	Geheim
    send	Senden
    server	Server
    servers	Server
    service	Dienst
    services	Dienste
    set		Einstellen
    shutdown	Abschlu�
    signoff	{Kanal verlassen}
    silence	Stummschalten
    sound	Klang
    speak	Redner
    text	Text
    time	Zeit
    type	Eingeben
    topic	Thema
    unban	{Bann aufheben}
    unload	{Laden r�ckg�ngig}
    user	Teilnehmer
    users	Teilnehmer
    view	Ansicht
    who		Wer
    whois	{Wer ist}
    whowas	{Wer war}
    windows	Fenster
}
#
# Compounds of the above that are no language dependent
#
array set ztrans "
    bankick	{$ztrans(ban)+$ztrans(kick)}
"
