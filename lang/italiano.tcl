#
# $Source: /home/nlfm/Working/Zircon/Released/lang/RCS/italiano.tcl,v $
# $Date: 1996/12/12 14:07:39 $
# $Revision: 1.18.1.1 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Pino Zollo I2KFX <pinoz@mbox.vol.it>
# ----------------------------------------------------------------------
# Copyright 1996 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
array set ztrans {
    abort	Interrompi
    accept	Accetta
    action	Azione
    actions	Azioni
    append	Aggiungi
    away	{Vado via}
    back	{Sono tornato}
    ban		Disabilita
    brb		{Torno subito}
    busy	Occupato
    buttons	Bottoni
    call	Chiama
    cancel	Annulla
    channel	Canale
    channels	Canali
    chanop	ChanOp
    chat	Conversa
    clear	Cancella
    close	Chiudi
    connect	Connetti
    crypt	Cifra
    ctcp	CTCP
    dcc		DCC
    default	Iniziale
    delete	Cancella
    dismiss	Esci
    draw	Disegna
    empty	Vuoto
    error	Errorr
    exec	Esegui
    favourites	Favoriti
    finger	Finger
    flush	Scarica
    get		Acquisisci
    help	Aiuto
    history	Storia
    hostname	Hostname
    info	Informazioni
    invite	Invita
    irc		IRC
    ircname	{Nome IRC}
    join	Unisciti
    jump	Salta
    keep	Tieni
    key		Tasto
    kick	Espelli
    kill	Elimina
    leave	Termina
    log		Registra
    limit	Limita
    list	Lista
    message	Messaggio
    messages	Messaggi
    mode	Modo
    moderated	Moderato
    monitor	Controlla
    name	Nome
    names	Nomi
    new		Nuovo
    nickname	Soprannome
    nocase	Nocase
    notice	Avviso
    notify	Avvisa
    offer	Offri
    ok		SI
    open	Apri
    operator	Operatore
    parameter	Parametro
    parameters	Parametri
    password	Parolachiave
    pattern	Configurazione
    people	Gente
    plugin	{Plug In}
    port	Porta
    private	Privato
    quiet	Quieto
    quit	Termina
    reconnecting	Riconnettendo
    refresh	Rinfresca
    register	Registra
    reject	Rifiuta
    script	Script
    secret	Segreto
    send	Manda
    server	Servente
    servers	Serventi
    service	Servizio
    services	Servizi
    set		Imposta
    shutdown	Chiudi
    signoff	Sconnetti
    sound	Suono
    speak	Parla
    text	Testo
    time	Ora
    topic	Argomento
    unban	Riabilita
    user	Utente
    users	Utenti
    view	Vedi
    who		Chi
    whois	Chi_e'
    whowas	Chi_era
    windows	Finestre
}
#
# Compounds of the above that are no language dependent
#
array set ztrans "
    bankick	{$ztrans(ban)+$ztrans(kick)}
"
