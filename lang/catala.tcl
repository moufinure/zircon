#
# by Jordi Sanfeliu <mikaku@arrakis.es>
#
# Darrera actualitzaci� 1999/10/10
#
array set ztrans {
    abort       Avorta
    accept      Accepta
    action      Acci�
    actions     Accions
    admin	Administrador
    anonymous   An�nim
    append      Afegeix
    apply	Aplica
    away        Absent
    back        Torna
    ban         Censura
    brb         Tornar�
    busy        Ocupat
    buttons     Botons
    {by name}	{Per Nom}
    {by users}	{Per Usuaris}
    call        Trucada
    cancel      Anul�la
    channel     Canal
    channels    Canals
    chanop      {Operador del Canal}
    chat        Xerra
    clear       Neteja
    clone	Duplicat
    close       Tanca
    configure   Configura
    connect     Connecta
    crypt       Encripta
    ctcp        CTCP
    dcc         DCC
    debug       Depura
    default     {Per Defecte}
    delete      Esborra
    directory	Directori
    dismiss     Abandona
    draw        Dibuixa
    dump        Aboca
    edit	Edita
    empty       Buit
    error       Error
    exact	Exacte
    exec        Executa
    favourites  Favorits
    filename    {Nom de Fitxer}
    filter      Filtre
    finger      Finger
    flush       Buida
    font	Font
    friend	Amic
    friends	Amics
    get         Agafa
    help        Ajuda
    history     Hist�ria
    hostname    {Nom de M�quina}
    icon	Icona
    ignore	Ignora
    info        Informaci�
    invisible   Invisible
    invite      Convida
    irc         IRC
    ircname     IRCNAME
    join        Entra
    jump        Salta
    keep        Mant�
    key         Clau
    kick        Expulsa
    kill        Mata
    leave       Surt
    limit       L�mit
    list        Llista
    load	Carrega
    log         Informe
    map         Mapa
    message     Missatge
    messages    Missatges
    mode        Mode
    moderated   Moderat
    monitor     Monitor
    name        Nom
    names       Noms
    new         Nou
    nickname    Sobrenom
    nocase      Min�scules
    notice      Av�s
    notify      Notifica
    offer       Ofereix
    ok          D�acord
    open        Obre
    operator    Operador
    other       Altres
    parameter   Par�metre
    parameters  Par�metres
    password    Contrasenya
    pattern     Model
    people      Gent
    plugin      {Plug In}
    port        Port
    private     Privat
    public	P�blic
    quiet       Silenci�s
    quit        Anar-se�n
    reconnecting        Reconnectant
    refresh     Refresca
    register    Registra
    reject      Rebutja
    rejoin      Retorna
    save        Desa
    script      Script
    scroll      Despla�a
    search	Cerca
    secret      Secret
    send        Envia
    server      Servidor
    servers     Servidors
    service     Servei
    services    Serveis
    set         Fixa
    shutdown    Caiguda
    signoff     Desconnecta
    silence     Silenci
    sorted	Ordenat
    sound       So
    speak       Parla
    text        Texte
    time        Hora
    topic       Tema
    trace	Traceja
    unban       Descensura
    user        Usuari
    users       Usuaris
    version	Versi�
    view        Veure
    who         Qui
    whois       {Qui �s}
    whowas      {Qui era}
    windows     Finestres
}
#
# Compounds of the above that are no language dependent
#
array set ztrans "
    bankick     {$ztrans(ban)+$ztrans(kick)}
"
