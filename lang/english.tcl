#
# $Source: /home/nlfm/Working/Zircon/Released/lang/RCS/english.tcl,v $
# $Date: 1999/08/16 14:02:22 $
# $Revision: 1.18.1.2 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 1996 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
array set ztrans {
    abort	Abort
    {about zircon}	{About Zircon}
    accept	Accept
    action	Action
    actions	Actions
    {active icon}	{Active Icon}
    all		All
    anonymous	Anonymous
    append	Append
    apply	Apply
    auto	Auto
    away	Away
    back	Back
    background	Background
    {background colour}	{Background Colour}
    backwards	Backwards
    ban		Ban
    bold	Bold
    brb		BRB
    busy	Busy
    buttons	Buttons 
    {by name}	{By Name}
    {by users}	{By Users} 
    call	Call
    cancel	Cancel
    {case insensitive}	{Case Insensitive}
    channel	Channel
    {channel pattern}	{Channel Pattern}
    channels	Channels
    chanop	ChanOp
    chat	Chat
    clear	Clear
    clone	Clone
    close	Close
    configure	Configure
    connect	Connect
    crypt	Crypt
    ctcp	CTCP
    dcc		DCC
    debug	Debug
    default	Default
    delete	Delete
    directory	Directory
    dismiss	Dismiss
    draw	Draw
    dump	Dump
    edit	Edit
    empty	Empty
    error	Error
    exact	Exact
    exec	Exec
    favourites	Favourites
    filename    Filename
    filter	Filter
    finger	Finger
    flags	Flags
    flush	Flush
    font	Font
    foreground	Foreground
    {foreground colour}	{Foreground Colour}
    forwards	Forwards
    friends	Friends
    get		Get
    help	Help
    history	History
    hostname	Hostname
    icon	Icon
    ignore	Ignore
    info	Info
    inverse	Inverse
    invisible	Invisible
    invite	Invite
    irc		IRC
    ircname	Ircname
    join	Join
    {join all}	{Join All}
    jump	Jump
    keep	Keep
    key		Key
    kick	Kick
    kill	Kill
    leave	Leave
    limit	Limit
    list	List
    load	Load
    local	Local
    log		Log
    {log file}	{Log File}
    map		Map
    {maximum number of members}	{Maximum Number of Members}
    message	Message
    messages	Messages
    {minimum number of members}	{Minimum Number of Members}
    mode	Mode
    moderated	Moderated
    monitor	Monitor
    name	Name
    names	Names
    netspaces	Netspaces
    new		New
    nickname	Nickname
    {no buttons}	{No Buttons}
    nocase	Nocase
    {no confirm}	{No Confirm}
    normal	Normal
    notice	Notice
    notify	Notify
    offer	Offer
    ok		OK
    open	Open
    operator	Operator
    other	Other
    parameter	Parameter
    parameters	Parameters
    password	Password
    pattern	Pattern
    people	People
    plugin	{Plug In}
    {pop down}	{Pop Down}
    {pop up}	{Pop Up}
    port	Port
    print	Print
    private	Private
    public	Public
    query	Query
    quiet	Quiet
    quit	Quit
    reconnecting	Reconnecting
    refresh	Refresh
    regexp	Regexp
    register	Register
    reject	Reject
    rejoin	Rejoin
    {rearead prefs}	{Reread Prefs}
    revert	Revert
    save	Save
    {save current}	{Save Current}
    {save layout}	{Save Layout}
    script	Script
    scroll	Scroll
    search	Search
    {search string}	{Search String}
    secret	Secret
    send	Send
    {send to info}	{Send To Info}
    server	Server
    servers	Servers
    service	Service
    services	Services
    set		Set
    {show messages}	{Show Messages}
    shutdown	Shutdown
    signoff	Signoff
    silence	Silence
    sorted	Sorted
    sound	Sound
    speak	Speak
    text	Text
    time	Time
    timestamp	Timestamp
    type	Type
    topic	Topic
    {topic pattern}	{Topic Pattern}
    unban	Unban
    underline	Underline
    unload	Unload
    user	User
    users	Users
    view	View
    who		Who
    whois	Whois
    whowas	Whowas
    windows	Windows
    {with topic}	{With Topic}
}
#
# Compounds of the above that are no language dependent
#
array set ztrans "
    bankick	{$ztrans(ban)+$ztrans(kick)}
"
