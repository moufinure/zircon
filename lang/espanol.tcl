#
# $Source: /home/nlfm/Working/Zircon/Released/lang/RCS/espanol.tcl,v $
# $Date: 1999/08/16 14:02:22 $
# $Revision: 1.18.1.2 $
#
# ----------------------------------------------------------------------
#   AUTHOR:  Lindsay Marshall <lindsay.marshall@newcastle.ac.uk>
# ----------------------------------------------------------------------
# Copyright 1996 The University of Newcastle upon Tyne (see COPYRIGHT)
# ======================================================================
#
# Spanish translation by Rafael Perez Gonzalez <rafperez@cica.es>
#
array set ztrans {
    abort       Abortar
    accept      Aceptar
    action      Acci=F3n
    actions     Acciones
    anonymous   An=F3nimo
    append      A=F1adir
    away        Ausente
    back        Volver
    ban         Censurar
    brb         Volver=E9
    busy        Ocupado
    buttons     Botones
    call        Llamada
    cancel      Anular
    channel     Canal
    channels    Canales
    chanop      ChanOp
    chat        Charlar
    clear       Limpiar
    close       Cerrar
    configure   Configurar
    connect     Conectar
    crypt       Encriptar
    ctcp        CTCP
    dcc         DCC
    debug       Tracear
    default     {por defecto}
    delete      Borrar
    dismiss     Descartar
    draw        Dibujar
    dump        Volcar
    empty       Vac=EDo
    error       Error
    exec        Ejecutar
    favourites  Favoritos
    filename    Nom.fich
    filter      Filtro
    finger      Finger
    flush       Vaciar
    get         Coger
    help        Ayuda
    history     Historia
    hostname    Nom.nodo
    info        Informaci=F3n
    invisible   Invisible
    invite      Invitar
    irc         IRC
    ircname     IRCNAME
    join        Entrar
    jump        Saltar
    keep        Mantener
    key         Clave
    kick        Expulsar
    kill        Matar
    leave       Salir
    log         Log
    limit       L=EDmite
    list        Lista
    map         Mapa
    message     Mensaje
    messages    Mensajes
    mode        Modo
    moderated   Moderado
    monitor     Monitor
    name        Nombre
    names       Nombres
    new         Nuevo
    nickname    Apodo
    nocase      min=FAsculas
    notice      Aviso
    notify      Notificar
    offer       Ofrecer
    ok          OK
    open        Abrir
    operator    Operador
    other       Otros
    parameter   Par=E1metro
    parameters  Par=E1metros
    password    Password
    pattern     Modelo
    people      Gente
    plugin      {Plug In}
    port        Puerta
    private     Privado
    quiet       Silencioso
    quit        IRSE
    reconnecting        Reconectando
    refresh     Refrescar
    register    Registrar
    reject      Rechazar
    rejoin      Volver
    save        Grabar
    script      Script
    scroll      Desplazar
    secret      Secreto
    send        Enviar
    server      Servidor
    servers     Servidores
    service     Servicio
    services    Servicios
    set         Fijar
    shutdown    Ca=EDda
    signoff     Desconectar
    silence     Silencio
    sound       Sonido
    speak       Hablar
    text        Texto
    time        Hora
    topic       Asunto
    unban       No-censura
    user        Usuario
    users       Usuarios
    view        Ver
    who         Qui=E9n
    whois       {Qui=E9n es}
    whowas      {Qui=E9n era}
    windows     Ventanas
}
#
# Compounds of the above that are no language dependent
#
array set ztrans "
    bankick     {$ztrans(ban)+$ztrans(kick)}
"
