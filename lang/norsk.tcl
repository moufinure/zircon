#
# Thanks to oddms@ii.uib.no for this translation
#
# If you would like to help - send zircon@catless.ncl.ac.uk a version of this file
# in your favourite language....
#
array set ztrans {
    dismiss	Fjern
    cancel	Avbryt
    ok	OK
    whois	HvemEr
    whowas	HvemVar
    quit	Avslutt
    mode	Tilstand
    action	Handling
    error	Feil
    help	Hjelp
    leave	Forlat
    monitor	Observer
    names	Navn
    notice	Notis
    operator	Operat�r
    password	Passord
    server	Server
    servers	Serverer
    service	Tjeneste
    services	Tjenester
    topic	Tema
    user	Bruker
    users	Brukere
    nickname	Nicknavn
    ircname	Ircnavn
}

